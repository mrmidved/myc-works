﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Wpf_Exam
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public Dictionary<string, List<string>> _ImageFolder = new Dictionary<string, List<string>>(); // имя  папки с картинкамми и сами картинки
        public MainWindow()
        {
            InitializeComponent();
            string[] drivers = Directory.GetLogicalDrives();
            foreach(string d in drivers)
            {
                DirItems dir = new DirItems(d, d);
                this.AddTree(dir);
                this.treeView.Items.Add(dir);
            }
            this.listView.MouseDoubleClick += ListView_MouseDoubleClick;

			// no image
			//Image im = new Image();
			//BitmapImage bmp = new BitmapImage();
			//bmp.BeginInit();
			//bmp.UriSource = new Uri(@"C:\Users\mrmid\Desktop\Wpf_Exam1\Wpf_Exam\Resources\noImage.jpg");
			//bmp.EndInit();
			//im.Source = bmp;
			//this.lablePichter.Content = im;

			// context menu listview
			ContextMenuService.SetShowOnDisabled(this.listView.ContextMenu,true);
        }
 
        private void AddTree( DirItems dir) // метод заполнения дерева
        {
            try
            {
                string[] directory = Directory.GetDirectories(dir.FullPath);
                foreach (string d in directory)
                {
                    DirItems dirinDisk = new DirItems(System.IO.Path.GetFileName(d), d);
                    dir.Nodes.Add(dirinDisk);
                    try
                    {
                        string[] deceptive_node = Directory.GetDirectories(d);
                        if (deceptive_node.Length > 0)
                        {
                            DirItems deceptiveNode = new DirItems("?", "?");
                            dirinDisk.Nodes.Add(deceptiveNode);
                        }
                    }
                    catch { }
                }
            }
            catch { }         
        }

        private void treeView_Expanded(object sender, RoutedEventArgs e) // обработчик распахивания узла дерева
        {
            DirItems DI = ((TreeViewItem)e.OriginalSource).Header as DirItems;
            if (DI == null)
                return;
            DI.Nodes.Clear();
            this.AddTree(DI);

        }

        private void treeView_MouseDoubleClick(object sender, MouseButtonEventArgs e) // обработчик двойного клика елемента дерева
        {
            try
            {
                DirItems d =(DirItems) this.treeView.SelectedItem;
                DirectoryInfo DF = new DirectoryInfo(d.FullPath);
                FileInfo[] arrfile = DF.GetFiles();
                List<string> LfilesImages = new List<string>();
                foreach (FileInfo f in arrfile)
                {
                    if (f.Extension == ".jpg")
                        LfilesImages.Add(f.FullName);
                }
                this._ImageFolder.Add(d.DirName, LfilesImages);
                this.SmallImage();// размер картинок в listview по умолчанию
                this.comboBox.Text = "Маленькие значки";
                this.comboBoxFolders.Items.Add(d.DirName);
              
            }
            catch { }
        }
       
        //************************** отображение картинок в listview

        private MyImages PichterinComboBox( string fs, Image im) // записываем в MyImages поля
        {
            BitmapImage bmp = new BitmapImage();
            bmp.BeginInit();
            bmp.UriSource = new Uri(fs);
            bmp.EndInit();
            im.Source = bmp;
            string name = System.IO.Path.GetFileName(fs);// достаем имя из строки представляющей полное имя
            MyImages images = new MyImages(im, name);
            images._FullName = fs;
            return images;
        }
        private void SmallImage()
        {
            this.listView.Items.Clear();
                     foreach (List<string> f in this._ImageFolder.Values)
                    {
                        foreach (string fs in f)
                        {
                            Image im = new Image();
                            im.Height = 20;
							im.Margin = new Thickness(5);
							this.listView.Items.Add(this.PichterinComboBox(fs,im));
                       }
                    }
            this.listView.View = null;
        }

        private void BigImage()
        {
            this.listView.Items.Clear();
            foreach (List<string> f in this._ImageFolder.Values)
            {
                foreach (string fs in f)
                {
                    Image im = new Image();
                    im.Height = 50;
                    im.Margin = new Thickness(5);
                    this.listView.Items.Add(this.PichterinComboBox(fs, im));
                }
            }
            this.listView.View = null;
        }

        private void TableImage()
        {
            GridViewColumn Col1 = new GridViewColumn();
            Col1.Width = 300;
            Col1.Header = "Имя";
            Col1.DisplayMemberBinding = new System.Windows.Data.Binding("_ImageName");

            GridViewColumn Col2 = new GridViewColumn();
            Col2.Width = 200;
            Col2.Header = "Дата создания";
            Col2.DisplayMemberBinding = new System.Windows.Data.Binding("_Time");

            GridViewColumn Col3 = new GridViewColumn();
            Col3.Width = 150;
            Col3.Header = "Размер";
            Col3.DisplayMemberBinding = new System.Windows.Data.Binding("_Lenght");

            GridView GV = new GridView();
            GV.Columns.Add(Col1);
            GV.Columns.Add(Col2);
            GV.Columns.Add(Col3);
            this.listView.View = GV;

            foreach (List<string> f in this._ImageFolder.Values)
            {
                foreach (string fs in f)
                {
                    Image im = new Image();
                    im.Height = 10;
                    im.Margin = new Thickness(5);
                    FileInfo file = new FileInfo(fs);
                    MyImages images = new MyImages();
                    images = this.PichterinComboBox(fs, im);
                    images._Time = string.Format("{0}", file.CreationTime);
                    images._Lenght = file.Length;
                    this.listView.Items.Add(images);
                }
            }
        }

        private void comboBox_SelectionChanged(object sender, SelectionChangedEventArgs e) // обработчик выбора размера значков в listview
        {
            this.listView.Items.Clear();
            string nameComboboxItem = ((ComboBoxItem)(this.comboBox.SelectedItem)).Name;// приводим this.comboBox.SelectedItem к типу ComboBoxItem и получаем имя
            switch (nameComboboxItem)
            {
                case "bigImage":
                    this.BigImage();
                    break;
                case "smallImage":
                    this.SmallImage();
                    break;
                case "table":
                    this.TableImage();
                    break;
            }        
        }

        private void ListView_MouseDoubleClick(object sender, MouseButtonEventArgs e) // обработчик загрузки фото из listview
        {
            MyImages d = (MyImages)((ListView)e.Source).SelectedItem; // получаем имя выбранного файла(точнее объект - выбранная картинка)                                                              
            Image im = new Image();
            im.Margin = new Thickness(5);
            BitmapImage bmp = new BitmapImage();
            bmp.BeginInit();
            bmp.UriSource = new Uri(d._FullName);
            bmp.EndInit();
            im.Source = bmp;
            this.lablePichter.Content = im;
        }
   
        private string Button(Image im) // получаем строку с адресом текущей картини в лейбле для обработчиков кнопок вперед/назад
        {
            string s = string.Format("{0}", im.Source);
            string name = s.Remove(0, 8);
            string[] arr = name.Split('/');
            string name2 = null;
            for (int i = 0; i < arr.Length; i++)
            {
                if (i == arr.Length - 1)
                {
                    name2 += arr[i];
                    break;
                }
                name2 += arr[i] + "\\";
            }
            return name2;
        }
        private void Efects()
        {
            DoubleAnimation animation = new DoubleAnimation();
            animation.From = 0;
            animation.To = 550;
            animation.Duration = TimeSpan.FromMilliseconds(1000);
            this.lablePichter.BeginAnimation(Grid.WidthProperty, animation);
        }
        private void Efects1()
        {
            DoubleAnimation animation = new DoubleAnimation();
            ElasticEase r = new ElasticEase();
            animation.EasingFunction = r;
            animation.From =100;
            animation.To = 550;
            animation.Duration = TimeSpan.FromMilliseconds(1000);
            this.lablePichter.BeginAnimation(Grid.WidthProperty, animation);
        }
        private void forward_Click(object sender, RoutedEventArgs e) // кнопка вперед
        {
            this.Efects();
            Image im = (Image)this.lablePichter.Content;
            string name2 = Button(im);
            List<string> serch = new List<string>(); // записываем все адреса добавленных картинок в List
            foreach (List<string> f in this._ImageFolder.Values)
            {
                foreach (string fs in f)
                {
                    serch.Add(fs);
                }

            }
            int index = 0;
            foreach (string fs in serch) // Уже в Liste  находим текущюю картинку и назначаем лейблу следующюю
            {
                if (name2 == fs)
                {
                    BitmapImage bmp = new BitmapImage();
                    bmp.BeginInit();
                    try { bmp.UriSource = new Uri(serch[index + 1]); }
                    catch
                    {
                        index = -1;
                        bmp.UriSource = new Uri(serch[index + 1]);
                    }
                    bmp.EndInit();
                    im.Source = bmp;

                    this.lablePichter.Content = im;
                }
                index++;
            }
            this.listView.SelectedIndex+=1;
        }
        private void backward_Click(object sender, RoutedEventArgs e) // кнопка назад
        {
            this.Efects1();
            Image im = (Image)this.lablePichter.Content;
            string name2 = Button(im);
            List<string> serch = new List<string>(); // записываем все адреса добавленных картинок в List
            foreach (List<string> f in this._ImageFolder.Values)
            {
                foreach (string fs in f)
                {
                    serch.Add(fs);
                }

            }
            int index = 0;
            foreach (string fs in serch) // Уже в Liste  находим текущюю картинку и назначаем лейблу предыдущюю
            {
                if (name2 == fs)
                {
                    BitmapImage bmp = new BitmapImage();
                    bmp.BeginInit();
                    try { bmp.UriSource = new Uri(serch[index - 1]); }
                    catch
                    {
                        index = -1;
                        bmp.UriSource = new Uri(serch[index + 1]);
                    }
                    bmp.EndInit();
                    im.Source = bmp;
                    this.lablePichter.Content = im;
                }
                index++;
            }
            this.listView.SelectedIndex += 1;
        }

        private void comboBoxFolders_SelectionChanged(object sender, SelectionChangedEventArgs e)// обработчик удаления папки
        {
           string nameComboboxItem =(string)this.comboBoxFolders.SelectedValue;  
            foreach(string s in this._ImageFolder.Keys)
            {
                if (s == nameComboboxItem) 
                {
                    this._ImageFolder.Remove(s);
                    break;
                }
            }
            this.SmallImage();
            try { this.comboBoxFolders.Items.RemoveAt(this.comboBoxFolders.SelectedIndex); }
            catch { };   
        }

        private void MenuItem_Click(object sender, RoutedEventArgs e) // обработчик удаления картинки из ListView
        {
            MyImages d = (MyImages)this.listView.SelectedItem;
            foreach (List<string> f in this._ImageFolder.Values)
            {
                foreach (string fs in f)
                {
                    if (d._FullName == fs)
                    {
                        f.Remove(fs);
                        break;
                    }   
                }
            }
            this.SmallImage();
        }

        private void buttonSlide_Click(object sender, RoutedEventArgs e) // обработчик кнопки Slide
        {
            List<string> serch = new List<string>(); // записываем все адреса добавленных картинок в List
            foreach (List<string> f in this._ImageFolder.Values)
            {
                foreach (string fs in f)
                {
                    serch.Add(fs);
                }
            }
            Window1 win = new Window1();
            for (int i = 0; i < serch.Count; i++)
            {
                Image im = new Image();
                BitmapImage bmp = new BitmapImage();
                bmp.BeginInit();
                bmp.UriSource = new Uri(serch[i]);
                bmp.EndInit();
                im.Source = bmp;
                win.w1_im.Add(im); 
            }
                win.Show();// при открытии окна таймер в нем запускаеься автоматически
        }
    }

    class MyImages
    {
        public string _ImageName { get; set; }
        public string _FullName { get; set; }
        public Image _Picture { get; set; }
        public string _Time { get; set; }
        private double lenght;
        public double _Lenght
        {
            get { return lenght; }

            set { lenght = value * 0.001; }
        }

      
        public MyImages (Image images,string fullPath)
        {
            _Picture = images;
            _ImageName = fullPath;
        }

        public MyImages() { }
    }

    class DirItems
    {
       public string DirName { get; set; }
       public string FullPath { get; set; }
      
       private ObservableCollection<DirItems> nodes = new ObservableCollection<DirItems>();
       public ObservableCollection<DirItems> Nodes
        {
            set { this.nodes = value; }
            get { return this.nodes; }
        }

        public string ImageName
        {
            get {return @"Resources/Folder-icon.png"; }
            set { }
        }

        public DirItems(string dirname, string fullpath)
        {
            this.DirName = dirname;
            this.FullPath = fullpath;
        }
        public DirItems() { }
    }


    //Image im = new Image();
    //im.Height = 10;
    //im.Margin = new Thickness(5);
    //BitmapImage bmp = new BitmapImage();
    //bmp.BeginInit();
    //bmp.UriSource = new Uri(f.FullName);
    //bmp.EndInit();
    //im.Source = bmp;
    //MyImages images = new MyImages(im, f.Name);
    //this.listView.Items.Add(images);
}
