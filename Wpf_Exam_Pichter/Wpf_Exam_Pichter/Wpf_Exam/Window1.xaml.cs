﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace Wpf_Exam
{
    /// <summary>
    /// Логика взаимодействия для Window1.xaml
    /// </summary>
    public partial class Window1 : Window
    {
        public List<Image> w1_im = new List<Image>();
        private DispatcherTimer DT; // таймер
        private int index=0;
        public Window1()
        {
            InitializeComponent();
            this.KeyDown += Window1_KeyDown;
            DT = new DispatcherTimer();
            DT.Tick+= new EventHandler(dispatcherTimer_Tick);
            DT.Interval = new TimeSpan(0, 0, 3);
            DT.Start();

        }
        private void Efects()
        {
            DoubleAnimation animation = new DoubleAnimation();
            animation.From = 0;
            animation.To = 550;
            animation.Duration = TimeSpan.FromMilliseconds(1000);
            this.lableSlide.BeginAnimation(Grid.WidthProperty, animation);
        }
        private void dispatcherTimer_Tick(object sender, EventArgs e) // то что будет происходить в таймере
        {
            this.Efects();
            if (index < w1_im.Count)// вылетает!!!!!!
            {
                this.lableSlide.Content = w1_im[index];
            }
            else
            {
                DT.Stop();
                this.Close();
            }
            CommandManager.InvalidateRequerySuggested();
            index++;
          
        }

        private void Window1_KeyDown(object sender, KeyEventArgs e)
        {
            if(e.Key==Key.Escape)
            {
                DT.Stop();
                this.Close();
            }
             
        }
    }
}
