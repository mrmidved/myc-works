﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Data.Common;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Cinemas_Exam
{
    public partial class Form2 : Form
    {
        private DbCommand Db_comand;
        public  DataSet D_set;
        private DbDataAdapter Db_adapter;
        private DbProviderFactory Db_providerFactory;
        private DbConnection Db_connection;
        private string[] tablename = { "Cinemas", "Genre", "Films", "Sesions", "CinemaTickets" };

        private Form1 F1 = new Form1();// окно редактирования
        private Form3 F3 = new Form3();// окно выбора мест
        private ListViewItem LV1 = new ListViewItem();//получаем из колонок значения 
        private TableLayoutPanel TLP = new TableLayoutPanel();// места в кинотеатре
        private DateTime Now = DateTime.Now;// текущяя дата
        public Form2()
        {
            InitializeComponent();
            this.F3.FormClosed += F3_FormClosed;
            this.FormClosing += Form2_FormClosing;// удаляем прошедшие сеансы из БД
            this.listView1.Click += ListView1_Click;// активизируем кнопку брони
            this.button1_Serch.Click += Button1_Serch_Click;// кнопка поиск
            this.toolStripButton1.Click += ToolStripButton1_Click;// кнопка бронь
            this.toolStripButton1.Visible = false; // скрываем кнопу бронь
            //скрываем админские кнопки
            this.AdminButton();
          
            this.textBox1_Password.LostFocus += TextBox1_Password_LostFocus; // обработчик ввода пароля
            // админсие обработчики
            this.toolStripComboBox1.SelectedIndexChanged += ToolStripComboBox1_SelectedIndexChanged;// обработчик выбора таблиц админом
            this.toolStripButton2_DEL.Click += ToolStripButton2_Del_Click;
            this.toolStripButton3_ADD.Click += ToolStripButton3_ADD_Click;
            this.toolStripButton4_EDIT.Click += ToolStripButton4_EDIT_Click;
         
            //подключаем БД
            this.AddDataBase();

            //соединяем ключи
            this.KeyTables();

            //назначаем и заполняем колонки LV
            this.Colums();
           // this.AllFilms();
        }
        private void Form2_FormClosing(object sender, FormClosingEventArgs e) // чистим таблицу сеансов (удаляем прошедшие) 
        {
            this.Db_connection.Open();
            this.Db_comand.CommandText = string.Format("DELETE FROM Sesions WHERE date_beginning<'{0}'", this.Now.ToShortDateString());
            try { this.Db_comand.ExecuteNonQuery(); }
            catch (Exception e1) { MessageBox.Show(e1.Message); }
            this.Db_connection.Close();
        }
        private void AdminButton() // скрываем админские кнопки 
        {
            this.toolStripButton2_DEL.Visible = false;
            this.toolStripButton4_EDIT.Visible = false;
            this.toolStripButton3_ADD.Visible = false;
            this.toolStripComboBox1.Visible = false;
            this.toolStripSeparator1.Visible = false;
            this.toolStripSeparator2.Visible = false;
            this.toolStripSeparator3.Visible = false;
            this.toolStripSeparator4.Visible = false;
        }
        private void FillDataset() // заполняем DtaSet и комбобоксы выбора кинотеатров и жанров 
        {
            this.Db_connection.Open();
            this.Db_comand = this.Db_connection.CreateCommand();
            this.Db_adapter = this.Db_providerFactory.CreateDataAdapter();
            this.D_set = new DataSet();
            foreach (string t_name in this.tablename)
            {
                this.Db_comand.CommandText = "SELECT * FROM " + t_name;
                this.Db_adapter.SelectCommand = this.Db_comand;
                this.Db_adapter.Fill(this.D_set, t_name);
            }
            this.Db_connection.Close();
            foreach (DataRow R in this.D_set.Tables["Genre"].Rows)
            {
                this.comboBox1_Genre.Items.Add(R["name"]);
            }
            foreach (DataRow R in this.D_set.Tables["Cinemas"].Rows)
            {
                this.comboBox1_CinName.Items.Add(R["name"]);
            }

        }
        private void AddDataBase() // подключаем БД и загружаем таблицы 
        {
            string prowider = ConfigurationManager.AppSettings["provider"];
            string constring = ConfigurationManager.AppSettings["connectionStr"];

            this.Db_providerFactory = DbProviderFactories.GetFactory(prowider);
            this.Db_connection = this.Db_providerFactory.CreateConnection();

            //this.Text = this.Db_connection.GetType().FullName;
            this.Db_connection.ConnectionString = constring;

            this.FillDataset(); // заполняем DtaSet и комбобоксы с выбором жанров и фильмов
          
        }
        private void Colums() // колонки для LV с результатом userserch 
        {
            ColumnHeader filmname = new ColumnHeader();
            filmname.Text = "Название";
            filmname.Width = 160;
            this.listView1.Columns.Add(filmname);

            ColumnHeader filmgenre = new ColumnHeader();
            filmgenre.Text = "Жанр";
            filmgenre.Width = 100;
            this.listView1.Columns.Add(filmgenre);

            ColumnHeader filmduration = new ColumnHeader();
            filmduration.Text = "Время";
            filmduration.Width = 50;
            this.listView1.Columns.Add(filmduration);

            ColumnHeader cinema = new ColumnHeader();
            cinema.Text = "Кинотеатр";
            cinema.Width = 100;
            this.listView1.Columns.Add(cinema);

            ColumnHeader session = new ColumnHeader();
            session.Text = "Начало";
            session.Width = 60;
            this.listView1.Columns.Add(session);

            ColumnHeader date = new ColumnHeader();
            date.Text = "Дата";
            date.Width = 60;
            this.listView1.Columns.Add(date);

            this.listView1.View = View.Details;
            this.listView1.GridLines = true;
        }
        private void KeyTables() //соединяем ключи_ 
        {
            DataRelation DR_FilmGenre = new DataRelation("FilmGenre",
               this.D_set.Tables["Genre"].Columns["id"],
               this.D_set.Tables["Films"].Columns["id_Genre"]);
            this.D_set.Relations.Add(DR_FilmGenre);
            ForeignKeyConstraint FKS = DR_FilmGenre.ChildKeyConstraint;
            FKS.DeleteRule = Rule.None;
            FKS.UpdateRule = Rule.None;

            //DataRelation DR_TicketSession = new DataRelation("TicetSession", this.D_set.Tables["Sesions"].Columns["id"],
            //   this.D_set.Tables["CinemaTickets"].Columns["id_Sesion"]);
            //this.D_set.Relations.Add(DR_TicketSession);
            //ForeignKeyConstraint FKS1 = DR_TicketSession.ChildKeyConstraint;
            //FKS1.DeleteRule = Rule.None;
            //FKS1.UpdateRule = Rule.None;

            DataRelation DR_SessionFilm = new DataRelation("SessionFilm", this.D_set.Tables["Films"].Columns["id"],
              this.D_set.Tables["Sesions"].Columns["id_Film"]);
            this.D_set.Relations.Add(DR_SessionFilm);
            ForeignKeyConstraint FKS2 = DR_SessionFilm.ChildKeyConstraint;
            FKS2.DeleteRule = Rule.None;
            FKS2.UpdateRule = Rule.None;

            DataRelation DR_SessionCinema = new DataRelation("SessionCinema", this.D_set.Tables["Cinemas"].Columns["id"],
              this.D_set.Tables["Sesions"].Columns["id_Cinema"]);
            this.D_set.Relations.Add(DR_SessionCinema);
            ForeignKeyConstraint FKS3 = DR_SessionCinema.ChildKeyConstraint;
            FKS3.DeleteRule = Rule.None;
            FKS3.UpdateRule = Rule.None;

        }

        #region заполнение админских таблиц 
        private void TextBox1_Password_LostFocus(object sender, EventArgs e) // при вводе админом пароля 
        {
            if (this.textBox1_Password.Text == "1")
            {
                this.toolStripButton2_DEL.Visible = true;
                this.toolStripButton4_EDIT.Visible = true;
                this.toolStripButton3_ADD.Visible = true;
                this.toolStripComboBox1.Visible = true;
                this.toolStripButton1.Visible = false;
                this.toolStripSeparator1.Visible = true;
                this.toolStripSeparator2.Visible = true;
                this.toolStripSeparator3.Visible = true;
                this.toolStripSeparator4.Visible = true;
                this.listView1.Clear();
            }
        }
        private void ColumsAdmin_Films()
        {
            ColumnHeader filmname = new ColumnHeader();
            filmname.Text = "Название";
            filmname.Width = 160;
            this.listView1.Columns.Add(filmname);

            ColumnHeader filmgenre = new ColumnHeader();
            filmgenre.Text = "Жанр";
            filmgenre.Width = 100;
            this.listView1.Columns.Add(filmgenre);

            ColumnHeader filmduration = new ColumnHeader();
            filmduration.Text = "Время";
            filmduration.Width = 50;
            this.listView1.Columns.Add(filmduration);
            this.Admin_Films();
        }
        private void Admin_Films() 
        {
            var order =
                      from T in this.D_set.Tables["Genre"].AsEnumerable()
                      from T2 in this.D_set.Tables["Films"].AsEnumerable()
                      where
                      T2.Field<int>("id_Genre") == T.Field<int>("id")
                      select new
                      {
                          namef = T2.Field<string>("name"),
                          nameg = T.Field<string>("name"),
                          timef = T2.Field<TimeSpan>("duration"),
                      };
         
            foreach (var q in order)
            {
                ListViewItem LV = new ListViewItem(q.namef);
                LV.SubItems.Add(q.nameg);
                LV.SubItems.Add(q.timef.ToString());
                this.listView1.Items.Add(LV);
            }
        }
        private void ColumsAdmin_Genre()
        {
            ColumnHeader filmname = new ColumnHeader();
            filmname.Text = "Название";
            filmname.Width = 160;
            this.listView1.Columns.Add(filmname);
            this.Admin_Genre();
        }
        private void Admin_Genre()
        {
            var order = from T in this.D_set.Tables["Genre"].AsEnumerable()
                        select new { nameg = T.Field<string>("name") };

            foreach (var q in order)
            {
                ListViewItem LV = new ListViewItem(q.nameg);
                this.listView1.Items.Add(LV);
            }
        }
        private void ColumsAdmin_Cinemas()
        {
            ColumnHeader filmname = new ColumnHeader();
            filmname.Text = "Название";
            filmname.Width = 160;
            this.listView1.Columns.Add(filmname);

            ColumnHeader countrow = new ColumnHeader();
            countrow.Text = "Количество рядов";
            countrow.Width = 160;
            this.listView1.Columns.Add(countrow);

            ColumnHeader countseat = new ColumnHeader();
            countseat.Text = "Количество мест в ряду";
            countseat.Width = 160;
            this.listView1.Columns.Add(countseat);
            this.Admin_Cinemas();
        }
        private void Admin_Cinemas()
        {
            var order = from T in this.D_set.Tables["Cinemas"].AsEnumerable()
                        select new {
                            namec = T.Field<string>("name"),
                            countr = T.Field<int>("number_rows"),
                            counts = T.Field<int>("number_seats"),
                        };

            foreach (var q in order)
            {
                ListViewItem LV = new ListViewItem(q.namec);
                LV.SubItems.Add(q.countr.ToString());
                LV.SubItems.Add(q.counts.ToString());
                this.listView1.Items.Add(LV);
            }

        }
        private void ColumsAdmin_Sesions()
        {
            ColumnHeader filmname = new ColumnHeader();
            filmname.Text = "Фильм";
            filmname.Width = 160;
            this.listView1.Columns.Add(filmname);

            ColumnHeader cinemaname = new ColumnHeader();
            cinemaname.Text = "Кинотеатр";
            cinemaname.Width = 160;
            this.listView1.Columns.Add(cinemaname);

            ColumnHeader date = new ColumnHeader();
            date.Text = "Дата";
            date.Width = 80;
            this.listView1.Columns.Add(date);

            ColumnHeader time = new ColumnHeader();
            time.Text = "Время";
            time.Width = 60;
            this.listView1.Columns.Add(time);
            this.Admin_Sesions();
        }
        private void Admin_Sesions()
        {
            var order = from T in this.D_set.Tables["Cinemas"].AsEnumerable()
                        from T2 in this.D_set.Tables["Films"].AsEnumerable()
                        from T3 in this.D_set.Tables["Sesions"].AsEnumerable()
                        where 
                        T3.Field<int>("id_Film")==T2.Field<int>("id") &&
                        T3.Field<int>("id_Cinema") == T.Field<int>("id") //&&
                        //T3.Field<DateTime>("date_beginning")>=this.Now
                        select new {
                            namef = T2.Field<string>("name"),
                            namec = T.Field<string>("name"),
                            date=T3.Field<DateTime>("date_beginning"),
                            time=T3.Field<TimeSpan>("times")
                        };

            foreach (var q in order)
            {
                ListViewItem LV = new ListViewItem(q.namef);
                LV.SubItems.Add(q.namec);
                LV.SubItems.Add(q.date.ToShortDateString());
                LV.SubItems.Add(q.time.ToString());
                this.listView1.Items.Add(LV);
            }
        }
        private void ToolStripComboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.listView1.Clear();
            switch (this.toolStripComboBox1.SelectedItem.ToString())
            {
                case "Films": this.ColumsAdmin_Films();break;
                case "Genre":this.ColumsAdmin_Genre(); break;
                case "Cinemas": this.ColumsAdmin_Cinemas(); break;
                case "Sesions":this.ColumsAdmin_Sesions(); break;
            }
        }
        #endregion

        #region user поисковики

        private void AllFilms()  //  заполняем сразу всеми фильмами которые идут в кино 
        {
            foreach (DataRow R in this.D_set.Tables["Genre"].Rows)
            {
                DataRow[] F_G = R.GetChildRows(this.D_set.Relations["FilmGenre"]);
                for (int i = 0; i < F_G.Length; i++)
                {
                    ListViewItem LV = new ListViewItem(F_G[i]["name"].ToString());
                    LV.SubItems.Add(R["name"].ToString());
                    LV.SubItems.Add(F_G[i]["duration"].ToString());
                    this.listView1.Items.Add(LV);
                }
            }
        }
        private List<DataRow> Time() // заполняем List строками которые подходят по времени +- час 
        {
            List<DataRow> res = new List<DataRow>();
            TimeSpan hours = new TimeSpan(1, 0, 0);  // час +-
            try
            {
                TimeSpan serchtime = new TimeSpan(int.Parse(this.textBox1_H.Text), int.Parse(this.textBox1_M.Text), 0);    // введенное время
                string date = this.dateTimePicker1.Value.ToShortDateString();// введенная дата
                foreach (DataRow R in this.D_set.Tables["Sesions"].AsEnumerable())
                {
                    object d = R["date_beginning"];// дата из сеанса
                    DateTime Dt = (DateTime)d;
                    string datesesion = Dt.ToShortDateString();
                    if (date == datesesion)
                    {
                        object o = R["times"];
                        TimeSpan T = (TimeSpan)o; // время сеансов
                        TimeSpan minus = T.Subtract(hours);
                        TimeSpan plus = T.Add(hours);
                        if (serchtime <= plus && serchtime >= minus)
                            res.Add(R);
                    }
                }
            }
            catch (Exception e) { MessageBox.Show(e.Message);  }
            return res;
        }
        private void Serch_1() // поис по времени +- час 
        {
            foreach (DataRow R in this.Time())
            {
                var order =
                       from T in this.D_set.Tables["Genre"].AsEnumerable()
                       from T2 in this.D_set.Tables["Films"].AsEnumerable()
                       from T3 in this.D_set.Tables["Cinemas"].AsEnumerable()
                       where
                       T2.Field<int>("id_Genre") == T.Field<int>("id") &&
                       T2.Field<int>("id") == (int)R["id_film"] &&
                       T3.Field<int>("id") == (int)R["id_Cinema"]
                       select new
                       {
                           namef = T2.Field<string>("name"),
                           nameg = T.Field<string>("name"),
                           timef = T2.Field<TimeSpan>("duration"),
                           namec = T3.Field<string>("name"),
                           timesession = (TimeSpan)R["times"],
                           date=(DateTime)R["date_beginning"]
                       };
                foreach (var q in order)
                {
                    ListViewItem LV = new ListViewItem(q.namef);
                    LV.SubItems.Add(q.nameg);
                    LV.SubItems.Add(q.timef.ToString());
                    LV.SubItems.Add(q.namec);
                    LV.SubItems.Add(q.timesession.ToString());
                    LV.SubItems.Add(q.date.ToShortDateString());
                   this.listView1.Items.Add(LV);
                }
            }
        }
        private void Serch_2(string serchgenre) // поиск по времени +- час и жанру 
        {
            foreach (DataRow R in this.Time())
            {
                var order =
                       from T in this.D_set.Tables["Genre"].AsEnumerable()
                       from T2 in this.D_set.Tables["Films"].AsEnumerable()
                       from T3 in this.D_set.Tables["Cinemas"].AsEnumerable()
                       where
                       T2.Field<int>("id_Genre") == T.Field<int>("id") &&
                       T2.Field<int>("id") == (int)R["id_film"] &&
                       T3.Field<int>("id") == (int)R["id_Cinema"] &&
                       T.Field<string>("name")==serchgenre
                       select new
                       {
                           namef = T2.Field<string>("name"),
                           nameg = T.Field<string>("name"),
                           timef = T2.Field<TimeSpan>("duration"),
                           namec = T3.Field<string>("name"),
                           timesession = (TimeSpan)R["times"],
                           date = (DateTime)R["date_beginning"]
                       };
                foreach (var q in order)
                {
                    ListViewItem LV = new ListViewItem(q.namef);
                    LV.SubItems.Add(q.nameg);
                    LV.SubItems.Add(q.timef.ToString());
                    LV.SubItems.Add(q.namec);
                    LV.SubItems.Add(q.timesession.ToString());
                    LV.SubItems.Add(q.date.ToShortDateString());
                    this.listView1.Items.Add(LV);
                }
            }
        }
        private void Serch_3 (string serchgenre) // поиск по жанру 
        {
            var order =
                      from T in this.D_set.Tables["Genre"].AsEnumerable()
                      from T2 in this.D_set.Tables["Films"].AsEnumerable()
                      from T3 in this.D_set.Tables["Cinemas"].AsEnumerable()
                      from T4 in this.D_set.Tables["Sesions"].AsEnumerable()
                      where
                      T2.Field<int>("id_Genre") == T.Field<int>("id") &&
                      T4.Field<int>("id_Cinema")== T3.Field<int>("id")&&
                      T4.Field<int>("id_Film") == T2.Field<int>("id")&&
                      T.Field<string>("name") == serchgenre &&
                      T4.Field<DateTime>("date_beginning") == this.dateTimePicker1.Value.Date
                      select new
                      {
                          namef = T2.Field<string>("name"),
                          nameg = T.Field<string>("name"),
                          timef = T2.Field<TimeSpan>("duration"),
                          namec = T3.Field<string>("name"),
                          timesession = T4.Field<TimeSpan>("times"),
                          date = T4.Field<DateTime>("date_beginning")
                      };
            foreach (var q in order)
            {
                ListViewItem LV = new ListViewItem(q.namef);
                LV.SubItems.Add(q.nameg);
                LV.SubItems.Add(q.timef.ToString());
                LV.SubItems.Add(q.namec);
                LV.SubItems.Add(q.timesession.ToString());
                LV.SubItems.Add(q.date.ToShortDateString());
                this.listView1.Items.Add(LV);
            }
        }
        private void Serch_4(string serchgenre, string serchcinema) // поиск по жанру и кинотеатру 
        {
            var order =
                      from T in this.D_set.Tables["Genre"].AsEnumerable()
                      from T2 in this.D_set.Tables["Films"].AsEnumerable()
                      from T3 in this.D_set.Tables["Cinemas"].AsEnumerable()
                      from T4 in this.D_set.Tables["Sesions"].AsEnumerable()
                      where
                      T2.Field<int>("id_Genre") == T.Field<int>("id") &&
                      T4.Field<int>("id_Cinema") == T3.Field<int>("id") &&
                      T4.Field<int>("id_Film") == T2.Field<int>("id") &&
                      T.Field<string>("name") == serchgenre &&
                      T3.Field<string>("name") == serchcinema &&
                      T4.Field<DateTime>("date_beginning") == this.dateTimePicker1.Value.Date
                      select new
                      {
                          namef = T2.Field<string>("name"),
                          nameg = T.Field<string>("name"),
                          timef = T2.Field<TimeSpan>("duration"),
                          namec = T3.Field<string>("name"),
                          timesession = T4.Field<TimeSpan>("times"),
                          date = T4.Field<DateTime>("date_beginning")
                      };
            foreach (var q in order)
            {
                ListViewItem LV = new ListViewItem(q.namef);
                LV.SubItems.Add(q.nameg);
                LV.SubItems.Add(q.timef.ToString());
                LV.SubItems.Add(q.namec);
                LV.SubItems.Add(q.timesession.ToString());
                LV.SubItems.Add(q.date.ToShortDateString());
                this.listView1.Items.Add(LV);
            }
        }
        private void Serch_5(string serchcinema) // поиск по кинотеатру 
        {
            var order =
                      from T in this.D_set.Tables["Genre"].AsEnumerable()
                      from T2 in this.D_set.Tables["Films"].AsEnumerable()
                      from T3 in this.D_set.Tables["Cinemas"].AsEnumerable()
                      from T4 in this.D_set.Tables["Sesions"].AsEnumerable()
                      where
                      T2.Field<int>("id_Genre") == T.Field<int>("id") &&
                      T4.Field<int>("id_Cinema") == T3.Field<int>("id") &&
                      T4.Field<int>("id_Film") == T2.Field<int>("id") &&
                      T3.Field<string>("name") == serchcinema &&
                      T4.Field<DateTime>("date_beginning") == this.dateTimePicker1.Value.Date
                      select new
                      {
                          namef = T2.Field<string>("name"),
                          nameg = T.Field<string>("name"),
                          timef = T2.Field<TimeSpan>("duration"),
                          namec = T3.Field<string>("name"),
                          timesession = T4.Field<TimeSpan>("times"),
                          date = T4.Field<DateTime>("date_beginning")
                      };
            foreach (var q in order)
            {
                ListViewItem LV = new ListViewItem(q.namef);
                LV.SubItems.Add(q.nameg);
                LV.SubItems.Add(q.timef.ToString());
                LV.SubItems.Add(q.namec);
                LV.SubItems.Add(q.timesession.ToString());
                LV.SubItems.Add(q.date.ToShortDateString());
                this.listView1.Items.Add(LV);
            }
        }
        private void Serch_6(string serchgenre, string serchcinema) // поиск по времени +- час, жанру, кинотеатру 
        {
            foreach (DataRow R in this.Time())
            {
                var order =
                       from T in this.D_set.Tables["Genre"].AsEnumerable()
                       from T2 in this.D_set.Tables["Films"].AsEnumerable()
                       from T3 in this.D_set.Tables["Cinemas"].AsEnumerable()
                       where
                       T2.Field<int>("id_Genre") == T.Field<int>("id") &&
                       T2.Field<int>("id") == (int)R["id_film"] &&
                       T3.Field<int>("id") == (int)R["id_Cinema"] &&
                       T.Field<string>("name") == serchgenre &&
                       T3.Field<string>("name") == serchcinema
                       select new
                       {
                           namef = T2.Field<string>("name"),
                           nameg = T.Field<string>("name"),
                           timef = T2.Field<TimeSpan>("duration"),
                           namec = T3.Field<string>("name"),
                           timesession = (TimeSpan)R["times"],
                           date = (DateTime)R["date_beginning"]
                       };
                foreach (var q in order)
                {
                    ListViewItem LV = new ListViewItem(q.namef);
                    LV.SubItems.Add(q.nameg);
                    LV.SubItems.Add(q.timef.ToString());
                    LV.SubItems.Add(q.namec);
                    LV.SubItems.Add(q.timesession.ToString());
                    LV.SubItems.Add(q.date.ToShortDateString());
                    this.listView1.Items.Add(LV);
                }
            }
        }
        private void Serch_7(string serchcinema) // поиск по времени +- час и кинотеатру  
        {
            foreach (DataRow R in this.Time())
            {
                var order =
                       from T in this.D_set.Tables["Genre"].AsEnumerable()
                       from T2 in this.D_set.Tables["Films"].AsEnumerable()
                       from T3 in this.D_set.Tables["Cinemas"].AsEnumerable()
                       where
                       T2.Field<int>("id_Genre") == T.Field<int>("id") &&
                       T2.Field<int>("id") == (int)R["id_film"] &&
                       T3.Field<int>("id") == (int)R["id_Cinema"] &&
                       T3.Field<string>("name") == serchcinema
                       select new
                       {
                           namef = T2.Field<string>("name"),
                           nameg = T.Field<string>("name"),
                           timef = T2.Field<TimeSpan>("duration"),
                           namec = T3.Field<string>("name"),
                           timesession = (TimeSpan)R["times"],
                           date = (DateTime)R["date_beginning"]
                       };
                foreach (var q in order)
                {
                    ListViewItem LV = new ListViewItem(q.namef);
                    LV.SubItems.Add(q.nameg);
                    LV.SubItems.Add(q.timef.ToString());
                    LV.SubItems.Add(q.namec);
                    LV.SubItems.Add(q.timesession.ToString());
                    LV.SubItems.Add(q.date.ToShortDateString());
                    this.listView1.Items.Add(LV);
                }
            }
        }
        private void Serch_8(DateTime DT) // поиск по дате 
        {
            var order =
                    from T in this.D_set.Tables["Genre"].AsEnumerable()
                    from T2 in this.D_set.Tables["Films"].AsEnumerable()
                    from T3 in this.D_set.Tables["Cinemas"].AsEnumerable()
                    from T4 in this.D_set.Tables["Sesions"].AsEnumerable()
                    where
                    T2.Field<int>("id_Genre") == T.Field<int>("id") &&
                    T4.Field<int>("id_Cinema") == T3.Field<int>("id") &&
                    T4.Field<int>("id_Film") == T2.Field<int>("id") &&
                    T4.Field<DateTime>("date_beginning") == DT
                    select new
                    {
                        namef = T2.Field<string>("name"),
                        nameg = T.Field<string>("name"),
                        timef = T2.Field<TimeSpan>("duration"),
                        namec = T3.Field<string>("name"),
                        timesession = T4.Field<TimeSpan>("times"),
                        date = T4.Field<DateTime>("date_beginning")
                    };
            foreach (var q in order)
            {
                ListViewItem LV = new ListViewItem (q.namef);
                LV.SubItems.Add(q.nameg);
                LV.SubItems.Add(q.timef.ToString());
                LV.SubItems.Add(q.namec);
                LV.SubItems.Add(q.timesession.ToString());
                LV.SubItems.Add(q.date.ToShortDateString());
                this.listView1.Items.Add(LV);
            }
        }
        private void Button1_Serch_Click(object sender, EventArgs e) //  варианты user поиска 
        {
            this.listView1.Clear();
            this.Colums();// колонки пользовательского окна
            if (this.textBox1_H.Text != "" && this.textBox1_M.Text != "" && this.comboBox1_Genre.SelectedIndex == -1 && this.comboBox1_CinName.SelectedIndex == -1)
                this.Serch_1();
            if (this.textBox1_H.Text != "" && this.textBox1_M.Text != "" && this.comboBox1_Genre.SelectedIndex != -1 && this.comboBox1_CinName.SelectedIndex == -1)
                this.Serch_2(this.comboBox1_Genre.SelectedItem.ToString());
            if (this.textBox1_H.Text == "" && this.textBox1_M.Text == "" && this.comboBox1_Genre.SelectedIndex != -1 && this.comboBox1_CinName.SelectedIndex == -1)
                this.Serch_3(this.comboBox1_Genre.SelectedItem.ToString());
            if (this.textBox1_H.Text == "" && this.textBox1_M.Text == "" && this.comboBox1_Genre.SelectedIndex != -1 && this.comboBox1_CinName.SelectedIndex != -1)
                this.Serch_4(this.comboBox1_Genre.SelectedItem.ToString(), this.comboBox1_CinName.SelectedItem.ToString());
            if (this.textBox1_H.Text == "" && this.textBox1_M.Text == "" && this.comboBox1_Genre.SelectedIndex == -1 && this.comboBox1_CinName.SelectedIndex != -1)
                this.Serch_5(this.comboBox1_CinName.SelectedItem.ToString());
            if (this.textBox1_H.Text != "" && this.textBox1_M.Text != "" && this.comboBox1_Genre.SelectedIndex != -1 && this.comboBox1_CinName.SelectedIndex != -1)
                this.Serch_6(this.comboBox1_Genre.SelectedItem.ToString(), this.comboBox1_CinName.SelectedItem.ToString());
            if (this.textBox1_H.Text != "" && this.textBox1_M.Text != "" && this.comboBox1_Genre.SelectedIndex == -1 && this.comboBox1_CinName.SelectedIndex != -1)
                this.Serch_7(this.comboBox1_CinName.SelectedItem.ToString());
            if (this.textBox1_H.Text == "" && this.textBox1_M.Text == "" && this.comboBox1_Genre.SelectedIndex == -1 && this.comboBox1_CinName.SelectedIndex == -1)
            {
                this.Serch_8(this.dateTimePicker1.Value.Date);
                if (this.listView1.Items.Count == 0)
                {
                    MessageBox.Show("На выбранную дату сеансов нет");
                  //  this.AllFilms();
                }
            }
            this.textBox1_H.Clear();
            this.textBox1_M.Clear();
        }

        #endregion

        #region "Забронировать"
        private void ListView1_Click(object sender, EventArgs e)// активируем кнопку "Забронировать" 
        {
           // LV1 = (ListViewItem)this.listView1.SelectedItems[0];
            if (this.textBox1_Password.Text != "1")
            {
                this.toolStripButton1.Visible = true;
                this.toolStripButton1.Enabled = true;
            }
        }
        private void ToolStripButton1_Click(object sender, EventArgs e) // обработчик "Забронировать" 
        {
            int row = 0;
            int col = 0;
            try
            {
                LV1 = (ListViewItem)this.listView1.SelectedItems[0];
            }
            catch {
                this.toolStripButton1.Enabled = false;
                return;
            }
            // получаем количество мест в кинотеатре
            var order  = from T in this.D_set.Tables["Sesions"].AsEnumerable()
                         from T1 in this.D_set.Tables["Films"].AsEnumerable()
                         from T2 in this.D_set.Tables["Cinemas"].AsEnumerable()
                         where
                         T.Field<int>("id_Film") == T1.Field<int>("id") &&
                         T.Field<int>("id_Cinema") == T2.Field<int>("id") &&
                         T2.Field<string>("name") == LV1.SubItems[3].Text &&
                         T1.Field<string>("name") == LV1.SubItems[0].Text &&
                         T.Field<DateTime>("date_beginning") == DateTime.Parse(LV1.SubItems[5].Text) &&
                         T.Field<TimeSpan>("times") == TimeSpan.Parse(LV1.SubItems[4].Text)
                         select new {
                             rows = T2.Field<int>("number_rows"),
                             seats= T2.Field<int>("number_seats"),
                             idS=T.Field<int>("id")
                         };
            int ids = 0;
            foreach (var q in order)
            {
                row = q.rows;
                col = q.seats;
                ids = q.idS;// id сеанса где хотят забронировать место
            }
            this.TLP.Controls.Clear();
            this.TLP.RowCount = row;
            this.TLP.ColumnCount = col;
            this.TLP.Anchor = AnchorStyles.Left;
            this.TLP.Anchor = AnchorStyles.Right;
            this.TLP.Anchor = AnchorStyles.Top;
            this.TLP.Anchor = AnchorStyles.Bottom;
            this.TLP.BackColor = Color.Azure;
            this.TLP.AutoSizeMode = AutoSizeMode.GrowAndShrink;
            this.TLP.AutoSize = true;
            this.TLP.AutoScroll = true;
            this.F3.tableLayoutPanel2.Controls.Add(TLP, 1, 1);
            int allseats = row * col;
            
            for (int i = 0; i < allseats; i++)
            {
                Button B = new Button();
                B.BackColor = Color.Beige;
                B.Size = new Size(35, 40);
                B.Text = (i + 1).ToString();
                B.Click += B_Click;
                TLP.Controls.Add(B);
            }

            //получаем все билеты на этот сеанс с местами
            var order2 = from T in this.D_set.Tables["CinemaTickets"].AsEnumerable()
                         from T1 in this.D_set.Tables["Sesions"].AsEnumerable()
                         where
                        T.Field<int>("id_Sesions") == T1.Field<int>("id") &&
                        T1.Field<int>("id") == ids
                        select new
                        { seats = T.Field<int>("seat"), };
            List<int> numseats = new List<int>();
            foreach (var q in order2)
            {
                numseats.Add(q.seats);
            }
          for(int i=0;i<numseats.Count;i++)
            {
                ((Button)this.TLP.Controls[(numseats[i])-1]).Enabled = false;
                ((Button)this.TLP.Controls[(numseats[i]) - 1]).BackColor = Color.Azure;
            }
            
            this.F3.ShowDialog();
        }
        private void B_Click(object sender, EventArgs e) // создаем билет и помещаем его в БД 
        {
            Button B = (Button)sender;
            B.Enabled = false;
            int id_Sessions = 0;
            int seats = 0;
            var order = from T in this.D_set.Tables["Sesions"].AsEnumerable()
                         from T1 in this.D_set.Tables["Films"].AsEnumerable()
                         from T2 in this.D_set.Tables["Cinemas"].AsEnumerable()
                         where
                         T.Field<int>("id_Film") == T1.Field<int>("id") &&
                         T.Field<int>("id_Cinema") == T2.Field<int>("id") &&
                         T1.Field<string>("name") == LV1.SubItems[0].Text &&
                         T2.Field<string>("name") == LV1.SubItems[3].Text &&
                         T.Field<DateTime>("date_beginning") == DateTime.Parse(LV1.SubItems[5].Text) &&
                         T.Field<TimeSpan>("times") == TimeSpan.Parse(LV1.SubItems[4].Text)
                         select new {
                             id_Sessions = T.Field<int>("id"),
                             seats=T2.Field<int>("number_seats")
                         };
            foreach( var q in order )
            {
                id_Sessions = q.id_Sessions;
                seats = q.seats;
            }
            int numseat = int.Parse(B.Text);// номер места
            int row = ((numseat - (seats - 1)) / seats) + 1;// высчитываем номер ряда
            this.Db_connection.Open();
            this.Db_comand.CommandText = string.Format("INSERT INTO CinemaTickets (id_Sesions,rows,seat) VALUES ({0},{1},{2})", id_Sessions,row,numseat);
            try { this.Db_comand.ExecuteNonQuery(); } 
            catch (Exception e1) { MessageBox.Show(e1.Message); }
            this.Db_connection.Close();

        }
        private void F3_FormClosed(object sender, FormClosedEventArgs e)// обновляем Dataset после закрытия формы покупки билета 
        {
            this.comboBox1_CinName.Items.Clear();
            this.comboBox1_Genre.Items.Clear();
            this.FillDataset();
        }
        #endregion

        #region обработчики кнопок админа (ADD/UPDATE/DELETE)
        private void ClearAndFill() // чистим LV и перезаполняем DataSet & ComBobox поисков 
        {
            this.comboBox1_Genre.Items.Clear();// чистим комбобокс поиска по жанрам
            this.comboBox1_CinName.Items.Clear(); // чистим комбобокс с названием кинотеатров
            this.listView1.Items.Clear();// чистим LV
            this.FillDataset(); // заполняем DataSet
        }
        private void WindowSize(string table)// меняем размеры окна редактирования и соответствующие контролы 
        {
            this.F1.comboBox1.Items.Clear();
            this.toolStripButton1.Visible = false;// скрываем кнопку брони
            switch (this.toolStripComboBox1.SelectedItem.ToString())
            {
                case "Cinemas":
                    this.F1.Size = new Size(420, 190);
                    this.F1.textBox1.Visible = true;
                    this.F1.textBox1.Clear();
                    this.F1.comboBox1.Visible = false;
                    this.F1.textBox2_H.Visible = true;
                    this.F1.textBox2_H.Clear();
                    this.F1.textBox3_M.Visible = true;
                    this.F1.textBox3_M.Clear();
                    this.F1.label1.Visible = true;
                    this.F1.label1.Text = "Рядов/Мест";
                    this.F1.dateTimePicker1.Visible = false;
                    this.F1.comboBox2_Sfilms.Visible = false;
                    break;
                case "Genre": this.F1.Size = new Size(230, 160);
                    this.F1.textBox1.Visible = true;
                    this.F1.textBox1.Clear();
                    this.F1.comboBox1.Visible = false;
                    this.F1.textBox2_H.Visible = false;
                    this.F1.textBox3_M.Visible = false;
                    this.F1.label1.Visible = false;
                    this.F1.dateTimePicker1.Visible = false;
                    this.F1.comboBox2_Sfilms.Visible = false;
                    break;
                case "Sesions": this.F1.Size = new Size(390, 260);
                    //чистим и заполняем комбобокс фильмами
                    this.F1.comboBox2_Sfilms.Items.Clear();
                    foreach (DataRow R in this.D_set.Tables["Films"].Rows)
                        this.F1.comboBox2_Sfilms.Items.Add(R["name"]);
                    this.F1.comboBox2_Sfilms.Visible = true;

                    // чистим и заполняем комбобокс кинотеатрами
                    this.F1.comboBox1.Items.Clear();
                    foreach (DataRow R in this.D_set.Tables["Cinemas"].Rows)
                        this.F1.comboBox1.Items.Add(R["name"]);
                    this.F1.comboBox1.Visible = true;
                  
                    this.F1.textBox1.Visible = false;
                    this.F1.dateTimePicker1.Visible = true;
                    this.F1.textBox2_H.Visible = true;
                    this.F1.textBox2_H.Clear();
                    this.F1.textBox3_M.Visible = true;
                    this.F1.textBox3_M.Clear();
                    this.F1.label1.Visible = true;
                    this.F1.label1.Text = "Время";
                    break;
                case "Films": this.F1.Size = new Size(360, 230);
                    //чистим и заполняем комбобокс жанрами
                    this.F1.comboBox1.Items.Clear();
                    foreach (DataRow R in this.D_set.Tables["Genre"].Rows)
                        this.F1.comboBox1.Items.Add(R["name"]);
                    
                    this.F1.textBox1.Visible = true;
                    this.F1.textBox1.Clear();
                    this.F1.comboBox1.Visible = true;
                    this.F1.textBox2_H.Visible = true;
                    this.F1.textBox2_H.Clear();
                    this.F1.textBox3_M.Visible = true;
                    this.F1.textBox3_M.Clear();
                    this.F1.label1.Visible = true;
                    this.F1.label1.Text = "Время";
                    this.F1.dateTimePicker1.Visible =false ;
                    this.F1.comboBox2_Sfilms.Visible = false;
                    break;
            }
        }
        public bool SessionTime(int id_Cinema,DateTime sessions,TimeSpan st,DateTime DT)// метод проверки времени и даты сеансов при добавлении/редактировании
        {
            bool sesiontime = true;
            var order3 =
                         from T in this.D_set.Tables["Cinemas"].AsEnumerable()
                         from T1 in this.D_set.Tables["Sesions"].AsEnumerable()
                         from T2 in this.D_set.Tables["Films"].AsEnumerable()
                         where
                         T1.Field<int>("id_Cinema") == T.Field<int>("id") &&
                         T.Field<int>("id") == id_Cinema &&
                         T1.Field<int>("id_Film") == T2.Field<int>("id") &&
                         (T1.Field<DateTime>("date_beginning") <= sessions && T1.Field<DateTime>("date_beginning") >= DT)
                         select new
                         {
                             time = T1.Field<TimeSpan>("times"),
                             fdurations = T2.Field<TimeSpan>("duration")
                         };
            Dictionary<TimeSpan, TimeSpan> LTS = new Dictionary<TimeSpan, TimeSpan>(); // получам список времен сеансов кинотеатра в заданном промежутке дат
            TimeSpan time_plus = new TimeSpan(); // вспомогательная переменная
            TimeSpan time_film = new TimeSpan(); // продолжительность фильма
         
                foreach (var q in order3)
                {
                    time_film = q.fdurations;
                    time_plus = q.time.Add(time_film);// добавляем к ним длительность фильма и заносим в List
                try
                {
                    LTS.Add(q.time, time_plus);
                }
                catch { }
            }
           
            foreach (KeyValuePair<TimeSpan, TimeSpan> t in LTS)// сверяем данное время с промежутками
                if (st >= t.Key && st <= t.Value)
                    sesiontime = false;

            return sesiontime;
        }
        private void ToolStripButton2_Del_Click(object sender, EventArgs e) 
        {
            if (this.toolStripComboBox1.SelectedIndex == -1)
            { MessageBox.Show("Не выбрана таблица"); return; }
            this.toolStripButton1.Visible = false;// скрываем кнопку брони
            try
            {
               LV1 = (ListViewItem)this.listView1.SelectedItems[0];
            }
            catch
            {
                MessageBox.Show("Не выбран елемент списка", "Error!!!", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
                this.Db_connection.Open();
                switch (this.toolStripComboBox1.SelectedItem.ToString())
                {
                    case "Cinemas":
                        // получаем id удаляемого кинотеатра
                        int id_Cinema = 0;
                        var order1 = from T in this.D_set.Tables["Cinemas"].AsEnumerable()
                                     where T.Field<string>("name") == LV1.SubItems[0].Text
                                     select new { id_nameg = T.Field<int>("id") };
                        id_Cinema = order1.Single().id_nameg; // получаем id_Genre

                        this.Db_comand.CommandText = string.Format("DELETE FROM Cinemas WHERE id ={0}", id_Cinema);
                        try { this.Db_comand.ExecuteNonQuery(); } 
                        catch (Exception e1) { MessageBox.Show(e1.Message); }
                        this.Db_connection.Close();
                        this.ClearAndFill();
                        this.Admin_Cinemas(); // заполняем админские таблицы
                        break;
                    case "Genre":
                        // получаем id удаляемого жанра
                        int id_Genre = 0;
                        var order = from T in this.D_set.Tables["Genre"].AsEnumerable()
                                    where T.Field<string>("name") == LV1.SubItems[0].Text
                                    select new { id_nameg = T.Field<int>("id") };
                        id_Genre = order.Single().id_nameg; // получаем id_Genre

                        this.Db_comand.CommandText = string.Format("DELETE FROM Genre WHERE id ={0}", id_Genre);
                        try { this.Db_comand.ExecuteNonQuery(); } //для обновления БД, вызываем после каждого запроса
                        catch (Exception e1) { MessageBox.Show(e1.Message); }
                        this.Db_connection.Close();
                        this.ClearAndFill();
                        this.Admin_Genre(); // заполняем админские таблицы
                        break;
                    case "Films":
                       // получаем id удаляемого фильма
                       int id_Film = 0;
                       var order3 = from T in this.D_set.Tables["Films"].AsEnumerable()
                                     where T.Field<string>("name") == LV1.SubItems[0].Text
                                     select new { id_films = T.Field<int>("id") };
                       id_Film = order3.Single().id_films;

                       // формируем запрос
                        this.Db_comand.CommandText = string.Format("DELETE FROM Films WHERE id ={0}", id_Film);
                        try { this.Db_comand.ExecuteNonQuery(); }
                        catch (Exception e1) { MessageBox.Show(e1.Message); }
                        this.Db_connection.Close();
                        this.ClearAndFill();
                        this.Admin_Films();
                        break;
                    case "Sesions":
                        // получаем id редактируемого сеанса
                        int id_Sessions = 0;
                        var order6 = from T in this.D_set.Tables["Sesions"].AsEnumerable()
                                     from T1 in this.D_set.Tables["Films"].AsEnumerable()
                                     from T2 in this.D_set.Tables["Cinemas"].AsEnumerable()
                                     where
                                     T.Field<int>("id_Film") == T1.Field<int>("id") &&
                                     T.Field<int>("id_Cinema") == T2.Field<int>("id") &&
                                     T1.Field<string>("name") == LV1.SubItems[0].Text &&
                                     T2.Field<string>("name") == LV1.SubItems[1].Text &&
                                     T.Field<DateTime>("date_beginning") == DateTime.Parse(LV1.SubItems[2].Text) &&
                                     T.Field<TimeSpan>("times") == TimeSpan.Parse(LV1.SubItems[3].Text)
                                     select new{  id_Sessions = T.Field<int>("id"),};
                        id_Sessions = order6.Single().id_Sessions;

                       // формируем запрос
                       this.Db_comand.CommandText = string.Format("DELETE FROM Sesions WHERE id ={0}", id_Sessions);
                       try { this.Db_comand.ExecuteNonQuery(); }
                       catch (Exception e1) { MessageBox.Show(e1.Message); }
                       this.Db_connection.Close();
                       this.ClearAndFill();
                       this.Admin_Sesions();
                       break;
            }
        }
        private void ToolStripButton4_EDIT_Click(object sender, EventArgs e) 
        {
            if (this.toolStripComboBox1.SelectedIndex == -1)
            { MessageBox.Show("Не выбрана таблица"); return; }
            this.WindowSize(this.toolStripComboBox1.SelectedItem.ToString()); // меняем размеры окна и соответствующие контролы

            // заполняем поля формы для редактирования
            try
            {
                LV1 = (ListViewItem)this.listView1.SelectedItems[0];
            }
            catch
            {
                MessageBox.Show("Не выбран елемент списка", "Error!!!", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }
            switch (this.toolStripComboBox1.SelectedItem.ToString())
            {
                case "Cinemas":
                  this.F1.textBox1.Text = LV1.SubItems[0].Text;
                    this.F1.textBox2_H.Text = LV1.SubItems[1].Text;
                    this.F1.textBox3_M.Text = LV1.SubItems[2].Text;
                    break;
                case "Genre":
                    this.F1.textBox1.Text = LV1.SubItems[0].Text;
                    break;
                case "Films":
                    this.F1.textBox1.Text = LV1.SubItems[0].Text;
                    this.F1.comboBox1.SelectedItem= LV1.SubItems[1].Text;
                    var order = from T in this.D_set.Tables["Films"].AsEnumerable()
                                 where T.Field<string>("name") == LV1.SubItems[0].Text
                                 select new { time = T.Field<TimeSpan>("duration") };
                    TimeSpan TS = order.Single().time;
                    this.F1.textBox2_H.Text = TS.Hours.ToString();
                    this.F1.textBox3_M.Text = TS.Minutes.ToString();
                    break;
                case "Sesions":
                    this.F1.comboBox2_Sfilms.SelectedItem = LV1.SubItems[0].Text;
                    this.F1.comboBox1.SelectedItem = LV1.SubItems[1].Text;
                    var order2 = from T in this.D_set.Tables["Sesions"].AsEnumerable()
                                 from T1 in this.D_set.Tables["Films"].AsEnumerable()
                                 from T2 in this.D_set.Tables["Cinemas"].AsEnumerable()
                                 where 
                                 T.Field<int>("id_Film")==T1.Field<int>("id")&&
                                 T.Field<int>("id_Cinema") == T2.Field<int>("id")&&
                                 T1.Field<string>("name") == LV1.SubItems[0].Text&&
                                 T2.Field<string>("name") == LV1.SubItems[1].Text
                                 select new {
                                    time = T.Field<TimeSpan>("times"),
                                    date=T.Field<DateTime>("date_beginning")
                                };
                    TimeSpan TS2= new TimeSpan (0,0,0);
                    DateTime DT2= new DateTime();
                    foreach ( var q in order2)
                    {
                        TS2 = q.time;
                        DT2 = q.date;
                    }
                    this.F1.textBox2_H.Text = TS2.Hours.ToString();
                    this.F1.textBox3_M.Text = TS2.Minutes.ToString();
                    this.F1.dateTimePicker1.Value = DT2;
                    break;
            }
         
            if (F1.ShowDialog() == DialogResult.OK)
            {
                this.Db_connection.Open();
                switch (this.toolStripComboBox1.SelectedItem.ToString())
                {
                    case "Cinemas":
                        // получаем id редактируемого кинотеатра
                        int id_Cinema = 0;
                        var order1 = from T in this.D_set.Tables["Cinemas"].AsEnumerable()
                                    where T.Field<string>("name") == LV1.SubItems[0].Text
                                    select new { id_nameg = T.Field<int>("id") };
                        id_Cinema = order1.Single().id_nameg; // получаем id_Genre

                        this.Db_comand.CommandText = string.Format("UPDATE {0} SET name='{1}',number_rows={2},number_seats={3} WHERE id={4}",
                                this.toolStripComboBox1.SelectedItem.ToString(), this.F1.textBox1.Text,int.Parse(this.F1.textBox2_H.Text), int.Parse(this.F1.textBox3_M.Text), id_Cinema);
                        try { this.Db_comand.ExecuteNonQuery(); } //для обновления БД, вызываем после каждого запроса
                        catch (Exception e1) { MessageBox.Show(e1.Message); }
                        this.Db_connection.Close();
                        this.ClearAndFill();
                        this.Admin_Cinemas(); // заполняем админские таблицы
                        break;
                    case "Genre":
                        // получаем id редактируемого жанра
                        int id_Genre = 0;
                        var order = from T in this.D_set.Tables["Genre"].AsEnumerable()
                                    where T.Field<string>("name") == LV1.SubItems[0].Text
                                    select new { id_nameg = T.Field<int>("id") };
                        id_Genre = order.Single().id_nameg; // получаем id_Genre

                        this.Db_comand.CommandText = string.Format("UPDATE {0} SET name='{1}' WHERE id={2}",
                                this.toolStripComboBox1.SelectedItem.ToString(), this.F1.textBox1.Text,id_Genre);
                        try { this.Db_comand.ExecuteNonQuery(); } //для обновления БД, вызываем после каждого запроса
                        catch (Exception e1) { MessageBox.Show(e1.Message); }
                        this.Db_connection.Close();
                        this.ClearAndFill();
                        this.Admin_Genre(); // заполняем админские таблицы
                        break;
                    case "Films":
                        // получаем id редактируемого фильма
                        int id_Film = 0;
                        var order3 = from T in this.D_set.Tables["Films"].AsEnumerable()
                                     where T.Field<string>("name") == LV1.SubItems[0].Text
                                     select new { id_films = T.Field<int>("id") };
                        id_Film = order3.Single().id_films; 

                        //получаем id жанра по его имени из комбобокса
                        int id_GenreF = 0;
                        var order2 = from T in this.D_set.Tables["Genre"].AsEnumerable()
                                    where T.Field<string>("name") == this.F1.comboBox1.SelectedItem.ToString()
                                    select new { id_nameg = T.Field<int>("id") };
                        id_GenreF = order2.Single().id_nameg; 

                        // получаем продолжительность фильма из текстбоксов
                        TimeSpan ft = new TimeSpan(int.Parse(this.F1.textBox2_H.Text), int.Parse(this.F1.textBox3_M.Text), 0);

                        // формируем запрос
                        this.Db_comand.CommandText = string.Format("UPDATE {0} SET name='{1}',id_Genre={2},duration='{3}' WHERE id={4}",
                                this.toolStripComboBox1.SelectedItem.ToString(), this.F1.textBox1.Text, id_GenreF, ft,id_Film);
                        try { this.Db_comand.ExecuteNonQuery(); }
                        catch (Exception e1) { MessageBox.Show(e1.Message); }
                        this.Db_connection.Close();
                        this.ClearAndFill();
                        this.Admin_Films();
                        break;
                    case "Sesions":
                        // получаем id редактируемого сеанса
                        int id_Sessions = 0;
                        var order6 = from T in this.D_set.Tables["Sesions"].AsEnumerable()
                                     from T1 in this.D_set.Tables["Films"].AsEnumerable()
                                     from T2 in this.D_set.Tables["Cinemas"].AsEnumerable()
                                     where
                                     T.Field<int>("id_Film") == T1.Field<int>("id") &&
                                     T.Field<int>("id_Cinema") == T2.Field<int>("id") &&
                                     T1.Field<string>("name") == LV1.SubItems[0].Text &&
                                     T2.Field<string>("name") == LV1.SubItems[1].Text &&
                                     T.Field<DateTime>("date_beginning") == DateTime.Parse(LV1.SubItems[2].Text) &&
                                     T.Field<TimeSpan>("times")== TimeSpan.Parse(LV1.SubItems[3].Text)
                                     select new
                                     {
                                         id_Sessions = T.Field<int>("id"),
                                     };
                        id_Sessions = order6.Single().id_Sessions;

                        //получаем id фильма по его имени из комбобокса
                        int id_FilmS = 0;
                        var order4 = from T in this.D_set.Tables["Films"].AsEnumerable()
                                     where T.Field<string>("name") == this.F1.comboBox2_Sfilms.SelectedItem.ToString()
                                     select new { id_namef = T.Field<int>("id") };
                        id_FilmS = order4.Single().id_namef; 

                        // получаем id кинотеатра 
                        int id_CinemaS = 0;
                        var order5 = from T in this.D_set.Tables["Cinemas"].AsEnumerable()
                                     where T.Field<string>("name") == this.F1.comboBox1.SelectedItem.ToString()
                                     select new { id_namec = T.Field<int>("id") };
                        id_CinemaS = order5.Single().id_namec; 

                        // получаем время сеанса
                        TimeSpan st = new TimeSpan(int.Parse(this.F1.textBox2_H.Text), int.Parse(this.F1.textBox3_M.Text), 0);
                        DateTime DT = DateTime.Now;// сегоднешняя дата
                        if (this.SessionTime(id_CinemaS, this.F1.dateTimePicker1.Value, st, DT))
                        {
                            // формируем запрос
                            this.Db_comand.CommandText = string.Format("UPDATE {0} SET id_Film={1},id_Cinema={2},date_beginning='{3}',times='{4}' WHERE id={5}",
                                 this.toolStripComboBox1.SelectedItem.ToString(), id_FilmS, id_CinemaS, this.F1.dateTimePicker1.Value, st, id_Sessions);
                            try { this.Db_comand.ExecuteNonQuery(); }
                            catch (Exception e1) { MessageBox.Show(e1.Message); }
                        }
                        else
                        {
                            string messadge = string.Format("В {0} {1} идут другие фильмы !!!",st, this.F1.dateTimePicker1.Value.ToShortDateString());
                            MessageBox.Show(messadge);
                        }
                           
                        this.Db_connection.Close();
                        this.ClearAndFill();
                        this.Admin_Sesions();
                        break;
                }
            }
        }
        private void ToolStripButton3_ADD_Click(object sender, EventArgs e) 
        {
            if (this.toolStripComboBox1.SelectedIndex==-1)
            { MessageBox.Show("Не выбрана таблица");return; }
            this.WindowSize(this.toolStripComboBox1.SelectedItem.ToString()); // меняем размеры окна и соответствующие контролы
          
            if (F1.ShowDialog() == DialogResult.OK)
            {
                this.Db_connection.Open();
                switch (this.toolStripComboBox1.SelectedItem.ToString())
                {
                    case "Cinemas":
                        this.Db_comand.CommandText = string.Format("INSERT INTO {0} (name,number_rows,number_seats) VALUES ('{1}',{2},{3})",
                                this.toolStripComboBox1.SelectedItem.ToString(), this.F1.textBox1.Text,int.Parse(this.F1.textBox2_H.Text), int.Parse(this.F1.textBox3_M.Text));
                        try { this.Db_comand.ExecuteNonQuery(); } //для обновления БД, вызываем после каждого запроса
                        catch (Exception e1) { MessageBox.Show(e1.Message); }
                        this.Db_connection.Close();
                        this.ClearAndFill();
                        this.Admin_Cinemas(); // заполняем админские таблицы
                        break;
                    case "Genre":
                        this.Db_comand.CommandText = string.Format("INSERT INTO {0} (name) VALUES ('{1}')",
                                this.toolStripComboBox1.SelectedItem.ToString(), this.F1.textBox1.Text);
                        try { this.Db_comand.ExecuteNonQuery(); } //для обновления БД, вызываем после каждого запроса
                        catch (Exception e1) { MessageBox.Show(e1.Message); }
                        this.Db_connection.Close();
                        this.ClearAndFill();
                        this.Admin_Genre(); // заполняем админские таблицы
                        break;
                    case "Films":
                        // получаем id жанра по его имени из комбобокса
                        int id_Genre = 0;
                        var order = from T in this.D_set.Tables["Genre"].AsEnumerable()
                                    where T.Field<string>("name") == this.F1.comboBox1.SelectedItem.ToString()
                                    select new { id_nameg = T.Field<int>("id") };
                         id_Genre=order.Single().id_nameg; // получаем id_Genre

                        // получаем продолжительность фильма из текстбоксов
                        TimeSpan ft = new TimeSpan(int.Parse(this.F1.textBox2_H.Text), int.Parse(this.F1.textBox3_M.Text),0);

                        // формируем запрос
                        this.Db_comand.CommandText = string.Format("INSERT INTO {0} (name,id_Genre,duration) VALUES ('{1}',{2},'{3}')",
                                this.toolStripComboBox1.SelectedItem.ToString(),this.F1.textBox1.Text,id_Genre,ft);
                        try { this.Db_comand.ExecuteNonQuery(); } 
                        catch (Exception e1) { MessageBox.Show(e1.Message); }
                        this.Db_connection.Close();
                        this.ClearAndFill();
                        this.Admin_Films();
                        break;
                    case "Sesions":
                        // получаем id фильма по его имени из комбобокса
                        int id_Film = 0;
                        var order1 = from T in this.D_set.Tables["Films"].AsEnumerable()
                                     where T.Field<string>("name") == this.F1.comboBox2_Sfilms.SelectedItem.ToString()
                                     select new { id_namef = T.Field<int>("id")};
                        id_Film = order1.Single().id_namef; // получаем id_Genre

                        // получаем id кинотеатра 
                        int id_Cinema = 0;
                        var order2 = from T in this.D_set.Tables["Cinemas"].AsEnumerable()
                                     where T.Field<string>("name") == this.F1.comboBox1.SelectedItem.ToString()
                                     select new { id_namec = T.Field<int>("id") };
                        id_Cinema = order2.Single().id_namec; // получаем id_Cinema

                        // получаем время сеанса
                         TimeSpan  st = new TimeSpan(int.Parse(this.F1.textBox2_H.Text), int.Parse(this.F1.textBox3_M.Text), 0);
                       
                        // количество сеансов
                        TimeSpan countsessions;
                        DateTime DT = DateTime.Now;// сегоднешняя дата
                        DateTime sessions = this.F1.dateTimePicker1.Value.Date; // дата до которой идет фильм
                        countsessions=sessions.Subtract(DT);
                        int count = countsessions.Days;
                        // перед добавление сеанса выбираем сеансы в конкретном кинотеатре у которых дата транслирования фильма меньше или равна дате до которой мы вввели(т.е потенциальных конкурентов) и время начала сеанса меньше или равно времени введенному
                        if (this.SessionTime(id_Cinema, sessions, st, DT))
                        {// формируем запрос
                            for (int i = 0; i < count + 2; i++)
                            {
                                this.Db_comand.CommandText = string.Format("INSERT INTO {0} (id_Film,id_Cinema,date_beginning,times) VALUES ({1},{2},'{3}','{4}')",
                                     this.toolStripComboBox1.SelectedItem.ToString(), id_Film, id_Cinema, DT.AddDays(i), st);
                                try { this.Db_comand.ExecuteNonQuery(); }
                                catch (Exception e1) { MessageBox.Show(e1.Message); }
                            }
                        }
                        else
                        {
                            string messadge = string.Format("В {0} {1} идут другие фильмы !!!", st, this.F1.dateTimePicker1.Value.ToShortDateString());
                            MessageBox.Show(messadge);
                        }
                        this.Db_connection.Close();
                        this.ClearAndFill();
                        this.Admin_Sesions();
                        break;
                }
            }
        }
        #endregion

    }
}

