﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Data.Common;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Cinemas_Exam
{
    public partial class Form1 : Form
    {
        private bool field1 = true;
        private bool field2 = true;
        private bool field3 = true;
        private bool field4 = true;
        private bool field5 = true;
        public bool field6 = true;
        public Form1()
        {
            InitializeComponent();
            this.comboBox1.Visible = false;
            this.textBox2_H.Visible = false;
            this.textBox3_M.Visible = false;
            this.dateTimePicker1.Visible = false;
            this.label1.Visible = false;
            this.comboBox2_Sfilms.Visible = false;
            this.FormClosing += Form1_FormClosing;

        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (this.DialogResult == DialogResult.OK)
            {
                // проверяем поля часов и минут на цифры
                string test=this.textBox2_H.Text+this.textBox3_M.Text;
                for (int i = 0; i < test.Length; i++)
                {
                    if (test[i] >= (char)48 && test[i] <= (char)57)
                    {
                        field6 = true;
                    }
                    else
                    {
                        field6 = false;
                        break;
                    } 
                }
                // проверяем заполнено или не заполнено
                if (this.textBox1.Text == "" && this.textBox1.Visible==true)
                {
                    this.errorProvider1_TB.SetError(this.textBox1, "Поле не заполнено!!!");
                    field1 = false;
                }
                else
                {
                    this.errorProvider1_TB.SetError(this.textBox1, "");
                    field1 = true;
                }

                if (this.comboBox2_Sfilms.SelectedIndex == -1&& this.comboBox2_Sfilms.Visible==true)
                {
                    this.errorProvider4_CBSfilm.SetError(this.comboBox2_Sfilms, "Поле не заполнено!!!");
                    field2 = false;
                }
                else
                {
                    this.errorProvider4_CBSfilm.SetError(this.comboBox2_Sfilms, "");
                    field2 = true;
                }

                if (this.comboBox1.SelectedIndex == -1 && this.comboBox1.Visible==true  )
                {
                    this.errorProvider5_CB1.SetError(this.comboBox1, "Поле не заполнено!!!");
                    field3 = false;
                }
                else
                {
                    this.errorProvider5_CB1.SetError(this.comboBox1, "");
                    field3 = true;
                }
              
                if (this.textBox2_H.Text == "" && this.textBox2_H.Visible==true)
                {
                    this.errorProvider2_TB_H.SetError(this.textBox2_H, "Поле не заполнено!!!");
                    field4 = false;
                }
                else
                {
                    this.errorProvider2_TB_H.SetError(this.textBox2_H, "");
                    field4 = true;
                }
                if (this.textBox3_M.Text == "" && this.textBox3_M.Visible==true)
                {
                    this.errorProvider3_TB_M.SetError(this.textBox3_M, "Поле не заполнено!!!");
                    field5 = false;
                }
                else
                {
                    this.errorProvider3_TB_M.SetError(this.textBox3_M, "");
                    field5 = true;
                }

                if (!field1 || !field2 || !field3 || !field4 || !field5|| !field6)
                {
                    MessageBox.Show("Поля введены не верно");
                     e.Cancel = true;
                }
            }
        }
    }
}



//private void tableLayoutPanel1_Paint(object sender, PaintEventArgs e)
//{

//}