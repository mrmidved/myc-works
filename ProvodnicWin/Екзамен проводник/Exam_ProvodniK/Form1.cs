﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Exam_ProvodniK
{
    public partial class Form1 : Form
    {   
        static private TreeNode _tn = new TreeNode(); // ветвь для изминения 
        private List<DirectoryInfo> Ldir = new List<DirectoryInfo>();
      
        //*** поля для пункта меню copy/past
        private string copyadres;                   // адрес файла или папки который копируют
        private string doubleClickInPast;           // адрес папки куда копируют
        private string key;                         // определяем что выбрано, файл или папка
        private string Fname;                       // имя файла
        public Form1()
        {
            InitializeComponent();
            string[] disks = Directory.GetLogicalDrives();          // получаем логические диски 

            this.treeView1.ImageList = this.imageList1;             // левому дереву назначаем imageList
            this.listView1.LargeImageList = this.imageList2;        // правой части окна (большим иконка) назначаем список иконок
            this.listView1.SmallImageList = this.imageList3;        // правой части окна (маленьким иконка) назначаем список иконок

            //*** заполняем дерево
            foreach (string d in disks)
            {
                TreeNode TnLogicalDisc = new TreeNode(d, 0, 1);     // создаем ветвь диска
                this.AddTreView(TnLogicalDisc, d);                  // запускаем метод по заполнению
                this.treeView1.Nodes.Add(TnLogicalDisc);            // добавляем ветвь (диск) в дерево(treeView)
            }
         
            this.treeView1.BeforeExpand += TreeView1_BeforeExpand;  // свойство происходит перед распахиванием ветви в treeView         
            this.treeView1.LineColor = Color.Blue;
			this.treeView1.AfterSelect += TreeView1_AfterSelect;    // после выделения элемента дерева, заполняем ListView
            this.treeView1.DoubleClick += TreeView1_DoubleClick;    // двойной клик по елементу дерева
          
            //*** подключаем контекстные меню
            this.treeView1.ContextMenuStrip = this.contextMenu_Tree;
            this.listView1.ContextMenuStrip = this.contextMenu_List;
			//*** обработчие клацания по столбцу при таблице (для сортировки)
			this.listView1.ColumnClick += ListView1_ColumnClick;
        }

		//************************* методы TreeView
		private void AddTreView(TreeNode tn, string diskname) // заполнияем TreViev
		{
			try
			{
				DirectoryInfo DF = new DirectoryInfo(diskname);
				DirectoryInfo[] Directory = DF.GetDirectories();

				foreach (DirectoryInfo d in Directory)
				{
					TreeNode TnDiskDirectorys = new TreeNode(d.Name, 0, 1);     // создаем ветвь диска (дочернюю ветвь)
					tn.Nodes.Add(TnDiskDirectorys);                                 // добаляем в ветвь диска ( пришедшую из параметра метода) новую ветвь(дочернюю)
					try
					{
						DirectoryInfo[] subDirectory = d.GetDirectories();          // получаем новый массив каталогов которые находяться в каталоге Directory
						if (subDirectory.Length > 0)                                // если есть каталоги в subDirectory (т.е если вложеные папки все таки существуют)
						{
							TreeNode TnTrickDirectory = new TreeNode("?", 0, 1);    // создаем обманную ветвь...
							TnDiskDirectorys.Nodes.Add(TnTrickDirectory);           // ... и добавляем ее в предыдущюю ветвь
						}
					}
					catch { }
				}
			}
			catch { }
		}
		private void TreeView1_BeforeExpand(object sender, TreeViewCancelEventArgs e)// ... перед распахиванием 
		{
			TreeNode TnReplace_TnTrickDirectory = e.Node;       // создаем новую ветвь и присваиваем ей обманную ветвь
			TnReplace_TnTrickDirectory.Nodes.Clear();           // чистим новую ветвь от знака "?"
			string name = TnReplace_TnTrickDirectory.FullPath;
			// string name = null;
			//string name = TnReplace_TnTrickDirectory.Text;      // свойство Text - получаем путь для подстановки в метод.FullPatsh - почемуто не работает???
			this.AddTreView(TnReplace_TnTrickDirectory, name);
		}
        private void TreeView1_DoubleClick(object sender, EventArgs e) // двойной клик по елементу дерева
        {
            try
            {
                string s = this.treeView1.SelectedNode.FullPath;
                DirectoryInfo df = new DirectoryInfo(s);
                DirectoryInfo[] d = df.GetDirectories();
                if (d.Length > 0)
                {
                    this.listView1.Clear();
                    this.AddListView(s);
                    _tn.Expand();
                }
                if (d.Length == 0)
                {
                    this.listView1.Items.Clear();
                    DirectoryInfo files = new DirectoryInfo(s);
                    FileInfo[] arrfiles = files.GetFiles();
                    for (int i = 0; i < arrfiles.Length; i++)
                    {
                        ListViewItem LVM = new ListViewItem(arrfiles[i].Name, 1);
                        string time = string.Format("{0}", arrfiles[i].CreationTime);
                        LVM.SubItems.Add(time);
                        LVM.SubItems.Add("Файл");
                        LVM.SubItems.Add(string.Format("{0:0.0} кБ", (int)arrfiles[i].Length * 0.001));      // размер файла
                        LVM.Tag = arrfiles[i].FullName;                                                      // добавляем  полный путь 
                        LVM.Name = "file";
                        this.listView1.Items.Add(LVM);
                    }
                }
            }
            catch
            {
                MessageBox.Show("Доступ закрыт", "Error!!!", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
        }
		private void TreeView1_AfterSelect(object sender, TreeViewEventArgs e) // после выделения элемента дерева, загружаем ListView
		{
			this.listView1.Items.Clear();
			string name = e.Node.FullPath; // получаем имя распахнутого каталога
            this.doubleClickInPast = name;// для метода copy/past записываем в поле адрес последней открытой папки
                                       // string name = e.Node.Text; // неправильный первый вариант
            _tn = e.Node;
			this.listView1.Columns.Clear();
			this.AddListView(name);
		}

		//************************* методы ListView
		private void AddListView(string adres) // заполняем ListView 
		{
			try
			{
				DirectoryInfo DF = new DirectoryInfo(adres);
				DirectoryInfo[] arrDirectory = DF.GetDirectories();
				FileInfo[] arrFile = DF.GetFiles();

				//*** добавляем колонки в ListView
				ColumnHeader CH1 = new ColumnHeader();
				CH1.Width = 200;
				CH1.Text = "Имя";

				ColumnHeader CH2 = new ColumnHeader();
				CH2.Width = 100;
				CH2.Text = "Дата создания";

				ColumnHeader CH3 = new ColumnHeader();
                CH3.Width = 100;
                CH3.Text = "Тип";

                ColumnHeader CH4 = new ColumnHeader();
                CH4.Width = 60;
				CH4.Text = "Размер";

                this.listView1.Columns.Add(CH1);
                this.listView1.Columns.Add(CH2);
                this.listView1.Columns.Add(CH3);
                this.listView1.Columns.Add(CH4);

                //*** заполняем listview папками
                for (int i = 0; i < arrDirectory.Length; i++)
				{
					ListViewItem LVD = new ListViewItem(arrDirectory[i].Name, 0);
					string time = string.Format("{0}", arrDirectory[i].CreationTime);        // получаем время создания файла для второй колонки в списке
					LVD.SubItems.Add(time);
					LVD.SubItems.Add("Папка с файлами");
					LVD.SubItems.Add(" ");
					LVD.Tag = arrDirectory[i].FullName;                                      // добавляем  полный путь 
                    LVD.Name = "dir";
                    this.listView1.Items.Add(LVD);
				}
                //*** заполняем listview файлами
                for (int i = 0; i < arrFile.Length; i++)
                {
                    ListViewItem LVM = new ListViewItem(arrFile[i].Name, 1);
                    string time = string.Format("{0}", arrFile[i].CreationTime);
                    LVM.SubItems.Add(time);
                    LVM.SubItems.Add("Файл");
                    LVM.SubItems.Add(string.Format("{0:0.0} кБ", (int)arrFile[i].Length * 0.001));      // размер файла
                    LVM.Tag = arrFile[i].FullName;                                                      // добавляем  полный путь 
                    LVM.Name = "file";
                    this.listView1.Items.Add(LVM);
                }
            }
			catch { }
		}
        private void listView1_DoubleClick(object sender, EventArgs e) // обработчик двойного клика по елементу в ListView
        {
            try
            {
                int index = this.listView1.SelectedIndices[0]; // индекс 
                string s = string.Format("{0}", this.listView1.SelectedItems[0].Tag); //получаем из тега елемента полный путь нажатого файла(папки)
                this.doubleClickInPast = s;// для метода copy/past записываем в поле адрес последней открытой папки
                string t = this.listView1.SelectedItems[0].Name;
				if (t == "file")
					Process.Start(s);
				if (t=="dir")
				{
					DirectoryInfo df = new DirectoryInfo(s);
					DirectoryInfo[] d = df.GetDirectories();

					if (d.Length > 0)
					{
						this.listView1.Clear();
						this.AddListView(s);
                      //  _tn.Nodes[index].Checked = true;
						_tn.Expand();                 
					}
					if (d.Length == 0)
					{
						this.listView1.Items.Clear();
						DirectoryInfo files = new DirectoryInfo(s);
						FileInfo[] arrfiles = files.GetFiles();
                        
                        for (int i = 0; i < arrfiles.Length; i++)
						{
							ListViewItem LVM = new ListViewItem(arrfiles[i].Name, 1);
							string time = string.Format("{0}", arrfiles[i].CreationTime);
							LVM.SubItems.Add(time);
							LVM.SubItems.Add("Файл");
							LVM.SubItems.Add(string.Format("{0:0.0} кБ", (int)arrfiles[i].Length * 0.001));      // размер файла
							LVM.Tag = arrfiles[i].FullName;                                                      // добавляем  полный путь 
                            LVM.Name = "file";
                            this.listView1.Items.Add(LVM);
						}
					}   
                }            
            }
            catch
            {
                 MessageBox.Show("Доступ закрыт", "Error!!!", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
            
           
        }
		private void ListView1_ColumnClick(object sender, ColumnClickEventArgs e)// обработчик клацания по столбцу для сортировки
		{
			this.listView1.ListViewItemSorter = new Sorting(e.Column);
		}
		
		//************************* методы ContextMenu
		private void CreateDirectory() // метод создания нового каталога
        {
            Form2 F2 = new Form2();
            string namedir = this.treeView1.SelectedNode.FullPath;
            if (F2.ShowDialog() == DialogResult.OK)
            {
                string _path = namedir + "/" + F2.textBox1.Text; // путь + имя папки
				if (Directory.Exists(_path))
				{
					MessageBox.Show("Папка c именем " + F2.textBox1.Text + " уже существует!!!");
					this.CreateDirectory();
				}
				else
				{
					Directory.CreateDirectory(_path);
					_tn = this.treeView1.SelectedNode.Parent;
					if (_tn != null)
					{
						int index = _tn.Index;
						_tn.Collapse();
					}				
				}
            }
            if(this.treeView1.SelectedNode.IsExpanded)
            {
                this.treeView1.SelectedNode.Collapse();
                this.treeView1.SelectedNode.Expand();
            }
            // *** очистка и перересовка
            this.listView1.Clear();
            this.AddListView(namedir);
        }
		private void DeletTree() // метод удаления в дереве
		{
			string namedir = this.treeView1.SelectedNode.FullPath;
			_tn = this.treeView1.SelectedNode.Parent; // записываем в нашу темповую ветку выделеный узел в дереве( его родительский узел)
			MessageBox.Show("Вы действительно хотите удалить "+ namedir+" ?");
			Directory.Delete(namedir,true);			
			int index = _tn.Index;// получаем из темповой ветки индекс выделенного елемента
            // *** очистка и перересовка
			_tn.Collapse();
            _tn.Expand();
			this.listView1.Clear();
            this.AddListView(this.treeView1.SelectedNode.FullPath);// заполняем лист содержимым предыдущей ветки
           
        } 
        private void DeletList() // метод удаления в листе
        {
            string namedir = string.Format("{0}", this.listView1.SelectedItems[0].Tag); //получаем из тега елемента полный путь нажатого файла(папки)
            int index = this.listView1.SelectedIndices[0];
            string file_dir = this.listView1.SelectedItems[0].Name;
            _tn = _tn.Nodes[index].Parent; // записываем в нашу темповую ветку выделеный узел в дереве( его родительский узел)
            MessageBox.Show("Вы действительно хотите удалить " + namedir + " ?");
            if (file_dir == "dir")
                Directory.Delete(namedir, true);
            else File.Delete(namedir);
            // *** очистка и перересовка
            if (_tn.IsExpanded)
            {
                _tn.Collapse();
                _tn.Expand();
            }
            this.listView1.Clear();
            this.AddListView(this.treeView1.SelectedNode.FullPath);// заполняем лист содержимым предыдущей ветки
        }
        private void NewNameTree() // метод переименовывания папки в дереве
        {
            Form2 F2 = new Form2();
            string namedir = this.treeView1.SelectedNode.Text; // старое имя
			string lastname1 =  this.treeView1.SelectedNode.Parent.FullPath;// путь до старого имени
			string adres1 = this.treeView1.SelectedNode.FullPath;// старый адрес
            _tn = this.treeView1.SelectedNode.Parent;
            F2.textBox1.Text = namedir;
            if (F2.ShowDialog() == DialogResult.OK)
            {
                string _path = lastname1+"\\"+F2.textBox1.Text; // новое имя папки
                if (Directory.Exists(_path))
                {
                    MessageBox.Show("Папка c именем " + F2.textBox1.Text + " уже существует!!!");
                    this.CreateDirectory();
                }
                else
                {
					Directory.Move(adres1, _path);
					_tn = this.treeView1.SelectedNode.Parent;
					if (_tn != null)
					{
						int index = _tn.Index;
                        // *** очистка и перересовка дерева
                        _tn.Collapse();
                        _tn.Expand();
                    }
				}
            }
            // *** очистка и перересовка листа
            this.listView1.Clear();
            this.AddListView(lastname1);
        }
		private void NewNameList() // метод переименовывания папки в листе
		{
			Form2 F2 = new Form2();
			string namedir = this.listView1.SelectedItems[0].Text;							// старое имя
			string adres1 = string.Format("{0}", this.listView1.SelectedItems[0].Tag);      // старый адрес			
			int slashindex=adres1.LastIndexOf('\\');
			string lastname1 = adres1.Remove(slashindex); 		
			F2.textBox1.Text = namedir;
			if (F2.ShowDialog() == DialogResult.OK)
			{
				string _path = lastname1 + "\\" + F2.textBox1.Text; // новое имя папки
				if (Directory.Exists(_path))
				{
					MessageBox.Show("Папка c именем " + F2.textBox1.Text + " уже существует!!!");
					this.CreateDirectory();
				}
				else
				{
					Directory.Move(adres1, _path);
                    // *** очистка и перересовка дерева
                    if (_tn.IsExpanded)
                    {
                        _tn.Collapse();
                        _tn.Expand();
                    }
				}
                // *** очистка и перересовка листа
                this.listView1.Clear();
				this.AddListView(lastname1);
			}
			
		}

        //*** методы copy/past ContextMenu
        private void Rec(DirectoryInfo DF, string patch)// получаем массив всех папок которые находяться в папке пришедшей по адреу
        {
            DirectoryInfo[] arrDirectorySize = DF.GetDirectories(); 
            foreach (DirectoryInfo d in arrDirectorySize)
            {
                this.Ldir.Add(d);
                this.Rec(d, d.FullName);
            }
        }
        private void RecSizeDirectory(string newfoldername) // вытягиваем все папаки
        {
            for (int i = 0; i < Ldir.Count; i++)
            {
                // *** забиваем скопированую папку папками
                string patch = Ldir[i].FullName;         // получам имя папки которую нужно скопировать
                string newpatch = patch.Remove(0, 3);    // удаляем с имени папки первые символы 
                string newname = newfoldername +"\\"+ newpatch;       // новое имя папки
                Directory.CreateDirectory(newname);// создаем там папку с таким же именем
               
                // *** забиваем папки файлами
                DirectoryInfo DF = new DirectoryInfo(patch); // проверяем есть ли файлы в той папке которую копируем 
                FileInfo[] arrFileinDirectory = DF.GetFiles();
                if (arrFileinDirectory.Length != 0)
                {
                    foreach (FileInfo f in arrFileinDirectory)
                    {
                        string filename = f.FullName;
                        string newfilename = newname+"\\"+ f.Name;
                        File.Copy(filename, newfilename);
                    }
                }
            }      
         
        }
        private void CopyList(string adres,string adres2, string key) // метод копирования папки в листе
		{
            try
            {
                if (key == "dir")
                {
                    //*** создаем копию самой папки               
                    string newfoldername = adres2; // имя основной папки копии

                    Directory.CreateDirectory(newfoldername);// создаем основную папку
                    DirectoryInfo DF = new DirectoryInfo(adres);
                    this.Rec(DF, adres); // извлекаем все папки из которой копируем в лист        
                    this.RecSizeDirectory(newfoldername);

                    //*** если есть файлы то копируем
                    FileInfo[] arrFiles = DF.GetFiles();
                    foreach (FileInfo f in arrFiles)
                    {
                        string filename = f.FullName;
                        string newpatch = filename.Remove(0, 3);
                        string newfilename = adres2 + "\\" + newpatch;
                        if (this.Ldir.Count == 0) // если в папке которую копируем нет вложенных папок а только файлы
                        {
                            string noLdir = this.copyadres.Remove(0, 3);    // удаляем с имени папки первые символы 
                            string newname = newfoldername + "\\" + noLdir;       // новое имя папки
                            Directory.CreateDirectory(newname);            // создаем основную папку
                        }
                        File.Copy(filename, newfilename);
                    }
                }
                if (key == "file")
                {
                    string newfilename = adres2 + "\\" + this.Fname; // новый адрес куда копировать
                    File.Copy(this.copyadres, this.doubleClickInPast + "\\" + this.Fname);
                }
                this.listView1.Clear();
                this.AddListView(adres2);
            }
            catch
            {
                if (key == "dir")
                    MessageBox.Show("Папка с таким именем уже существует ! ");
                else
                    MessageBox.Show("Файл с таким именем уже существует ! ");
            }
           
        }
        private void ToolStripMenuItemTreeView_Click(object sender, EventArgs e) // обработчик пунктов контекстного меню для TreeView
        {
            if (sender is ToolStripMenuItem)
            {
                ToolStripMenuItem TSMItree = (ToolStripMenuItem)sender;
                int index = int.Parse((string)TSMItree.Tag);
                switch (index)
                {
                    case 1: // - создать новый каталог
                        this.CreateDirectory();
                        break;
                    case 2: // - удалить каталог
						this.DeletTree(); break;
                    case 3:// - переименовать каталог
                        this.NewNameTree();
                        break;
                }

            }
        }
        private void ToolStripMenuItemListView_Click(object sender, EventArgs e) // обработчик пунктов контекстного меню для ListView
        {
            ToolStripMenuItem TSMIlist = (ToolStripMenuItem)sender;
            int index = int.Parse((string)TSMIlist.Tag);     
            try
            {
                if (sender is ToolStripMenuItem)
				{
					
                    if (index!=5)
                    {
                        switch (index)
                        {
                            case 1: // -создать новый каталог
                                this.CreateDirectory();
                                break;
                            case 2:// - удалить каталог
                                this.DeletList();
                                break;
                            case 3:// - переименовываем каталог
                                this.NewNameList();
                                break;
                            case 4:// - копировать
                                this.copyadres = string.Format("{0}", this.listView1.SelectedItems[0].Tag);
                                this.key = this.listView1.SelectedItems[0].Name; // определяем что выбрано, файл или папка
                                this.Fname = this.listView1.SelectedItems[0].Text;// название файла(если хотим копировать файл)
                                break;
                        }
                    }
                }
            }
            catch
            {
                MessageBox.Show(" Не выбран елемент ! ");
            }
            if (index==5) // - вставить
            {
                this.CopyList(this.copyadres, this.doubleClickInPast, key);
                this.Ldir.Clear(); // чистим главный список папок
            }

           

        }

        //************************* другие методы 
        private void toolStripComboBox1_SelectedIndexChanged(object sender, EventArgs e) // меняем отображение значков в меню комбобокса
        {
            int index = this.toolStripComboBox1.SelectedIndex;
            switch(index)
            {
                case 0: this.listView1.View = View.LargeIcon; break;
                case 1: this.listView1.View = View.SmallIcon; break;
                case 2: this.listView1.View = View.List; break;
                case 3: this.listView1.View = View.Details; this.listView1.GridLines = true; break;
                case 4: this.listView1.View = View.Tile; break;
            }
        }
        private void цветФонаToolStripMenuItem_Click(object sender, EventArgs e) // меняем цвет фона listviev
        {
            ColorDialog CD = new ColorDialog();
            CD.AllowFullOpen = true;
            CD.AnyColor = true;
            if (CD.ShowDialog() == DialogResult.OK)
                this.listView1.BackColor = CD.Color;
        }
        
    }
	class Sorting : IComparer
	{
		private int _col;

		public Sorting()
		{
			_col = 0;
		}
		public Sorting(int col)
		{
			_col = col;
		}
     
		public int Compare(object x, object y)
		{
            if(_col==1) // сортировка даты
            {
                ListViewItem LVX = (ListViewItem)x;
                string sx = LVX.SubItems[_col].Text;
                int index = sx.LastIndexOf(':');
                sx = sx.Remove(index-5);
                string[] Xarr = sx.Split('.');
               
                ListViewItem LVY = (ListViewItem)y;
                string sy = LVY.SubItems[_col].Text;
                int indey = sy.LastIndexOf(':');
                sy = sy.Remove(index - 5);
                string[] Yarr = sy.Split('.');

               if(int.Parse(Xarr[2]) != int.Parse(Yarr[2]))
                return (int.Parse(Xarr[2]) -int.Parse(Yarr[2]));
               if(int.Parse(Xarr[2]) == int.Parse(Yarr[2]))
                    return (int.Parse(Xarr[1]) - int.Parse(Yarr[1]));

            }
            if (_col == 3) // сортировка по размерам
            {
                double dx = 0;
                double dy=0;
                ListViewItem LVX = (ListViewItem)x;
                if (LVX.Name=="file")
                {
                    string sx = LVX.SubItems[_col].Text;
                    int index = sx.LastIndexOf(' ');
                    sx = sx.Remove(index);
                    dx = double.Parse(sx);
                }
                ListViewItem LVY = (ListViewItem)y;
                if (LVY.Name == "file")
                {
                    string sy = LVY.SubItems[_col].Text;
                    int index = sy.LastIndexOf(' ');
                    sy = sy.Remove(index);
                    dy = double.Parse(sy);
                }
                return ((int)dx - (int)dy);
            }

                return String.Compare(((ListViewItem)x).SubItems[_col].Text, ((ListViewItem)y).SubItems[_col].Text);
        }
	}
}
