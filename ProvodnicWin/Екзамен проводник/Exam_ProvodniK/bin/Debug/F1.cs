﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WF_H_W4_Country
{
    public partial class Form1 : Form
    {
        string[] arrCountry = { "Украина", "США", "Африка", "Англия", "Канада" };
        string[] arrSityUA = { "Киев", "Днепропетровск", "Запорожье", "Одесса", "Львов" };
        string[] arrSityUSA = { "Чикаго", "Калифорния", "Нью-Йорк", "Манхетан", "Бруклин" };
        string[] arrSityAfrica = { "Кейптаун", "Аддис-Абеба", "Найроби", "Каир", "Аккра" };
        string[] arrSityEngland = { "Лондог", "Бирмингем", "Шеффилд", "Манчестер", "Ливерпуль" };
        string[] arrSityCanada = { "Торонто", "Монреаль", "Ванкувер", "Эдмонтон", "Гамильтон" };

        public Form1()
        {
            InitializeComponent();
            this.comboBox1.Anchor = AnchorStyles.Left | AnchorStyles.Top | AnchorStyles.Right;
            this.comboBox2.Anchor = AnchorStyles.Left | AnchorStyles.Top | AnchorStyles.Right;
            for (int i = 0; i < arrCountry.Length; i++)
                this.comboBox1.Items.Add(arrCountry[i]);
            this.comboBox1.Sorted = true;
            this.comboBox1.SelectedIndexChanged += ComboBox1_SelectedIndexChanged; // выбираем страну   
        }

        private void ComboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            ComboBox CB = (ComboBox)sender;
            this.comboBox1.Tag = CB.SelectedItem;// записываем в тег название страны
            this.comboBox2.Items.Clear(); // перед записью в комбобокс городов предварительно очишаем от старых записей
            this.label3.Visible = false; // результат предыдущего выбора скрыт
            switch ((string)CB.SelectedItem)
            {
                case "Украина":
                    for (int i = 0; i < arrSityUA.Length; i++)
                        this.comboBox2.Items.Add(arrSityUA[i]);           
                    this.comboBox2.Sorted = true;
                    break;
                case "США":
                    for (int i = 0; i < arrSityUSA.Length; i++)
                        this.comboBox2.Items.Add(arrSityUSA[i]);
                    this.comboBox2.Sorted = true;
                    break;
                case "Африка":
                    for (int i = 0; i < arrSityAfrica.Length; i++)
                        this.comboBox2.Items.Add(arrSityAfrica[i]);
                    this.comboBox2.Sorted = true;
                    break;
                case "Англия":
                    for (int i = 0; i < arrSityEngland.Length; i++)
                        this.comboBox2.Items.Add(arrSityEngland[i]);  
                    this.comboBox2.Sorted = true;
                    break;
                case "Канада":
                    for (int i = 0; i < arrSityCanada.Length; i++)
                        this.comboBox2.Items.Add(arrSityCanada[i]);
                    this.comboBox2.Sorted = true;
                    break;
            }
            this.comboBox2.SelectedIndexChanged += ComboBox2_SelectedIndexChanged;
        }

        private void ComboBox2_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.label3.Visible = true;
            this.comboBox2.Tag = comboBox2.SelectedItem; // записываем в тег название города
            this.label3.Text ="страна - "+(string)this.comboBox1.Tag+"\nгород - "+(string)this.comboBox2.Tag;
        }
    }
}
