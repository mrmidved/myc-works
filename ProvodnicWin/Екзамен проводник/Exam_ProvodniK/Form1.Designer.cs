﻿namespace Exam_ProvodniK
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.treeView1 = new System.Windows.Forms.TreeView();
            this.listView1 = new System.Windows.Forms.ListView();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.toolStripComboBox1 = new System.Windows.Forms.ToolStripComboBox();
            this.цветФонаToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.imageList2 = new System.Windows.Forms.ImageList(this.components);
            this.imageList3 = new System.Windows.Forms.ImageList(this.components);
            this.contextMenu_Tree = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.создатьКаталогToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.удалитьКаталогToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.переименоватьКаталогToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.contextMenu_List = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.создатьКаталогToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.удалитьКаталогToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.переименоватьКаталогToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.копироватьToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.вставитьToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            this.contextMenu_Tree.SuspendLayout();
            this.contextMenu_List.SuspendLayout();
            this.SuspendLayout();
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.treeView1);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.listView1);
            this.splitContainer1.Panel2.Controls.Add(this.menuStrip1);
            this.splitContainer1.Size = new System.Drawing.Size(1145, 593);
            this.splitContainer1.SplitterDistance = 391;
            this.splitContainer1.TabIndex = 0;
            // 
            // treeView1
            // 
            this.treeView1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.treeView1.Location = new System.Drawing.Point(12, 50);
            this.treeView1.Name = "treeView1";
            this.treeView1.Size = new System.Drawing.Size(376, 531);
            this.treeView1.TabIndex = 0;
            // 
            // listView1
            // 
            this.listView1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.listView1.Location = new System.Drawing.Point(3, 50);
            this.listView1.Name = "listView1";
            this.listView1.Size = new System.Drawing.Size(735, 531);
            this.listView1.TabIndex = 0;
            this.listView1.UseCompatibleStateImageBehavior = false;
            this.listView1.DoubleClick += new System.EventHandler(this.listView1_DoubleClick);
            // 
            // menuStrip1
            // 
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(24, 24);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripComboBox1,
            this.цветФонаToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(750, 37);
            this.menuStrip1.TabIndex = 1;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // toolStripComboBox1
            // 
            this.toolStripComboBox1.Items.AddRange(new object[] {
            " - крупные значки\t",
            " - мелкие значки",
            " - список\t",
            " - таблица",
            " - черепица"});
            this.toolStripComboBox1.Name = "toolStripComboBox1";
            this.toolStripComboBox1.Size = new System.Drawing.Size(200, 33);
            this.toolStripComboBox1.SelectedIndexChanged += new System.EventHandler(this.toolStripComboBox1_SelectedIndexChanged);
            // 
            // цветФонаToolStripMenuItem
            // 
            this.цветФонаToolStripMenuItem.Name = "цветФонаToolStripMenuItem";
            this.цветФонаToolStripMenuItem.Size = new System.Drawing.Size(110, 33);
            this.цветФонаToolStripMenuItem.Text = "Цвет фона";
            this.цветФонаToolStripMenuItem.Click += new System.EventHandler(this.цветФонаToolStripMenuItem_Click);
            // 
            // imageList1
            // 
            this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList1.Images.SetKeyName(0, "icons8-Файл Filled-50.png");
            this.imageList1.Images.SetKeyName(1, "файл открыт.png");
            // 
            // imageList2
            // 
            this.imageList2.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList2.ImageStream")));
            this.imageList2.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList2.Images.SetKeyName(0, "1.jpeg");
            this.imageList2.Images.SetKeyName(1, "icons8-Файл Filled-50.png");
            // 
            // imageList3
            // 
            this.imageList3.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList3.ImageStream")));
            this.imageList3.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList3.Images.SetKeyName(0, "1.jpeg");
            this.imageList3.Images.SetKeyName(1, "icons8-Файл Filled-50.png");
            // 
            // contextMenu_Tree
            // 
            this.contextMenu_Tree.ImageScalingSize = new System.Drawing.Size(24, 24);
            this.contextMenu_Tree.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.создатьКаталогToolStripMenuItem,
            this.удалитьКаталогToolStripMenuItem,
            this.toolStripSeparator1,
            this.переименоватьКаталогToolStripMenuItem});
            this.contextMenu_Tree.Name = "contextMenu_Tree";
            this.contextMenu_Tree.Size = new System.Drawing.Size(278, 100);
            // 
            // создатьКаталогToolStripMenuItem
            // 
            this.создатьКаталогToolStripMenuItem.Name = "создатьКаталогToolStripMenuItem";
            this.создатьКаталогToolStripMenuItem.Size = new System.Drawing.Size(277, 30);
            this.создатьКаталогToolStripMenuItem.Tag = "1";
            this.создатьКаталогToolStripMenuItem.Text = "создать каталог";
            this.создатьКаталогToolStripMenuItem.Click += new System.EventHandler(this.ToolStripMenuItemTreeView_Click);
            // 
            // удалитьКаталогToolStripMenuItem
            // 
            this.удалитьКаталогToolStripMenuItem.Name = "удалитьКаталогToolStripMenuItem";
            this.удалитьКаталогToolStripMenuItem.Size = new System.Drawing.Size(277, 30);
            this.удалитьКаталогToolStripMenuItem.Tag = "2";
            this.удалитьКаталогToolStripMenuItem.Text = "удалить каталог";
            this.удалитьКаталогToolStripMenuItem.Click += new System.EventHandler(this.ToolStripMenuItemTreeView_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(274, 6);
            // 
            // переименоватьКаталогToolStripMenuItem
            // 
            this.переименоватьКаталогToolStripMenuItem.Name = "переименоватьКаталогToolStripMenuItem";
            this.переименоватьКаталогToolStripMenuItem.Size = new System.Drawing.Size(277, 30);
            this.переименоватьКаталогToolStripMenuItem.Tag = "3";
            this.переименоватьКаталогToolStripMenuItem.Text = "переименовать каталог";
            this.переименоватьКаталогToolStripMenuItem.Click += new System.EventHandler(this.ToolStripMenuItemTreeView_Click);
            // 
            // contextMenu_List
            // 
            this.contextMenu_List.ImageScalingSize = new System.Drawing.Size(24, 24);
            this.contextMenu_List.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.создатьКаталогToolStripMenuItem1,
            this.удалитьКаталогToolStripMenuItem1,
            this.toolStripSeparator3,
            this.переименоватьКаталогToolStripMenuItem1,
            this.toolStripSeparator2,
            this.копироватьToolStripMenuItem,
            this.вставитьToolStripMenuItem});
            this.contextMenu_List.Name = "contextMenu_List";
            this.contextMenu_List.Size = new System.Drawing.Size(278, 166);
            // 
            // создатьКаталогToolStripMenuItem1
            // 
            this.создатьКаталогToolStripMenuItem1.Name = "создатьКаталогToolStripMenuItem1";
            this.создатьКаталогToolStripMenuItem1.Size = new System.Drawing.Size(277, 30);
            this.создатьКаталогToolStripMenuItem1.Tag = "1";
            this.создатьКаталогToolStripMenuItem1.Text = "создать каталог";
            this.создатьКаталогToolStripMenuItem1.Click += new System.EventHandler(this.ToolStripMenuItemListView_Click);
            // 
            // удалитьКаталогToolStripMenuItem1
            // 
            this.удалитьКаталогToolStripMenuItem1.Name = "удалитьКаталогToolStripMenuItem1";
            this.удалитьКаталогToolStripMenuItem1.Size = new System.Drawing.Size(277, 30);
            this.удалитьКаталогToolStripMenuItem1.Tag = "2";
            this.удалитьКаталогToolStripMenuItem1.Text = "удалить каталог";
            this.удалитьКаталогToolStripMenuItem1.Click += new System.EventHandler(this.ToolStripMenuItemListView_Click);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(274, 6);
            // 
            // переименоватьКаталогToolStripMenuItem1
            // 
            this.переименоватьКаталогToolStripMenuItem1.Name = "переименоватьКаталогToolStripMenuItem1";
            this.переименоватьКаталогToolStripMenuItem1.Size = new System.Drawing.Size(277, 30);
            this.переименоватьКаталогToolStripMenuItem1.Tag = "3";
            this.переименоватьКаталогToolStripMenuItem1.Text = "переименовать каталог";
            this.переименоватьКаталогToolStripMenuItem1.Click += new System.EventHandler(this.ToolStripMenuItemListView_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(274, 6);
            // 
            // копироватьToolStripMenuItem
            // 
            this.копироватьToolStripMenuItem.Name = "копироватьToolStripMenuItem";
            this.копироватьToolStripMenuItem.Size = new System.Drawing.Size(277, 30);
            this.копироватьToolStripMenuItem.Tag = "4";
            this.копироватьToolStripMenuItem.Text = "копировать";
            this.копироватьToolStripMenuItem.Click += new System.EventHandler(this.ToolStripMenuItemListView_Click);
            // 
            // вставитьToolStripMenuItem
            // 
            this.вставитьToolStripMenuItem.Name = "вставитьToolStripMenuItem";
            this.вставитьToolStripMenuItem.Size = new System.Drawing.Size(277, 30);
            this.вставитьToolStripMenuItem.Tag = "5";
            this.вставитьToolStripMenuItem.Text = "вставить";
            this.вставитьToolStripMenuItem.Click += new System.EventHandler(this.ToolStripMenuItemListView_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1145, 593);
            this.Controls.Add(this.splitContainer1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Form1";
            this.Text = "Provodnik++";
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            this.splitContainer1.Panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.contextMenu_Tree.ResumeLayout(false);
            this.contextMenu_List.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.TreeView treeView1;
        private System.Windows.Forms.ListView listView1;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripComboBox toolStripComboBox1;
        private System.Windows.Forms.ToolStripMenuItem цветФонаToolStripMenuItem;
        private System.Windows.Forms.ImageList imageList1;
        private System.Windows.Forms.ImageList imageList2;
        private System.Windows.Forms.ImageList imageList3;
        private System.Windows.Forms.ContextMenuStrip contextMenu_Tree;
        private System.Windows.Forms.ToolStripMenuItem создатьКаталогToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem удалитьКаталогToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem переименоватьКаталогToolStripMenuItem;
        private System.Windows.Forms.ContextMenuStrip contextMenu_List;
        private System.Windows.Forms.ToolStripMenuItem создатьКаталогToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem удалитьКаталогToolStripMenuItem1;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripMenuItem переименоватьКаталогToolStripMenuItem1;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripMenuItem копироватьToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem вставитьToolStripMenuItem;
    }
}

