﻿using SaperWcf_Server;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SaperWcf_Client
{
    public partial class Form1 : Form
    {
        // создаем соединение
        private static ChannelFactory<SaperWcf_Server.Game> factory = new ChannelFactory<SaperWcf_Server.Game>("MyConfig");
        private SaperWcf_Server.Game myClient;

        private string username="user";
        private int countOfFlag=100; 
        private bool check = true;
        private bool threadcheck = false;
        private Form2 F2 = new Form2();
        private SaperWcf_Server.GameState GS_Client;
        private ButtonState BS;
        private Button B;

        private TableLayoutPanel TLP; 
        private Button[] arrButton = new Button[225];
        private List<int> LlastClickButton = new List<int>();// список нажатых кнопок
        private List<int> Lbuttons = new List<int>();// получаем с сервера номера кнопок которые будем менять 
        private List<string> LbuttonsDemined = new List<string>();// получаем с сервера номера кнопок помеченные бомбой

        private delegate void Delegate();
        private Thread T;       // поток по проверке массива(поля игры) на сервере
        private Thread T2;      // поток по проверке состояния игры

        private int mycase; // предыдущее состояние игры(для победителя)
        public Form1()
        {
            InitializeComponent();
            this.Load += Form1_Load;
            textBox1.Text = this.countOfFlag.ToString();
        }
        private void Form1_Load(object sender, EventArgs e)
        {
            // Console.WriteLine("3+7 = {0}", this.myClient.s(3, 7));// проверка соединения с сервером
            this.CheskLogin();// проверяем логин и получаем игру в объект SaperWcf_Server.GameState GS_Client
            //this.CreateField(this.GS_Client); // из объекта  GS_Client получаем массив с минами
           
            //// запускаем поток по проверке массива (поля игры ) на сервере
            //this.T = new Thread(this.Run);
            //this.T.IsBackground = true;
            //Thread.Sleep(100);
            //this.T.Start(this.username);

            ////запускаем поток по проверке состояния игры
            //this. T2 = new Thread(this.RunState);
            //this.T2.IsBackground = true;
            //Thread.Sleep(100);
            //this.T2.Start(this.username);
        }
        private void CheskLogin() // метод запускает окно регистрации игрока и отправляет данные на сервер 
        {
            if(this.threadcheck)
                this.Visible = false;
            while (check)
            {
                if (this.F2.ShowDialog() == DialogResult.OK) 
                {
                    try
                    {
                        Console.WriteLine("Подключен1");
                        this.myClient = Form1.factory.CreateChannel(); // создаем соединение
                        Console.WriteLine("Подключен2");
                        this.GS_Client = this.myClient.LoginVerificationStart(this.F2.textBox1.Text); // отправляем запрос на проверку логина и создание игры
                        if(this.GS_Client!=null)
                        {
                            this.username = this.F2.textBox1.Text;
                            this.label4_Username.Text = this.username;
                            this.check = false;
                            if(this.threadcheck)
                            {
                                Console.WriteLine("Вырубаем потоки 1");
                                this.T.Abort();
                                this.T2.Abort();
                                this.threadcheck = false;
                                Console.WriteLine("Вырубаем потоки 2");
                                this.tableLayoutPanel1.Controls.Remove(this.TLP);
                            }
                            this.CreateField(this.GS_Client); // из объекта  GS_Client получаем массив с минами
                           // Console.WriteLine("************** Поле создано");
                            // запускаем поток по проверке массива (поля игры ) на сервере
                            this.T = new Thread(this.Run);
                            this.T.IsBackground = true;
                            Thread.Sleep(100);
                            this.T.Start(this.username);

                            //запускаем поток по проверке состояния игры
                            this.T2 = new Thread(this.RunState);
                            this.T2.IsBackground = true;
                            Thread.Sleep(100);
                            this.T2.Start(this.username);
                            Console.WriteLine("Запускаем потоки на клиенте");
                            this.Visible = true;
                        }
                        else
                            this.F2.textBox1.Text = "Login занят...";
                    }
                    catch
                    { this.F2.textBox1.Text = "Нет связи с сервером ..."; }
                }
                else
                {
                    this.Close();
                    break;
                }
            }
        }
        private void CreateField(GameState gs) // создаем поле 
        {
            this.TLP = new TableLayoutPanel();
            this.countOfFlag = 100;
            this.TLP.Anchor = AnchorStyles.Left | AnchorStyles.Right | AnchorStyles.Top | AnchorStyles.Bottom;
            this.TLP.RowCount = 15;
            this.TLP.ColumnCount = 15;

            this.TLP.AutoSizeMode = AutoSizeMode.GrowAndShrink;
            this.TLP.AutoSize = true;
            this.TLP.BackColor = Color.Aqua;
            this.tableLayoutPanel1.Controls.Add(this.TLP, 0, 1);
            for (int i = 0; i < (this.TLP.RowCount * this.TLP.ColumnCount); i++)
            {
                // объект для записи в тег кнопки
                this. BS = new ButtonState((i + 1), this.TLP.RowCount);
                this.BS.Isbomb = gs.arrfield[i];
                // сама кнопка
                this.B = new Button();
                this.B.Anchor = AnchorStyles.Left | AnchorStyles.Right | AnchorStyles.Top | AnchorStyles.Bottom;
                this.B.Size = new Size(22, 22);
                this.B.Text = gs.arrfield[i].ToString();// ******************
                this.B.Tag = BS;
                this.B.Enabled = true;
                this.B.Click += B_Click;
                this.B.MouseDown += B_MouseDown;
                this.B.Margin = new Padding(0, 0, 0, 0);
                this.arrButton[i] = this.B;
                this.TLP.Controls.Add(this.arrButton[i]);
            }
        }
        private void B_MouseDown(object sender, MouseEventArgs e) // метод помечает кнопки с минами и считает счет 
        {
            if (e.Button == MouseButtons.Right)
            {
                switch (this.GS_Client.state)
                {
                    case 1:// ход первого игрока
                        if (this.username == this.GS_Client.twousers[0])
                        {
                            Button B = (Button)sender;
                            ButtonState BS = (ButtonState)B.Tag;
                            this.countOfFlag--;
                            this.textBox1.Text = this.countOfFlag.ToString();
                            if (BS.Isbomb == '*')
                                this.myClient.Demined(this.username, BS.Num, true);
                            if (BS.Isbomb == '0')
                                this.myClient.Demined(this.username, BS.Num, false);
                        }
                        break;
                    case 2:// ход второго игрока 
                        if (this.username == this.GS_Client.twousers[1])
                        {
                            Button B = (Button)sender;
                            ButtonState BS = (ButtonState)B.Tag;
                            this.countOfFlag--;
                            this.textBox1.Text = this.countOfFlag.ToString();
                            if (BS.Isbomb == '*')
                                this.myClient.Demined(this.username, BS.Num, true);
                            if (BS.Isbomb == '0')
                                this.myClient.Demined(this.username, BS.Num, false);
                        }
                        break;
                }
            }
        }
        private void B_Click(object sender, EventArgs e) // обработчик нажатия кнопки  
        {
            if(this.GS_Client.state>0)
            {
                switch (this.GS_Client.state)
                {
                    case 1:// ход первого игрока
                        if (this.username == this.GS_Client.twousers[0])
                        {
                            Button B = (Button)sender;
                            ButtonState BS = (ButtonState)B.Tag;
                            this.LlastClickButton.Add(BS.Num);
                            B.BackColor = Color.White;
                            int bam = this.myClient.PlayerRun(this.username, BS.Num, BS.GetArrNeighbors());
                        }
                        break;
                    case 2:// ход второго игрока 
                        if (this.username == this.GS_Client.twousers[1])
                        {
                            Button B = (Button)sender;
                            ButtonState BS = (ButtonState)B.Tag;
                            this.LlastClickButton.Add(BS.Num);
                            B.BackColor = Color.White;
                            int bam = this.myClient.PlayerRun(this.username, BS.Num, BS.GetArrNeighbors());
                        }
                        break;
                }
            }
        }
        private void Run(object obj) // потоковый метод для обновления поля (сервер отваливается здесь!!!)
        {
            Delegate D = new Delegate(this.EditField);// основное обновление поля
            Delegate D2 = new Delegate(this.EditFieldDemined);// обновление меток с бомбами
           
            string name = (string)obj;
            while (true)
            {
                try// при отваливании сервера
                {
                    Console.WriteLine("1");
                    this.Lbuttons = this.myClient.ChangeField(name); // запрос на сервер об изминениях на поле
                    Console.WriteLine("2");
                    if (Lbuttons != null)
                        this.Invoke(D);
                    Thread.Sleep(100);

                    this.LbuttonsDemined = this.myClient.ChangeFieldDemined(name); // запрос на сервер об изминениях флагов с бомбам
                    if (LbuttonsDemined != null)
                        this.Invoke(D2);
                }
                catch
                {
                    if (this.GS_Client.state != 10) 
                    {
                        this.F2.textBox1.Text = "Нет связи с сервером...";
                        this.check = true;
                        this.threadcheck = true;
                        Delegate D3 = new Delegate(this.CheskLogin);
                        this.Invoke(D3);
                    }
                }
                Thread.Sleep(1000); 
            }
        }
        private void EditField() // метод обновляет поле 
        {
            // в возвращаемом листе первое значение:
            // (null) - ошибка 
            // 0 - рядом нет бомб
            // int (1 - 8) - количество рядом расположенных кнопок
            // все остальные значения - номера пустых кнопок соседей которые нужно открыть
                if (this.Lbuttons.Count > 0)
                {
                    // проверяем есть ли возле нажатой кнопки бомбы
                    try
                    {
                        if (this.Lbuttons[0] > 0)
                            this.arrButton[(this.LlastClickButton.Last() - 1)].Text = this.Lbuttons[0].ToString();
                        else
                            this.arrButton[(this.LlastClickButton.Last() - 1)].Text = "";

                        this.Lbuttons.RemoveAt(0);// удаляем первое значение
                    }
                    catch { }
                }

                int cntbombs;
                foreach (int k in this.Lbuttons)
                {
                    // ******* считаем количество бомб в пустых ячейках
                    cntbombs = 0;
                    Button B = this.arrButton[k - 1];
                    ButtonState BS = (ButtonState)B.Tag;
                    cntbombs = this.myClient.CountBombsInField(this.username, k, BS.GetArrNeighbors());
                    // *******
                    for (int i = 0; i < this.arrButton.Length; i++)
                    {
                        if (i == k - 1)// индекс кнопки в массиве кнопок arrButton  = номер кнопки - 1
                        {
                            if (cntbombs == 0)
                                this.arrButton[i].Text = "";
                            else
                                this.arrButton[i].Text = cntbombs.ToString();
                            this.arrButton[i].BackColor = Color.White;
                            this.arrButton[i].Enabled = false;
                        }
                    }
                }
                this.myClient.Clear(); // запрос на чистку списка
                object block = new object();
                lock (block)
                { this.Lbuttons.Clear(); }
            
        }
        private void EditFieldDemined() // метод проставляет отмеченные бомбы на поле 
        {
                if (this.LbuttonsDemined.Count > 0)
                {
                    foreach (string k in this.LbuttonsDemined)
                    {
                        string[] s = k.Split(':');
                        Console.WriteLine(s[0]);
                        for (int i = 0; i < this.arrButton.Length; i++)
                        {
                            if (i == int.Parse(s[1]) - 1)// индекс кнопки в массиве кнопок arrButton  = номер кнопки - 1
                            {
                                if (s[0] == "R")
                                    this.arrButton[i].BackColor = Color.Red;
                                if (s[0] == "B")
                                    this.arrButton[i].BackColor = Color.Blue;
                                this.arrButton[i].Enabled = false;
                            }
                        }
                    }
                }
        }
        private void GetState() // обрабатываем состояние игры 
        {
            switch (this.GS_Client.state)
            {
                case 0:
                    this.label5_GameState.Text = "Ждем игрока";
                    break;
                case 1:// ход первого игрока
                    if (this.GS_Client.twousers.Count == 1) // добавляем второго игрока в коллекцию
                        this.GS_Client.twousers.Add(this.myClient.GetLastUsers(this.GS_Client.twousers[0]));
                    if (this.username == this.GS_Client.twousers[0])
                    {
                        this.label5_GameState.Text = "Ваш ход";
                    }
                    else
                    {
                        this.label5_GameState.Text = "Ходит "+ this.GS_Client.twousers[0];
                    }
                    break;
                case 2:// ход второго игрока 
                    if (this.username == this.GS_Client.twousers[1])
                    {
                        this.label5_GameState.Text = "Ваш ход";
                    }
                    else
                    {
                        this.label5_GameState.Text = "Ходит " + this.GS_Client.twousers[1];
                    }
                    break;
                case 10: // кто то взорвался 
                    this.label1.Text = "Очков за игру";
                    switch (this.mycase)
                    {
                        case 0:
                        case 1:// предыдущий ход первого игрока
                            if (this.username == this.GS_Client.twousers[0])
                            {
                                this.textBox1.Text = this.myClient.GetScore(this.username)[0].ToString();// количество очков первого игрока
                                this.label5_GameState.Text = "Бабах!";
                            }
                            else
                            {
                                this.label5_GameState.Text = "Выиграл " + this.GS_Client.twousers[1];
                                this.textBox1.Text = this.myClient.GetScore(this.username)[1].ToString();
                            }
                            break;
                        case 2:// предыдущий ход второго игрока 
                            if (this.username == this.GS_Client.twousers[1])
                            {
                                this.textBox1.Text = this.myClient.GetScore(this.username)[1].ToString();
                                this.label5_GameState.Text = "Бабах!";
                            }
                            else
                            {
                                this.textBox1.Text = this.myClient.GetScore(this.username)[0].ToString();
                                this.label5_GameState.Text = "Выиграл " + this.GS_Client.twousers[0];
                            }
                            break;
                    }
                   // показываем все мины
                    char c;
                    for (int i = 0; i < this.arrButton.Length; i++)
                    {
                        c = ((ButtonState)this.arrButton[i].Tag).Isbomb;// получаем значек бомбы
                        if (c == '*')
                            this.arrButton[i].BackColor = Color.Black;
                    }
                    Console.WriteLine("Останавливаем потоки1");
                    this.T.Abort();
                    this.T2.Abort();
                    Console.WriteLine("Останавливаем потоки2");
                    break;
                case 100: // отметили все мины 
                    this.label1.Text = "Очков за игру";
                    int score1 = this.myClient.GetScore(this.username)[0];
                    int score2 = this.myClient.GetScore(this.username)[1];
                    if (this.username == this.GS_Client.twousers[0])
                    {
                        this.textBox1.Text = score1.ToString(); // количество очков первого игрока
                        if (score1 > score2)
                            this.label5_GameState.Text = "Выиграл";
                        if (score1 < score2)
                            this.label5_GameState.Text = "Проиграл ";
                    }
                    if (this.username == this.GS_Client.twousers[1])
                    {
                        this.textBox1.Text = score2.ToString();  // количество очков второго игрока
                        if (score1 > score2)
                            this.label5_GameState.Text = "Проиграл";
                        if (score1 < score2)
                            this.label5_GameState.Text = "Выиграл";
                    }
                    // получаем минное поле(с клиента)
                    char c1;
                    for (int i = 0; i < this.arrButton.Length; i++)
                    {
                        c1 = ((ButtonState)this.arrButton[i].Tag).Isbomb;// получаем значек бомбы
                        if (c1 == '*')
                            this.arrButton[i].BackColor = Color.Yellow;
                    }
                    this.T.Abort();
                    this.T2.Abort();
                    break;
            }
            this.mycase = this.GS_Client.state;// предыдущее состояние игры
        }
        private void RunState(object obj) // потоковый метод получает состояние игры 
        {
            Delegate D = new Delegate(this.GetState);
            string name = obj.ToString();
            while (true)
            {
                try
                {
                    this.GS_Client.state = this.myClient.GsState(name);
                    this.Invoke(D);
                }
                catch { }
                Thread.Sleep(1500);
            }
        }
    }

    class ButtonState // состояние кнопки (объект класса передаем в тег кнопки) 
    {
        public char Isbomb { get; set; }            // *- бомба, 0- не бомба
        public int Num { get; }                    // номер кнопки
        private int[] arrneighbors = new int[] { -1, -1, -1, -1, -1, -1, -1, -1 };    // номера соседних кнопок

        public ButtonState(int num, int countinrows)
        {
            this.Num = num;
            switch (num)
            {
                case 1:
                    this.arrneighbors[4] = (this.Num + 1);                  // правая
                    this.arrneighbors[6] = (this.Num + countinrows);        // нижняя
                    this.arrneighbors[7] = (this.Num + countinrows) + 1;    // нижняя правая
                    break;
                case 15:
                    this.arrneighbors[3] = (this.Num - 1);                  // левая
                    this.arrneighbors[5] = (this.Num + countinrows) - 1;    // нижняя левая
                    this.arrneighbors[6] = (this.Num + countinrows);        // нижняя
                    break;
                case 210:
                    this.arrneighbors[1] = (this.Num - countinrows);        // верхняя
                    this.arrneighbors[2] = (this.Num - countinrows) + 1;    // верхняя правая
                    this.arrneighbors[4] = (this.Num + 1);                  // правая
                    break;
                case 225:
                    this.arrneighbors[0] = (this.Num - countinrows) - 1;    // верхняя левая
                    this.arrneighbors[1] = (this.Num - countinrows);        // верхняя
                    this.arrneighbors[3] = (this.Num - 1);                  // левая
                    break;
                default:
                    int num2 = num % 15;
                    if (num2 == 1)
                    {
                        this.arrneighbors[1] = (this.Num - countinrows);        // верхняя
                        this.arrneighbors[2] = (this.Num - countinrows) + 1;    // верхняя правая
                        this.arrneighbors[4] = (this.Num + 1);                  // правая
                        this.arrneighbors[6] = (this.Num + countinrows);        // нижняя
                        this.arrneighbors[7] = (this.Num + countinrows) + 1;    // нижняя правая
                        break;
                    }
                    if (num2 == 0)
                    {
                        this.arrneighbors[0] = (this.Num - countinrows) - 1;    // верхняя левая
                        this.arrneighbors[1] = (this.Num - countinrows);        // верхняя
                        this.arrneighbors[3] = (this.Num - 1);                  // левая
                        this.arrneighbors[5] = (this.Num + countinrows) - 1;    // нижняя левая
                        this.arrneighbors[6] = (this.Num + countinrows);        // нижняя
                        break;
                    }
                    if (num < 15)
                    {
                        this.arrneighbors[3] = (this.Num - 1);                  // левая
                        this.arrneighbors[4] = (this.Num + 1);                  // правая
                        this.arrneighbors[5] = (this.Num + countinrows) - 1;    // нижняя левая
                        this.arrneighbors[6] = (this.Num + countinrows);        // нижняя
                        this.arrneighbors[7] = (this.Num + countinrows) + 1;    // нижняя правая
                        break;
                    }
                    if (num > 210)
                    {
                        this.arrneighbors[0] = (this.Num - countinrows) - 1;    // верхняя левая
                        this.arrneighbors[1] = (this.Num - countinrows);        // верхняя
                        this.arrneighbors[2] = (this.Num - countinrows) + 1;    // верхняя правая
                        this.arrneighbors[3] = (this.Num - 1);                  // левая
                        this.arrneighbors[4] = (this.Num + 1);                  // правая
                        break;
                    }
                    if (num > 15 && num < 210)
                    {
                        //эталон
                        this.arrneighbors[0] = (this.Num - countinrows) - 1;    // верхняя левая
                        this.arrneighbors[1] = (this.Num - countinrows);        // верхняя
                        this.arrneighbors[2] = (this.Num - countinrows) + 1;    // верхняя правая
                        this.arrneighbors[3] = (this.Num - 1);                  // левая
                        this.arrneighbors[4] = (this.Num + 1);                  // правая
                        this.arrneighbors[5] = (this.Num + countinrows) - 1;    // нижняя левая
                        this.arrneighbors[6] = (this.Num + countinrows);        // нижняя
                        this.arrneighbors[7] = (this.Num + countinrows) + 1;    // нижняя правая
                        break;
                    }
                    break;
            }
        }
        public int[] GetArrNeighbors()
        {
            return this.arrneighbors;
        }
    }
}

namespace SaperWcf_Server
{
    [DataContract]
    class GameState
    {
        [DataMember]
        public int state;   // состояние игры

        [DataMember]
        public List<string> twousers = new List<string>();  // массив игроков

        [DataMember]
        public int numgames;

        [DataMember]
        public int scoreUser1 = 0;// счет игрока 1

        [DataMember]
        public int scoreUser2 = 0;// счет игрока 2

        [DataMember]
        public char[] arrfield = new char[255];// поле игры
    }
    [ServiceContract]
    interface Game
    {
        //[OperationContract]
        //int s(int a, int b);   // проверка соединения с сервером

        [OperationContract]
        GameState LoginVerificationStart(string login);// создание игры и проверка логина

        [OperationContract]
        int PlayerRun(string username, int numButton, int[] arr);// ход игрока

        [OperationContract]
        List< int> ChangeField(string username);// возврашаем номера кнопок которые нужно изминить

        [OperationContract]
        void Clear(); // чистим список с количеством бомб и номерами кнопок на сервере

        [OperationContract]
        int GsState(string username); // получаем состояние текущей игры

        [OperationContract]
        string GetLastUsers(string username);// получаем имя второго узера(шоб его...)

        [OperationContract]
        int CountBombsInField(string username, int numButton, int[] arr); // количество бомб в пустых клетках

        //[OperationContract]
        //List<int> Winner(string username);// в случае подрыва выявляем победителя и скрываем поле

        [OperationContract]
        void Demined(string username, int numButton, bool isBomb);// помечаем в массиве помеченные бомбы 

        [OperationContract]
        List<string> ChangeFieldDemined(string username); // потоковый метод возвращает номера кнопок помеченных как бомба

        [OperationContract]
        List<int> GetScore(string username);// получаем счет игроков 
    }
}