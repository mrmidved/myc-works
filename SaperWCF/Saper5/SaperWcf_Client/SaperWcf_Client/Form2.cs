﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SaperWcf_Client
{
    public partial class Form2 : Form
    {
        public Form2()
        {
            InitializeComponent();
            this.FormClosing += Form2_FormClosing;
        }

        private void Form2_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (this.DialogResult == DialogResult.OK)
            {
                if (this.textBox1.Text == "" || this.textBox1.Text == " ")
                {
                    this.errorProvider1.SetError(this.textBox1, "Поле не заполнено!!!");
                    e.Cancel = true;
                }
                else
                {
                    this.errorProvider1.SetError(this.textBox1, "");
                }
            }
        }
    }
}
