﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace SaperWcf_Server
{
    [DataContract]
    class GameState
    {
        [DataMember]
        public int state;   // состояние игры:
                            // 0 - ожидание подключения 2го игрока
                            // 1 - ход первого игрока
                            // 2 - ход второго игрока
                            // 10 - взорвался
                            // 100 - все мины помечены
        [DataMember]
        public List<string> twousers = new List<string> ();  // массив игроков

        [DataMember]
        public int numgames;

        [DataMember]
        public int scoreUser1 = 0;// счет игрока 1

        [DataMember]
        public int scoreUser2 = 0;// счет игрока 2

        [DataMember]
        public char[] arrfield = new char[225]; // поле игры
    }
    [ServiceContract]
    class Game
    {
        private Random R = new Random();
        private GameState GS;
        private int countbombs = 100;
        private List<int> ListReturnNum = new List<int>();// в список записываем измененные елементы поля (и возврашаем клиенту)
        private List<int> LlastCntBomb = new List<int>(); // ходы в игре
        private List<string> LDemined = new List<string>();//записываем номера с кнопок с помеченными бомбами
        // private Dictionary<int, GameState> DGS = new Dictionary<int, GameState>(); //  игра и ее номер( в класс Program!!!!! -Павлик, ты балван, это общий ресурс)
        //*****************************************************************
        //[OperationContract]
        //public int s(int a, int b)// проверка соединения с сервером
        //{ return a + b; }
        //*****************************************************************
        [OperationContract]
        public GameState LoginVerificationStart(string login) // создание игры и проверка логина 
        {
            Console.WriteLine("Метод создания игры: проверка логина {0}", login);
            //*********************** прооверяем логин
            foreach (GameState gs in Program.DGS.Values)
                if (gs.twousers.Count > 0)
                {
                    for (int i = 0; i < gs.twousers.Count; i++)
                    {
                        if (gs.twousers[i] == login)
                            return null;
                    }
                }
            //*********************** записываем логин и время создания юзера в словарь времени
            object block = new object(); 
            lock (block)//************** lock on
            {
                Program.EA.WaitOne();
                Program.EM.Reset();
                for (int i = 0; i < Program.minS; i++)
                    Program.S.WaitOne();
                Program.EA.Set();
                Program.EM.Set();//**********************************************
               // Program.minS++;//счетчик открытых каналов
                Console.WriteLine("Метод создания игры: записываем логин и время создания юзера в словарь времени {0}", login);
                Program.TimerUsers.Add(login, DateTime.Now.ToLongTimeString());
                for (int i = 0; i < Program.minS; i++)// закрываем на один меньше
                    Program.S.Release();//**********************************************
                Thread.Sleep(100);
            }
            //*********************** проверяем есть ли игра с ожидающим игроком
            Console.WriteLine("Метод создания игры: проверяем есть ли игра с ожидающим игроком {0}", login);
            foreach (GameState gs in Program.DGS.Values)
            {
                if (gs.twousers.Count == 1)
                {
                    gs.twousers.Add(login);         // добавляем второго игрока в игру
                    gs.state = 1;                   // состояние -  ход первого игрока 
                    Console.WriteLine("Метод создания игры: игра есть, это первая игра {0} подключается ", login);
                    return gs;
                }
                if (gs.twousers.Count == 3)
                {
                    Console.WriteLine("Первый игрок отпал");
                    Thread.Sleep(2000);
                    gs.twousers.RemoveRange(0, 2);
                    gs.twousers.Insert(0, login);
                    if(gs.state==1)
                        gs.state = 2;
                    else gs.state = 1;
                    Console.WriteLine("Метод создания игры: подключение после выхода предыдущего игрока {0} подключается к{1} ", login, gs.twousers[0]);
                    return gs;
                }
            }
            //*********************** если нет игры с ожидающим игроком
            Console.WriteLine("Метод создания игры: создает игру {0}", login);
            this.GS = new GameState();              // создаем игру 
            this.GS.twousers.Add(login);            // добавляем первого игрока 
                                                    // создаем минное поле
            for (int i = 0; i < this.GS.arrfield.Length; i++)// row
            {
                if (i < this.countbombs)
                    this.GS.arrfield[i] = '*';
                else
                    this.GS.arrfield[i] = '0';
            }

            //перемешиваем мины
            for (int j = this.GS.arrfield.Length - 1; j > 0; j--)
            {
                int index = R.Next(j + 1);
                char temp = this.GS.arrfield[index];
                this.GS.arrfield[index] = this.GS.arrfield[j];
                this.GS.arrfield[j] = temp;
            }
            // добавляем игру
            int key;
            if (Program.DGS.Count > 0)
                key = Program.DGS.Keys.Max() + 1;
            else key = 1;
            this.GS.numgames = key;                 // номер игры
            this.GS.state = 0;                      // состояние -  ждем
            Program.DGS.Add(key, this.GS);          // добавляем в словарь игр
            Console.WriteLine("Метод создания игры: созданную {0} игру добавили в словарь", login);

            return this.GS;                         // возврашаем вновь созданную игру
        }

        [OperationContract]
        public int PlayerRun(string username, int numButton, int[] arr) // (ЧМ-DGS) ход игрока 
        {
            GameState state=null;
            bool flag = false;
            // находим номер игры по логину игрока
            Program.EM.WaitOne();
            Program.S.WaitOne();
            foreach (GameState gs in Program.DGS.Values)
            {
                try///////вылет
                {
                    if (gs.twousers[0] == username || gs.twousers[1] == username)
                    {
                        state = gs;
                        flag = true;
                        if (gs.twousers[0] == username)
                            state.state = 2;
                        else state.state = 1;
                        break;
                    }
                }
                catch { }
            }
            Program.S.Release();
            Thread.Sleep(100);
            if (!flag)
                return -1; // если непонятно кто - возврашаем ошибку
           
            // находим соответствующие кнопки в массиве игры (индекс кнопки в массиве=номер кнопки-1!)
            // если нажали бомбу
            if (state.arrfield[numButton - 1] == '*')
                state.state = 10;// состояние взорвался
            else
            {
                // считаем количество бомб рядом с нажатой кнопкой
               int cnt = 0;
                for (int i = 0; i < 8; i++)
                {
                    if (arr[i] > 0)
                    {
                        if (state.arrfield[arr[i] - 1] == '*')
                            cnt++;
                    }
                }
                this.LlastCntBomb.Add(cnt);// количество бомб возле нажатой кнопки
                if (cnt>0)
                    state.arrfield[numButton - 1] = char.Parse(cnt.ToString()); // нажатый елемент массива перезаписываем с 0=> на количество рядом расположенных бомб 
                else
                    state.arrfield[numButton - 1] ='-'; // если рядом нет бомб помечаем нажатую кнопку '-'
                // записываем номера пустых кнопок рядом с нажатой кнопкой
                for (int i = 0; i < 8; i++)
                {
                    if (arr[i] > 0)
                    {
                        if (state.arrfield[arr[i] - 1] == '0')// если у соседей нет бомб помечаем '-'
                            state.arrfield[arr[i] - 1] = '-';
                    }
                }
            }
            //Console.WriteLine("Клатц  -  " + username + " Состояние " + state.state+"№ "+ numButton);
            return state.state;
        }

        [OperationContract]
        public List<int> ChangeField(string username) // (ЧМ-DGS)+(ПМ-DT) потоковый метод возвращает номера кнопок которые нужно поменять 
        {
            GameState state = null;
            bool flag = false;
            // находим номер игры по логину игрока
            Program.EM.WaitOne();
            Program.S.WaitOne();
            foreach (GameState gs in Program.DGS.Values)///////// вылет
            {
                try
                {
                    if (gs.twousers[0] == username || gs.twousers[1] == username || gs.twousers[2] == username)
                    {
                        state = gs;
                        flag = true;
                        break;
                    }
                }
                catch { }
            }
            Program.S.Release();
            Thread.Sleep(100);
            Console.WriteLine("\t\t\t\t\t\tСигнал " + username);
            if (!flag) return null; // если непонятно кто - возврашаем ошибку
           
            // первое число в листе - количестао бомб рядом
            try {
                this.ListReturnNum.Add(this.LlastCntBomb.Last());
            }
            catch { }
            
            // заполняем список с измененными кнопками
            for (int i = 0; i < state.arrfield.Length; i++)
                if (state.arrfield[i] == '-')
                    this.ListReturnNum.Add(i + 1);

            //<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< проверяем жив ли игрок( блокировка - корзина )
            string key = null;
            object block = new object();
            lock (block)
            {
                Program.EA.WaitOne();
                Program.EM.Reset();
                for (int i = 0; i < Program.minS; i++)
                    Program.S.WaitOne();
                Program.EA.Set();
                Program.EM.Set();//**********************************************

                Console.WriteLine("<---------начало перезаписи игрока {0} ------------------", username);
                foreach (KeyValuePair<string, string> D in Program.TimerUsers)
                {
                    Console.WriteLine("User -{0}, time {1}", D.Key, D.Value);
                    if (username == D.Key)
                        key = D.Key;
                }
           
                if (key != null)
                        Program.TimerUsers.Remove(key);// удаляем со словаря
                    Program.TimerUsers.Add(key, DateTime.Now.ToLongTimeString());// перезаписываем с новым временем
                Console.WriteLine("---------конец перезаписи игрока {0} ------------------>", username);

                for (int i = 0; i < Program.minS; i++)//*************************
                    Program.S.Release();
                Thread.Sleep(100);
            }//<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
            return this.ListReturnNum;
        }

        [OperationContract]
        public void Clear() // чистим список с количеством бомб и номерами кнопок на сервере 
        {
            this.ListReturnNum.Clear();
        }

        [OperationContract]
        public int GsState(string username) // (ЧМ-DGS) получаем состояние текущей игры 
        {
            GameState state = null;
            bool flag = false;
            // находим номер игры по логину игрока
            Program.EM.WaitOne();
            Program.S.WaitOne();
            foreach (GameState gs in Program.DGS.Values)
            {
                try
                {
                    if (gs.twousers[0] == username || gs.twousers[1] == username)
                    {
                        state = gs;
                        flag = true;
                        break;
                    }
                }
                catch { }
            }
            Program.S.Release();
            Thread.Sleep(100);
            if (!flag) return -1; // если непонятно кто - возврашаем ошибку
            return state.state;
        }

        [OperationContract]
        public string GetLastUsers(string username) // дописываем игрока в спсок GS 
        {
            return this.GS.twousers[1];
        }

        [OperationContract ]
        public int CountBombsInField(string username, int numButton, int[] arr) // (ЧМ-DGS) метод считает количество бомб рядом с пустыми клетками 
        {
            int cnt = 0;
            GameState state = null;
            bool flag = false;
            // находим номер игры по логину игрока
            Program.EM.WaitOne();
            Program.S.WaitOne();
            foreach (GameState gs in Program.DGS.Values)
            {
                try////// вылет
                {
                    if (gs.twousers[0] == username || gs.twousers[1] == username)
                    {
                        state = gs;
                        flag = true;
                        break;
                    }
                }
                catch { }
                
            }
            Program.S.Release();
            Thread.Sleep(100);
          
            if (!flag)
                return -1; // если непонятно кто - возврашаем ошибку
                           // считаем количество бомб рядом с нажатой кнопкой

            for (int i = 0; i < 8; i++)
                if (arr[i] > 0)
                    if (state.arrfield[arr[i] - 1] == '*')
                        cnt++;
            return cnt;
        }

        [OperationContract]
        public void Demined(string username, int numButton, bool isBomb)// (ЧМ-DGS+Lock) помечаем в массиве помеченные бомбы 
        {
            GameState state = null;
            // находим номер игры по логину игрока
            Program.EM.WaitOne();
            Program.S.WaitOne();
            foreach (GameState gs in Program.DGS.Values)
            {
                if (gs.twousers[0] == username || gs.twousers[1] == username)
                {
                    state = gs;
                    if (gs.twousers[0] == username)
                        state.state = 2;
                    else state.state = 1;
                    break;
                }
            }
            Program.S.Release();
            Thread.Sleep(100);

           // Console.WriteLine("Метод помечаем в массиве помеченные бомбы   : закончили находим номер игры  {0}", username);
            // если бомба то увеличиваем счет того игрока кто ее пометил
            if (username == state.twousers[0] && isBomb)
                state.scoreUser1++;
            if (username == state.twousers[1] && isBomb)
                state.scoreUser2++;
           // Console.WriteLine("Первый игрок угадал - " + state.scoreUser1 + " бомб\n" + "Второй игрок угадал - " + state.scoreUser2 + " бомб");

            // помечаем в массиве поля что отмечено как бомба
            object block = new object();
            //Console.WriteLine("**********************"+username+"**********************"+ state.twousers[0]);
            //Console.WriteLine("**********************" + username + "**********************" + state.twousers[1]);
            lock (block)
            {
                if (username == state.twousers[0])
                    state.arrfield[numButton - 1] = '!';
                if (username == state.twousers[1])
                    state.arrfield[numButton - 1] = '^'; 
            }
            Thread.Sleep(100);
            // проверяем если найдены все мины (если вдруг с дуру обозначили все кнопки)
            int isBombWin = 0;
            for (int i = 0; i < state.arrfield.Length; i++)
            {
                if (state.arrfield[i] == '*')
                    isBombWin++;
            }
            //Console.WriteLine(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>" + isBombWin);
            if (isBombWin == 0) state.state = 100;
        }

        [OperationContract]
        public List<string> ChangeFieldDemined(string username) // (ЧМ-DGS) потоковый метод возвращает номера кнопок помеченных как бомба 
        {
            GameState state = null;
            bool flag = false;
            // находим номер игры по логину игрока
            Program.EM.WaitOne();
            Program.S.WaitOne();
            foreach (GameState gs in Program.DGS.Values)
            {
                try
                {
                    if (gs.twousers[0] == username || gs.twousers[1] == username)
                    {
                        state = gs;
                        flag = true;
                        break;
                    }
                }
                catch { }
            }
            Program.S.Release();
            Thread.Sleep(100);

            if (!flag) return null; // если непонятно кто - возврашаем ошибку

          
            // заполняем список с измененными кнопками
            for (int i = 0; i < state.arrfield.Length; i++)
            {
                if (state.arrfield[i] == '!')
                {
                    Console.Write(state.arrfield[i]);
                    this.LDemined.Add("B:" + (i + 1).ToString());
                }
                if (state.arrfield[i] == '^')
                {
                    Console.Write(state.arrfield[i]);
                    this.LDemined.Add("R:" + (i + 1).ToString());
                }
            }
            return this.LDemined;
        }

        [OperationContract]
        public List<int> GetScore( string username)// (ЧМ-DGS) получаем счет игроков 
        {
            List<int> Lscore = new List<int>();
            GameState state = null;
            bool flag = false;
            // находим номер игры по логину игрока
            Program.EM.WaitOne();
            Program.S.WaitOne();
            foreach (GameState gs in Program.DGS.Values)
            {
                if (gs.twousers[0] == username || gs.twousers[1] == username)
                {
                    state = gs;
                    flag = true;
                    break;
                }
            }
            Program.S.Release();
            Thread.Sleep(100);

            if (!flag) return null; // если непонятно кто - возврашаем ошибку
            Lscore.Add(state.scoreUser1);
            Lscore.Add(state.scoreUser2);
            return Lscore;
        }
    }

    class Program
    {
        static public Dictionary<int,GameState> DGS = new Dictionary<int, GameState>(); //  игра и ее номер
        static public Dictionary<string, string> TimerUsers = new Dictionary<string, string>();// в словарь записываем время жизни юзера (имя, время)

        static public EventWaitHandle EM = new EventWaitHandle(true, EventResetMode.ManualReset);
        static public EventWaitHandle EA = new EventWaitHandle(true, EventResetMode.AutoReset);
        static public int maxS = 100;
        static public int minS = 10;
        static public Semaphore S = new Semaphore(minS, maxS);
        static void Main(string[] args)
        {
            ServiceHost SvH = new ServiceHost(typeof(Game));
            SvH.Open();

            // поток по проверке жизни игроков
            Thread T = new Thread(Run);
            T.IsBackground = true;
            Thread.Sleep(100);
            T.Start();

            Thread T2 = new Thread(Run2);
            T2.IsBackground = true;
            Thread.Sleep(100);
            T2.Start();

            Console.WriteLine("Server run...");
            Console.ReadKey();
            SvH.Close();
        }
        static public void Run()// читающе -пишущий поток 
        {
            object block1 = new object();
            while (true)
            {
                DateTime DT = DateTime.Now;
                string dead = null;
               
                    Console.WriteLine("lock begin");
                lock (block1)//<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< 
                {
                    Program.EA.WaitOne();
                    Program.EM.Reset();
                    for (int i = 0; i < Program.minS; i++)
                        Program.S.WaitOne();
                    Program.EA.Set();
                    Program.EM.Set();//**********************************************
                    foreach (GameState gs in Program.DGS.Values)
                    {
                        for (int i = 0; i < gs.twousers.Count; i++)
                            Console.WriteLine("В словаре игр -{0}", gs.twousers[i]);
                    }
                    foreach (KeyValuePair<string, string> KV in Program.TimerUsers)
                    {
                        Console.WriteLine("В словаре таймера - {0}", KV.Key);
                    }
                    Console.WriteLine("Поиск...");
                    foreach (KeyValuePair<string, string> KV in Program.TimerUsers)
                    {
                        TimeSpan T = DT.Subtract(DateTime.Parse(KV.Value));
                        Console.WriteLine(T.TotalSeconds);
                        if (T.TotalSeconds > 15)
                        {
                            Console.WriteLine("\t\t\t************ RIP {0} **********", KV.Key);
                            // находим номер игры по логину игрока
                            foreach (GameState gs in Program.DGS.Values)
                            {
                                try
                                {
                                    if (gs.twousers[1] == KV.Key)
                                    {
                                        gs.twousers.Remove(KV.Key);
                                        break;
                                    }
                                    if (gs.twousers[0] == KV.Key)
                                    {
                                        gs.twousers.Insert(0, KV.Key);
                                        break;
                                    }
                                }
                                catch { }
                            }
                            dead = KV.Key;
                            //Program.minS--;
                        }
                    }

                    if (dead != null)
                    {
                        Console.WriteLine("ops");
                        Program.TimerUsers.Remove(dead);
                    }
                     
                    for (int i = 0; i < Program.minS; i++)//*************************
                        Program.S.Release();
                    Thread.Sleep(100);

                }//<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< 
                Console.WriteLine("lock end");
                Thread.Sleep(7500);
            }
        }
        static public void Run2()// читающе -пишущий поток
        {
            object block = new object();
            List<string> daedman = new List<string>();
            bool flag = false;
            int key = 0;
            while (true)
            {
                Console.WriteLine("Зачистка началась");
                lock (block)//<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< 
                {
                    Program.EA.WaitOne();
                    Program.EM.Reset();
                    for (int i = 0; i < Program.minS; i++)
                        Program.S.WaitOne();
                    Program.EA.Set();
                    Program.EM.Set();//**********************************************
                    foreach(GameState gs in Program.DGS.Values)
                    {
                        for (int i = 0; i < gs.twousers.Count; i++)
                        {
                            if (Program.TimerUsers.Count == 0)
                                daedman.Add(gs.twousers[i]);
                            else
                            { 
                                foreach (string name in Program.TimerUsers.Keys)
                                {
                                    flag = false;
                                    if (gs.twousers[i] == name)
                                    {
                                        flag = true;
                                        break;
                                    }
                                }
                                if (!flag)
                                {
                                    daedman.Add(gs.twousers[i]);
                                    flag = false;
                                }
                            }
                        }
                    }
                    foreach(string d in daedman)
                        Console.WriteLine("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"+d);

                    // чисти массив игроков
                    foreach (string s in daedman)
                    {
                        foreach (KeyValuePair<int, GameState> KV in Program.DGS)
                        {
                            GameState gs = KV.Value;
                            if (gs.twousers.Count() == 0)
                                key = KV.Key;
                            try
                            {
                                if (gs.twousers[0] == s || gs.twousers[1] == s)
                                {
                                    gs.twousers.Remove(s);
                                    Console.WriteLine("Удалили с главного словаря {0}", s);
                                    break;
                                }
                            }
                            catch { }

                        }
                    }
                    // провряем есть ли игры без игроков ( если есть удаляем из словаря )
                    foreach (KeyValuePair<int, GameState> KV in Program.DGS)
                    {
                        GameState gs = KV.Value;
                        if (gs.twousers.Count() == 0)
                            key = KV.Key;
                    }
                    if (key > 0)
                    {
                        Program.DGS.Remove(key);
                        key = 0;
                    }
                       
                    Console.WriteLine("Зачистка кончилась");
                    daedman.Clear();
                    for (int i = 0; i < Program.minS; i++)//*************************
                        Program.S.Release();
                    Thread.Sleep(20000);
                }
            }
        }
    }
}
