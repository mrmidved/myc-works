﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ConsoleApplicationXXX
{

    // ************************************ машинка
    class Car
    {
        private int _x { get; set; }
        private int _y { get; set; }
        public Car(int x, int y)
        {
            _x = x;
            _y = y;
        }
        public Car()
        {
            _x = 20;
            _y = 50;
        }
        public void Move(int x, int y, short lr)
        {
            _x = x;
            _y = y;

            switch (lr)
            {
                case 1: this.ShowCar(); break;
                case 2: this.ShowCarR(); break;
                case 3: this.ShowCarL(); break;
            }
        }
        public void Clear(int x, int y)
        {
            _x = x;
            _y = y;

            string[] car1 = { " ", " ", " " };
            string[] car2 = { " ", " ", " " };
            string[] car3 = { " ", " ", " " };
            string[] car4 = { " ", " ", " " };
            string[] car5 = { " ", " ", " " };
            string[][] car = new string[5][];
            car[0] = car1;
            car[1] = car2;
            car[2] = car3;
            car[3] = car4;
            car[4] = car5;

            Console.CursorTop = _x;
            foreach (string[] a in car)
            {
                Console.CursorLeft = _y;
                foreach (string b in a)
                {
                    Console.Write(b);
                }
                Console.WriteLine();
            }
        }

        public void ShowCar()
        {
            string[] car1 = { "*", "^", "*" };
            string[] car2 = { "0", "X", "0" };
            string[] car3 = { " ", "X", " " };
            string[] car4 = { "0", "X", "0" };
            string[] car5 = { " ", " ", " " };
            string[][] car = new string[5][];
            car[0] = car1;
            car[1] = car2;
            car[2] = car3;
            car[3] = car4;
            car[4] = car5;
       
            Console.CursorTop = _x;
            foreach (string[] a in car)
            {
                Console.CursorLeft = _y;
                foreach (string b in a)
                {
                    Console.Write(b);
                }
                Console.WriteLine();
            }

        }
        public void ShowCarR()
        {
            string[] car1 = { "*", "^", "*" };
            string[] car2 = { "/", "X", "/" };
            string[] car3 = { " ", "X", " " };
            string[] car4 = { "0", "X", "0" };
            string[] car5 = { "'", " ", "'" };
            string[][] car = new string[5][];
            car[0] = car1;
            car[1] = car2;
            car[2] = car3;
            car[3] = car4;
            car[4] = car5;

            Console.CursorTop = _x;
            foreach (string[] a in car)
            {
                Console.CursorLeft = _y;
                foreach (string b in a)
                {
                    Console.Write(b);
                }
                Console.WriteLine();
            }

        }
        public void ShowCarL()
        {
            string[] car1 = { "*", "^", "*" };
            string[] car2 = { "\\", "X", "\\" };
            string[] car3 = { " ", "X", " " };
            string[] car4 = { "0", "X", "0" };
            string[] car5 = { "'", " ", "'" };
            string[][] car = new string[5][];
            car[0] = car1;
            car[1] = car2;
            car[2] = car3;
            car[3] = car4;
            car[4] = car5;

            Console.CursorTop = _x;
            foreach (string[] a in car)
            {
                Console.CursorLeft = _y;
                foreach (string b in a)
                {
                    Console.Write(b);
                }
                Console.WriteLine();
            }

        }
    }
    // ************************** препятствие
    class Let
    {
        private int _Lx { get; set; }
        private int _Ly { get; set; }
        public Let(int x, int y)
        {
            _Lx = x;
            _Ly = y;
        }
        public Let() { }
        public void ShowLet()
        {
            Console.Write("██");
        }
    }

    // ************************** игровое поле

    public delegate void Numbers();
    //class Points
    //{
    //    //Random r = new Random();
    //    //private int p1;
    //    //private int p2;
    // static  public void Showpoints()
    //    {
    //        //p1 = r.Next(0, 61);
    //        //p2 = r.Next(1, 50);
    //        //Console.WriteLine("\t 0:{1}:{2}",p1,p2);
    //        //Console.WriteLine("Очки: ");
           
    //        Console.CursorLeft = 10;
    //        Console.Write("\t\t\t\t\tSped - 100");
    //        Console.CursorLeft = 10;
    //        Console.Write("\nSped - 200");

    //    }
    //}

    class GameField
    {
        public static int _hight = 270; // длина (высота) трассы 
        public static int _wight = 43;  // ширина трассы
        private int _GFx { get; set; }  // координаты расположения поля на экране
        private int _GFy { get; set; }

        public int _ix =_hight-1;    // начальный координаты курсора(машинки) на поле
        public int _jy = _wight / 2;

        private int[,] _field = new int[_hight, _wight];
       
        public GameField(int x, int y)
        {
            _GFx = x;
            _GFy = y;
        }
        public GameField() { }
        public void FieldInit(int x = 0, int y = 0) // ************************************** инициализация массива
        {
            _ix += x;
            _jy += y;
            if (_jy < 2) _jy = 2;
            if (_jy >39) _jy = 39;
         
            string str= ConfigurationManager.AppSettings["str"];
            FileStream FS = File.Open(str, FileMode.Open, FileAccess.Read);
            byte[] a = new byte[11610];
            int b = FS.Read(a, 0, a.Length);
            FS.Close();

            int k = 0;
            for (int i = 0; i < _hight; i++)
            {
                for (int j = 0; j < _wight; j++)
                {
                    if (a[k] == 10 || a[k] == 13)
                    {
                        k++;
                        continue;
                    }
                    else
                    {
                        if (i==_ix&j==_jy)
                            _field[i, j] = 2;                   
                        else
                            _field[i, j] = a[k];
                        k++;
                    }
                }               
            }
        }

        public void ShowField(Car Jeep, int pos1, int pos2)
        { 
            for (int i = pos1; i < pos2; i++)
            {
                Console.CursorLeft = _GFx;
                Console.CursorTop = _GFy + i;
                for (int j = 0; j < _wight; j++)
                {
                    if (_field[i, j] == 48)
                    {
                        Console.Write("*");
                    }
                    if (_field[i, j] == 49 || _field[i, j] == 50 || _field[i, j] == 51)
                    {
                        Console.Write(" ");
                    }
                    if (_field[i, j] == 52)
                    {
                        Console.Write("▓▓");
                    }

                    if (_field[i, j] == 2)
                    {              
                        if (_field[i, j - 1] == 49 || _field[i, j + 1] == 49)      // машинка едит прямо
                        {
                            Jeep.Move(_ix + _GFy / 2, _jy + _GFx - 1, 1);
                        }
                        if (_field[i, j - 1] == 50 || _field[i, j + 1] == 50)     // машинка поворачивает на право
                        {
                            Jeep.Move(_ix + _GFy / 2, _jy + _GFx - 1, 2);
                        }
                        if (_field[i, j - 1] == 51 || _field[i, j + 1] == 51)     // машинка поворачивает на лево
                        {
                            Jeep.Move(_ix + _GFy / 2, _jy + _GFx - 1, 3);
                        }
                        if (_field[i, j - 1] == 48 || _field[i, j + 1] == 48)    // по газону...
                        {
                            Jeep.Move(_ix + _GFy / 2, _jy + _GFx - 1, 1);
                        }
                        if (_field[i-1, j] == 52|| _field[i , j+1] == 52 || _field[i, j - 1] == 52)
                        {
                            Console.CursorLeft = 15;
                            Console.Beep();
                            Console.Write("BAM!!!");
                            Console.ReadKey();
                        }
                    }                  
                }
            }
        }
    }
        class Game
        {
            GameField GF = new GameField(0,5);
            Car Kia = new Car(0, 5);     
        public void Run(int x = 0)
            {
                int pos1 = GameField._hight-26;
                int pos2 = GameField._hight;
                GF.FieldInit(x);
                GF.ShowField(Kia, pos1, pos2);
            while (true)
                {
                GF.FieldInit(-1, 0);
                pos1--;
                pos2--;

                if (Console.KeyAvailable)
                    {
                        ConsoleKeyInfo CKI = Console.ReadKey(true);
                        if (CKI.Key == ConsoleKey.Escape)
                        {
                         GF._ix = GameField._hight - 1;
                         GF._jy = GameField._wight / 2;
                         Console.Clear();
                         break;
                        }
                        switch (CKI.Key)
                        {
                            case ConsoleKey.UpArrow:
                                GF.FieldInit(-1, 0);
                                pos1--;
                                pos2--;
                                break;
                            case ConsoleKey.DownArrow:
                                GF.FieldInit(1, 0);
                                pos1++;
                                pos2++;
                                break;
                            case ConsoleKey.LeftArrow:
                               GF.FieldInit(0, -1);
                                break;
                            case ConsoleKey.RightArrow:
                               GF.FieldInit(0, 1);
                                break;
                        }
                    }
                    Thread.Sleep(0);
                    if (pos1 == 0)
                    {
                        Console.Clear();
                        this.Run(GameField._hight - 26);
                    }                 
                GF.ShowField(Kia, pos1, pos2);
             }
           }
        }

        class Program
        {
        static void Main(string[] args)
        {
            Game first = new Game();
            while (true)
            {
                Console.Clear();
                Console.WriteLine("\t\t\t\t\tИграть - 1");      
                Console.WriteLine("\t\t\t\t\tВыход - 2");
                try
                {
                    char cheng = char.Parse(Console.ReadLine());
                    Console.WindowWidth = 42;
                    switch (cheng)
                    {                 
                        case '1': first.Run(); break;
                        case '2': return;
                    }
                }
                catch
                {
                    Console.WriteLine("Введите 1 или 2");
                }
            }


        }
    }
    
}






