﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Net.Sockets;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ClientMotos
{
    public partial class MainWindow : Window
    {
        private string ip;
        private int port;
        private string username="Vasya"; // логин пользователя
        private float course;// курс $ на сервере
        private float per; // процент прибыли продавца
        // таблица с товаром
        private DataTable DTProduct;
        // коллекция для DataGrid "Заказ"
        private ObservableCollection<Sale> LOrder = new ObservableCollection<Sale>();
        private ObservableCollection<Sale> LOrderTemp = new ObservableCollection<Sale>();
        // коллекция для хранения данных из комбобокса для поиска
        private List<string> LComboSerch = new List<string>();
        private ComboBox[] ArrComboboxSearchProduct;
        private TcpClient TSpC;
        private NetworkStream NS;
        private MemoryStream MS;

        private Window1 W = new Window1();
        private Label[] arrLablInOrder;
        public MainWindow()
        {
            InitializeComponent();
            if(this.W.ShowDialog()==true)
            {
                this.username = W.textBox_Name.Text;
                this.Title = "Hello " + this.username +"!!!";
                this.per = W.percented;
                this.Loaded += MainWindow_Loaded;
                this.DataGridView.SelectionChanged += DataGridView_SelectionChanged; // обработчик выбора картинки
                this.DataGridView.MouseDoubleClick += DataGridView_MouseDoubleClick; // обработчик выбора товара для продажи
                this.DataGridOrder.RowEditEnding += DataGridOrder_RowEditEnding;     // конец редактировани заказа (цена и количество)
                this.DataGridOrder.BeginningEdit += DataGridOrder_BeginningEdit;     // начало редактирования товара
                //this.DataGridOrder.AutoGeneratingColumn += DataGridOrder_AutoGeneratingColumn; // при автозаполнении задаем привязку для обновления ( при редактировании строки)
                // заполняем массив комбобоксов с поиском
                this.ArrComboboxSearchProduct = new ComboBox[] { this.combobox_Itemtype, this.combobox_CategoryName, this.combobox_ManufacturersName, this.combobox_ManufacturersTypeName, this.combobox_NameProduct };
                // массив с лейблами в меню заказа
                this.arrLablInOrder = new Label[] { this.label_Summ, this.label_Summ_count_grn, this.label_grn, this.label_Date, this.label_Date_content,this.label_Profit,this.label_Profit_count_grn,this.label_grn1 };
                // обработчики кнопок заказа
                this.button_Clear_order.Click += Button_order_Click;
                this.button_Sawe_order.Click+= Button_order_Click;
                this.LOrder.CollectionChanged += LOrder_CollectionChanged; // изминение суммы в лейбле при удалении товара
            }
            else
                this.Close();
        }
        #region ******************************************* Начало
        private void CreateDataTableProduct() // создаем колонки DataTable 
        {
            this.DTProduct = new DataTable();
            DTProduct.TableName = "Products";
            // колонки таблиц
            DataColumn id = new DataColumn("№", typeof(int));/////////////////////////////////////////
            id.Caption = "ID";
            DTProduct.Columns.Add(id);

            DataColumn nameProductDetail = new DataColumn("Товар", typeof(string));///////////////////////
            nameProductDetail.Caption = "Тип";
            DTProduct.Columns.Add(nameProductDetail);

            DataColumn cnt = new DataColumn("кол-во", typeof(int));///////////////////////
            cnt.Caption = "Кол-во";
            DTProduct.Columns.Add(cnt);

            DataColumn nameDelivery = new DataColumn("nameDelivery", typeof(string));
            nameDelivery.Caption = "Доставка";
            DTProduct.Columns.Add(nameDelivery);

            DataColumn price = new DataColumn("цена", typeof(float));//////////////////////
            price.Caption = "Цена закупка $";
            DTProduct.Columns.Add(price);

            DataColumn pichter = new DataColumn("pichter", typeof(string));
            id.Caption = "pichter";
            DTProduct.Columns.Add(pichter);

        }
        private void MainWindow_Loaded(object sender, RoutedEventArgs e) // FormLoad 
        {
            this.ip = ConfigurationManager.AppSettings["IP"];
            this.port = int.Parse(ConfigurationManager.AppSettings["PORT"]);
            // создаем соединение
            try
            {
                this.TSpC = new TcpClient(this.ip, this.port);
                this.NS = this.TSpC.GetStream();
                this.MS = new MemoryStream();
            }
            catch (Exception e1) { MessageBox.Show(e1.Message); }
            // создаем таблицу
            this.CreateDataTableProduct();
            this.RateText2.Content = this.GetCourse().ToString();// получаем курс $
            this.GetTableProduct();
        }
        private void GetTableProduct() // метод загружает таблицу с товаром 
        {
            try // на обмен данными
            {
                // ********** отправляем запрос на сервер
                byte[] a = Encoding.UTF8.GetBytes("GETPRODUCT|");
                NS.Write(a, 0, a.Length);
                byte[] buf = new byte[2048];
                // ********** получаем ответ от сервера
                do
                {
                    int cnt = NS.Read(buf, 0, buf.Length);
                    if (cnt == 0) throw new Exception("0 bytes received");
                    MS.Write(buf, 0, cnt);
                } while (NS.DataAvailable);

                // ********** обработка полученного ответа от сервера
                string result = Encoding.UTF8.GetString(MS.GetBuffer(), 0, (int)MS.Length);
                string[] arrResult = result.Split('!'); // достаем из ответа отправленную команду + OK
                if (arrResult[0] == "GETPRODUCT" && arrResult[1] == "OK")
                {
                    this.DTProduct.Clear();
                    string[] arrProduct = arrResult[2].Split('&');
                    for (int i = 0; i < arrProduct.Length - 1; i++)
                    {
                        string[] arr = arrProduct[i].Split('|');
                        // заполняем таблицу DataTable
                        DataRow DR = this.DTProduct.NewRow();
                        DR["№"] = arr[0];///////////////////////////
                        DR["товар"] = arr[1];///////////////////////
                        this.AddComboboxSerch(arr[1]); // вклиниваем заполнение комбобоксов с поиском
                        DR["Кол-во"] = arr[2];/////////////////////
                        DR["nameDelivery"] = arr[3];
                        DR["цена"] = string.Format("{0:F2}", float.Parse(arr[4]) * this.course);///////////////////////
                        DR["pichter"] = arr[5];
                        this.DTProduct.Rows.Add(DR); // добавляем строку в таблицу
                    }
                    this.DataGridView.ItemsSource = this.DTProduct.DefaultView;
                    //  колонки с доставкой и ссылка на картинку делаем невидимыми
                    this.DataGridView.Columns[3].Visibility = Visibility.Hidden;    // инфа по доставке
                    this.DataGridView.Columns[5].Visibility = Visibility.Hidden;    // путь картинок
                }
                else
                {
                    NS.Close();
                    MessageBox.Show("Не коректный ответ от сервера, соединение c сервером закрыто", "Ошибка", MessageBoxButton.OK, MessageBoxImage.Error);
                }
                MS.SetLength(0); // очишаем память
            }
            catch (Exception e1)
            {
                MessageBox.Show(e1.Message, "Ошибка 193", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
        private void B_Click(object sender, RoutedEventArgs e) // кнопка обновления таблицы с товаром 
        {
            // загружаем отправляем запрос и получаем таблицу
            this.GetTableProduct();
            this.RateText2.Content = this.GetCourse().ToString();// получаем курс $
        }
        #endregion

        #region ******************************************* Создание и редактирование заказа + кнопки обработки 
        private void DataGridView_MouseDoubleClick(object sender, MouseButtonEventArgs e)// обработчик выбора товара для продажи 
        {
            DataRowView RV = this.DataGridView.SelectedItem as DataRowView;
            if (RV != null)
            {
                DataRow DR = RV.Row;
                Sale sale = new Sale();
                sale.Id = (int)DR["№"];
                sale.ProductName = DR["товар"].ToString();
                sale.DeliveryName = DR["nameDelivery"].ToString();
                sale.SalerName = this.username;
                sale.CntProduct = 0;
                sale.SalePrice = ((float)DR["цена"]) + (float)1.0;
                sale.SaleSumm = 0;
                sale.ProfitAll = 0;
                sale.ProfitSaller = 0;
                sale.SaleDate = DateTime.Now.ToShortDateString();
                this.LOrder.Add(sale);
                this.DataGridOrder.ItemsSource = LOrder;
            }
        }
        private void DataGridOrder_BeginningEdit(object sender, DataGridBeginningEditEventArgs e)// начало редактирования заказа 
        {
            // показываем инфу в меню над заказом
            for (int i = 0; i < this.arrLablInOrder.Length; i++)
                this.arrLablInOrder[i].Visibility = Visibility.Visible;
            this.label_Date_content.Content = DateTime.Now.ToLongDateString();
        }
        private void InfInLabel()
        {
            // заносим общюю сумму и заработок в лейблы на панели меню 
            float a1 = 0, b1 = 0;
            foreach (Sale s in this.LOrder)
            {
                a1 += s.SaleSumm;
                b1 += s.ProfitSaller;
            }
            this.label_Summ_count_grn.Content = string.Format("{0:F2}", a1);
            this.label_Profit_count_grn.Content = string.Format("{0:F2}", b1);
        }
        private void DataGridOrder_RowEditEnding(object sender, DataGridRowEditEndingEventArgs e) // конец редактирования заказа 
        {
            Sale sale = e.Row.Item as Sale;
            int cntInsclad = 0;
            // находим количество товара на складе
            foreach (DataRow r in this.DTProduct.Rows)
                if ((int)r["№"] == sale.Id)
                    cntInsclad = (int)r["кол-во"];

            // по id ищем закупку
            float firstprice = 0;
            foreach (DataRow r in this.DTProduct.Rows)
            {
                if ((int)r["№"] == sale.Id)
                    firstprice = (float)r["цена"]; // закупка в грн уже в таблице на клиенте
            }

            // проверяем введенные данные
            if (sale.CntProduct == 0 || sale.SalePrice == 0 || sale.SalePrice < 0 || cntInsclad < sale.CntProduct || sale.SalePrice < firstprice)   // если введенные не верно вариант
            {
                sale.CancelEdit();
                string error = string.Format("Строка № {0} имеет неверный формат, проверьте заполняемые данные ", sale.Id);
                MessageBox.Show(error);
                this.InfInLabel();// корректируем данные лейблов с общей суммой и заработком продавца
                return;
            }
            sale.SaleSumm = (float)sale.CntProduct * sale.SalePrice;// в грн
            // считаем прибыль сервера в $
            float salesumUSD = sale.SaleSumm / this.course;// обшая стоимость единицы товара (цена+количество) в $
            float firstsalesummUSD = sale.CntProduct * (firstprice / this.course);// обшая закупочная стоимость для количества товара в $
            sale.ProfitAll = salesumUSD - firstsalesummUSD;// прибыль сервера в $
            // считаем прибыль продавца(процент от общей прибыли)
            string temp = string.Format("{0:F3}", ((sale.ProfitAll * this.course) * (this.per * (float)0.01))); // в грн
            sale.ProfitSaller = float.Parse(temp);  // в грн
            // имя продавца и дата продажи
            sale.SalerName = this.username;
            sale.SaleDate = DateTime.Now.ToShortDateString();
            this.InfInLabel(); // корректируем данные лейблов с общей суммой и заработком продавца
        }
        private void LOrder_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e) // изминение суммы в лейбле при удалении товара 
        {
            if (e.Action == NotifyCollectionChangedAction.Remove)// если удаление
            {
                Sale old = e.OldItems[0] as Sale;
                if (old.CntProduct != 0 || old.SalePrice != 0)
                    this.InfInLabel(); // корректируем данные лейблов с общей суммой и заработком продавца
            }
        }
        private void Button_order_Click(object sender, RoutedEventArgs e) // обработчик кнопок заказов 
        {
            Button B = sender as Button;
            if (B.Name == "button_Sawe_order")
            {
                MessageBoxResult result = MessageBox.Show("Отправить заказ ? ", "Заказ", MessageBoxButton.YesNo, MessageBoxImage.Question);
                if (result == MessageBoxResult.Yes)
                {
                    this.SaweOrder();
                    this.LOrder.Clear();
                }
            }
            if (B.Name == "button_Clear_order")
            {
                MessageBoxResult result = MessageBox.Show("Удалить все товары из заказа ?", "Удалить", MessageBoxButton.YesNo, MessageBoxImage.Question);
                if (result == MessageBoxResult.Yes)
                    this.LOrder.Clear();
            }
            // скрываем инфу в меню над заказом
            for (int i = 0; i < this.arrLablInOrder.Length; i++)
                this.arrLablInOrder[i].Visibility = Visibility.Hidden;
            // чистим общюю сумму заказа и общий заработок и лейбл
            this.label_Summ_count_grn.Content = "";
            this.label_Profit_count_grn.Content = "";
        }

        #endregion
    
        #region ******************************************* Комбобоксы с поиском 
        private bool IsContentInSerch(string str) // метод получает строку из таблицы DataSet возврашает true если содержит все строки(поисковые из комбобоксов поиска) из Lista 
        {
            for (int i = 0; i < this.LComboSerch.Count; i++)
            {
                if (str.Contains(this.LComboSerch[i]))
                    continue;
                else
                    return false;
            }
            return true;
        }
        private void AddComboboxSerch(string name) // метод заполняет комбобоксы с поиском 
        {
            string[] arrCombo = name.Split(' ');
            for (int j = 0; j < 4; j++)
            {
                if (!this.combobox_Itemtype.Items.Contains(arrCombo[0]))
                    this.combobox_Itemtype.Items.Add(arrCombo[0]);
                if (!this.combobox_CategoryName.Items.Contains(arrCombo[1]))
                    this.combobox_CategoryName.Items.Add(arrCombo[1]);
                if (!this.combobox_ManufacturersName.Items.Contains(arrCombo[2]))
                    this.combobox_ManufacturersName.Items.Add(arrCombo[2]);
                if (!this.combobox_ManufacturersTypeName.Items.Contains(arrCombo[3]) && arrCombo[3] != "")
                    this.combobox_ManufacturersTypeName.Items.Add(arrCombo[3]);
                if (!this.combobox_NameProduct.Items.Contains(arrCombo[4]))
                    this.combobox_NameProduct.Items.Add(arrCombo[4]);
            }
        }
        private void Button_Click(object sender, RoutedEventArgs e) // кнопка поиска по комбобоксам 
        {
            this.GetTableProduct();
            this.LComboSerch.Clear(); // чистим лист с поисковыми словами
            // считываем всю инфу из комбобоксов для поиска и заполняем лист
            for (int i = 0; i < this.ArrComboboxSearchProduct.Length; i++)
                if (this.ArrComboboxSearchProduct[i].SelectedItem != null)
                    this.LComboSerch.Add(this.ArrComboboxSearchProduct[i].SelectedItem.ToString());
            //... ищем
            DataTable temp = this.DTProduct.Clone();
            foreach (DataRow DR in this.DTProduct.Rows)
            {
                if (this.IsContentInSerch(DR["товар"].ToString()))
                {
                    DataRow R = temp.NewRow();
                    R["№"] = DR[0];//////////////////////////////////////////
                    R["товар"] = DR[1];//////////////////////////
                    R["кол-во"] = DR[2];/////////////////////////////////////
                    R["nameDelivery"] = DR[3];
                    R["цена"] = DR[4];//string.Format("{0:F2}",(float)DR[4]*this.course);////////////////////////////////////
                    R["pichter"] = DR[5];
                    temp.Rows.Add(R);
                }
            }
            // this.DTProduct.Clear();
            this.DataGridView.ItemsSource = temp.DefaultView;
            //  колонки с доставкой и ссылка на картинку делаем невидимыми
            this.DataGridView.Columns[3].Visibility = Visibility.Hidden;
            this.DataGridView.Columns[5].Visibility = Visibility.Hidden;
            this.RateText2.Content = this.GetCourse().ToString();// обновляем курс $
        }
        #endregion
        
        #region ******************************************* Запросы на сервер 
        private void SaweOrder() // отправляем заказ на сервер в БД 
        {
            try // на обмен данными
            {
                // ********** отправляем запрос на сервер
                string request = "SAWEORDER|";
                string id,product,delivery,saler,cnt,price,summ,profitall,profitsaler,date;
                foreach (Sale s in this.LOrder)
                {
                    id = s.Id.ToString() + "#";
                    product = s.ProductName.ToString() + "#";
                    delivery = s.DeliveryName.ToString() + "#";
                    saler = s.SalerName + "#";
                    cnt = s.CntProduct.ToString() + "#";
                    price = string.Format("{0:F2}#",(s.SalePrice/this.course));    // переводим в $
                    summ = string.Format("{0:F2}#", (s.SaleSumm / this.course));   // переводим в $
                    profitall = string.Format("{0:F2}#",s.ProfitAll);             // уже приходит в $
                    profitsaler = string.Format("{0:F2}#", (s.ProfitSaller / this.course)); // переводим в $
                    date = s.SaleDate + "#";
                    if (price == "0#"|| cnt == "0#")
                    {
                        MessageBox.Show(string.Format("В товаре под индексом {0} значение цены или количества\n товара указаны не верно ! ", id), "Ошибка", MessageBoxButton.OK, MessageBoxImage.Error);
                        return;
                    }
                    request +=(product+delivery+saler+cnt+price+summ+profitall+profitsaler+date)+"&";
                }
                // отправляем заказ на сервер
                byte[] a = Encoding.UTF8.GetBytes(request);
                NS.Write(a, 0, a.Length);
                byte[] buf = new byte[24];
                // ********** получаем ответ от сервера
                do
                {
                    int cnt1 = NS.Read(buf, 0, buf.Length);
                    if (cnt1 == 0) throw new Exception("0 bytes received");
                    MS.Write(buf, 0, cnt1);
                } while (NS.DataAvailable);

                // ********** обработка полученного ответа от сервера
                string result = Encoding.UTF8.GetString(MS.GetBuffer(), 0, (int)MS.Length);
                string[] arrResult = result.Split('!'); // достаем из ответа отправленную команду + OK
                if (arrResult[0] == "SAWEORDER" && arrResult[1] == "OK")
                {
                    MessageBox.Show("Заказ успешно оправлен", "Сообщение", MessageBoxButton.OK, MessageBoxImage.Information);
                }
                else
                {
                    NS.Close();
                    MessageBox.Show("Не коректный ответ от сервера, соединение c сервером закрыто", "Ошибка", MessageBoxButton.OK, MessageBoxImage.Error);
                }
                MS.SetLength(0); // очишаем память
            }
            catch (Exception e1)
            {
                MessageBox.Show(e1.Message, "Ошибка 149", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
        private float GetCourse()// отправляем запрос на курс $ 
        {
            try // на обмен данными
            {
                // ********** отправляем запрос на сервер
                byte[] a = Encoding.UTF8.GetBytes("GETRATE|");
                NS.Write(a, 0, a.Length);
                byte[] buf = new byte[48];
                // ********** получаем ответ от сервера
                do
                {
                    int cnt = NS.Read(buf, 0, buf.Length);
                    if (cnt == 0) throw new Exception("0 bytes received");
                    MS.Write(buf, 0, cnt);
                } while (NS.DataAvailable);

                // ********** обработка полученного ответа от сервера
                string result = Encoding.UTF8.GetString(MS.GetBuffer(), 0, (int)MS.Length);
                string[] arrResult = result.Split('!'); // достаем из ответа отправленную команду + OK
                if (arrResult[0] == "GETRATE" && arrResult[1] == "OK")
                {
                    this.course = float.Parse(arrResult[2]);
                }
                else
                {
                    NS.Close();
                    MessageBox.Show("Не коректный ответ от сервера, соединение c сервером закрыто", "Ошибка", MessageBoxButton.OK, MessageBoxImage.Error);
                }
                MS.SetLength(0); // очишаем память
            }
            catch (Exception e1)
            {
                MessageBox.Show(e1.Message, "Ошибка 209", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            return this.course;
        }
        private void GetPichter(string path)// отправляем  запрос на получение картинки 
        {
            try // на обмен данными
            {
                // ********** отправляем запрос на сервер
                byte[] a = Encoding.UTF8.GetBytes("GETPICHTER|"+path);
                NS.Write(a, 0, a.Length);
                byte[] buf = new byte[2048];

                //********************************* получаем ответ от сервера
                // сначала принимаем 4 байта  - длина
                BinaryReader BR = new BinaryReader(NS);
                int allCnt = BR.ReadInt32();    // сколько нужно получит байт
                int curCnt = 0;                 // сколько реально получено

                while (curCnt < allCnt)// пока не получили все байты
                {
                    int cnt = NS.Read(buf, 0, buf.Length);
                    if (cnt == 0)
                        break;
                    MS.Write(buf, 0, cnt);
                    curCnt += cnt; // переменную увеличиваем на количество полученных байт
                }
                //*********************************
                byte[] b = new byte[MS.Length];//создаем новый байтовый массив длиной как отправленный
                Array.Copy(MS.GetBuffer(), b, b.Length); // копируем в него данные из потока

                // загружаем фото
                MemoryStream M2 = new MemoryStream(b);
                M2.Position = 0;
                BitmapImage imgsource = new BitmapImage();
                imgsource.BeginInit();
                imgsource.CreateOptions = BitmapCreateOptions.PreservePixelFormat;
                imgsource.CacheOption = BitmapCacheOption.OnLoad;
                imgsource.UriSource = null;
                imgsource.StreamSource = M2;
                imgsource.EndInit();
                this.image.Stretch = Stretch.UniformToFill;
                this.image.Source = imgsource;

                MS.SetLength(0); // очишаем память
            }
            catch (Exception e1)
            {
                NS.Close();
                MessageBox.Show(e1.Message, "Ошибка 336", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
        private void DataGridView_SelectionChanged(object sender, SelectionChangedEventArgs e) // обработчик выбора картинки 
        {
            DataRowView RV = this.DataGridView.SelectedItem as DataRowView;
            if (RV != null)
            {
                DataRow DR = RV.Row;
                string pichterPath = DR["pichter"].ToString();
                if (pichterPath != "")
                {
                    this.GetPichter(pichterPath);
                }
                if (pichterPath == "")
                {
                    this.image.Stretch = Stretch.UniformToFill;
                    this.image.Source = new BitmapImage(new Uri("pack://application:,,,/Resources/Noimage.png"));
                }
            }
        }
        #endregion
    }
    class Sale : IEditableObject
    {
        public int Id { get; set; }
        public string ProductName { get; set; }
        public string DeliveryName { get; set; }
        public string SalerName { get; set; }
        public int CntProduct { get; set; }
        public float SalePrice { get; set; }
        public float SaleSumm { get; set; }
        public float ProfitAll { get; set; }
        public float ProfitSaller { get; set; }
        public string SaleDate { get; set; }

        public Sale() { }
        public Sale( int id,string prodname, string salername, int cnt, float saleprice, float salesumm, float profitall, float profitsaler, string date)
        {
            this.Id = id;
            this.ProductName = prodname;
            this.SalerName = salername;
            this.CntProduct = cnt;
            this.SalePrice = saleprice;
            this.SaleSumm = salesumm;
            this.ProfitAll = profitall;
            this.ProfitSaller = profitsaler;
            this.SaleDate = date;
        }
        // ********************************** реализация : IEditableObject
        private Sale backupCopy;
        private bool inEdit;
        public void BeginEdit()
        {
            Console.WriteLine("Start BeginEdit_Sale");
            if (inEdit) return;
            inEdit = true;
            backupCopy = this.MemberwiseClone() as Sale;
        }

        public void CancelEdit()
        {
            Console.WriteLine("Start CancelEdit_Sale");
            if (!inEdit) return;
            inEdit = false;
            // все свойства которым возврашаем значения
            this.Id = backupCopy.Id;
            this.ProductName = backupCopy.ProductName;
            this.DeliveryName = backupCopy.DeliveryName;
            this.SalerName = backupCopy.SalerName;
            this.CntProduct = backupCopy.CntProduct;
            this.SalePrice = backupCopy.SalePrice;
            this.SaleSumm = backupCopy.SaleSumm;
            this.ProfitAll = backupCopy.ProfitAll;
            this.ProfitSaller = backupCopy.ProfitSaller;
            this.SaleDate = backupCopy.SaleDate;
        }

        public void EndEdit()
        {
            Console.WriteLine("Start EndEdit_Sale");
            if (!inEdit) return;
            inEdit = false;
            backupCopy = null;
        }
    }
}


//private void DataGridOrder_AutoGeneratingColumn(object sender, DataGridAutoGeneratingColumnEventArgs e)// во время автогенерации колонок задаем свойство обновления 
//{
//    Binding newBinding = new Binding(e.PropertyName);
//    newBinding.UpdateSourceTrigger = UpdateSourceTrigger.LostFocus;
//    (e.Column as DataGridTextColumn).Binding = newBinding;
//}