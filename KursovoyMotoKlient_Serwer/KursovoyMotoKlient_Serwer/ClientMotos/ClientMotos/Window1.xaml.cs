﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ClientMotos
{
    public partial class Window1 : Window
    {
        private bool IsAdmin;
        private TcpClient clientCheckLogin;
        private NetworkStream NS;
        private MemoryStream MS;

        private string ip;
        private int port;

        public float percented; // процент продавца получаем вместе с логином

        public Window1()
        {
            InitializeComponent();
            this.ip = ConfigurationManager.AppSettings["IP"];
            this.port = int.Parse(ConfigurationManager.AppSettings["PORT"]);
            // создаем соединение
            try
            {
                this.clientCheckLogin = new TcpClient(this.ip, this.port);
                this.NS = this.clientCheckLogin.GetStream();
                this.MS = new MemoryStream();
            }
            catch (Exception e1) { MessageBox.Show(e1.Message); }

            this.button_Oк.Click += Button_Oк_Click;
            this.button_Cancel.Click+= Button_Oк_Click;
            this.Closing += Window1_Closing;
        }

        private void Button_Oк_Click(object sender, RoutedEventArgs e)
        {
            Button B = sender as Button;
            int id = 0;
            this.IsAdmin = true;
            // если нажали OK
            if (B.Name == "button_Oк")
            {
                if (this.textBox_Name.Text == "" || this.passwordBox_Pasword.Password == "")
                {
                    this.IsAdmin = false;
                    this.textBox_Name.Text = "Заполнены не все поля !";
                    this.passwordBox_Pasword.Clear();
                    id = -1;
                }
                //************************************* проверка логина на сервере
                string inquiry = "LOGIN|" + this.textBox_Name.Text + "|" + this.passwordBox_Pasword.Password;// запрос
                // ********************** обмен данными:
                try
                {
                    //***  отправляем запрос на сервер
                    byte[] a = Encoding.UTF8.GetBytes(inquiry);
                    this.NS.Write(a, 0, a.Length);//////////вылетаем при закрытом сервере/////////
                    byte[] buf = new byte[128];
                    //*** получаем ответ от сервера
                    MS.SetLength(0); //перед заполнением освобождаем поток памяти
                    do
                    {
                        int cnt = NS.Read(buf, 0, buf.Length);
                        if (cnt == 0) throw new Exception("0 bytes received");
                        MS.Write(buf, 0, cnt);
                    } while (NS.DataAvailable);
                    // ********** обработка полученного ответа от сервера
                    string result = Encoding.UTF8.GetString(MS.GetBuffer(), 0, (int)MS.Length);
                    string[] arrResult = result.Split('!'); // достаем из ответа отправленную команду + OK
                    id = int.Parse(arrResult[2]);
                    if (arrResult[0] == "LOGIN" && arrResult[1] == "OK")
                    {
                        // проверяем результат
                        if (id > 0)
                        {
                            this.IsAdmin = true;
                            this.DialogResult = true;
                            this.percented =float.Parse( arrResult[3]); // процент заработка продавца
                        }
                        if (id == 0)
                        {
                            this.IsAdmin = false;
                            this.textBox_Name.Text = "Отказано в доступе !";
                            this.passwordBox_Pasword.Clear();
                        }
                    }
                    else
                    {
                        NS.Close();
                        MessageBox.Show("Не коректный ответ от сервера, соединение c сервером закрыто", "Ошибка", MessageBoxButton.OK, MessageBoxImage.Error);
                    }
                    MS.SetLength(0); // очишаем память
                }
                catch { MessageBox.Show("Нет соединения с сервером ","Ошибка",MessageBoxButton.OK,MessageBoxImage.Stop); this.IsAdmin = false; this.Close(); }
            }
            // если нажали Cancel
            if (B.Name == "button_Cancel")
                    this.DialogResult = false;
            // в конце в любом случае закрываем окно
            this.Close();
        }
        private void Window1_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (!this.IsAdmin)
                e.Cancel = true;
        }
    }
}
