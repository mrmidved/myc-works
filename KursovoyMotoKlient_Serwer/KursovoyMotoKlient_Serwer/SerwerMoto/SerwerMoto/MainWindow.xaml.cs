﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Data.Common;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace SerwerMoto
{
    //Data Source = DESKTOP - NS14CN7\SQLEXPRESS;Initial Catalog = MOTO; Integrated Security = True
    public partial class MainWindow : Window
    {
        private Window1 W1; // окно регистрации 
        private float course;// установленный курс $
        private string nameAdmin;// имя админа
        private string passwordAdmin;// пароль админа
       
        // Объекты соединения с БД
        private DbProviderFactory dbProviderFactory;
        private DbConnection dbConection;
        private DbDataAdapter dbAdapter;
        private DbCommand dbCommand;
        private DataSet dbDataSet;

        #region Таблицы и коллекции
        // таблицы
        private Dictionary<string, string> tablesname = new Dictionary<string, string> {
            {"Itemtype", "Тип товара"},
            {"ItemtypeCategory", "Категория товара"},
            {"Manufacturers", "Производитель"},
            {"ManufacturersModels","Модель" },
            {"ProductDetail","Описание товара" },
            {"Country","Страна" },
            {"Provider","Поставщик" },
            {"Delivery","Доставка" },
            {"Product","Товар" },
            {"SalerNames","Продавцы" },
            {"Sale","Продажи" },
            {"BedBugs","BedBugs" }
        };
        // массив с полями для запросов которые будем склеивать с выбранными значениями из комбобокса по поиску товара (Product)
        private string[] Arr_nameTable;
        // коллекции для загрузки в GridViev
        private ObservableCollection<Itemtype> LItemtype = new ObservableCollection<Itemtype>();
        private ObservableCollection<ItemtypeCategory> LItemtypeCategory = new ObservableCollection<ItemtypeCategory>();
        private ObservableCollection<Manufacturers> LManufacturers = new ObservableCollection<Manufacturers>();
        private ObservableCollection<ManufacturersModels> LManufacturersModels = new ObservableCollection<ManufacturersModels>();
        private ObservableCollection<ProductDetail> LProductDetail = new ObservableCollection<ProductDetail>();
        private ObservableCollection<Country> LCountry = new ObservableCollection<Country>();
        private ObservableCollection<Provider> LProvider = new ObservableCollection<Provider>();
        private ObservableCollection<Delivery> LDelivery = new ObservableCollection<Delivery>();
        private ObservableCollection<Product> LProduct = new ObservableCollection<Product>();
        private List<int> LidProductDel = new List<int>(); // список с удаленными id
        private List<Product> LidProductUpdate = new List<Product>(); // список с редактируемыми товарами

        private List<Product> LProductFromClient = new List<Product>();//  список товара для отправки клиенту
        private ObservableCollection<SalerNames> LSalerNames = new ObservableCollection<SalerNames>();
        private ObservableCollection<Sale> LSale = new ObservableCollection<Sale>();
        private ObservableCollection<BedBugs> LBedBugs = new ObservableCollection<BedBugs>();
        #endregion

        // команда для DB (удаление, добавление, редактирование)
        private DbCommand command;
        // массив комбобоксов по поиску товара
        private ComboBox[] ArrComboboxSearchProduct;
        // список для хранения удаленных id
        private List<int> idinDataset = new List<int>();
        // в список записываем выбранные пункты из комбобоксов поиска по товару(Product)
        private List<string> LsearchProduct = new List<string>();
        // событие для обработчика  добавления картинки ( ставим при создании колонок в табл товара)
        public event System.Windows.RoutedEventHandler button_Click;
        //************************************** поля для соединениия сервер-клиент
        private string ip;
        private int port;
        // поля для потока по обновлению таблицы товаров
        private delegate void De();
        static public bool IsPause = false;
      
        public MainWindow()
        {
            InitializeComponent();
            this.Conection();                                                   // создаем соединение
            this.dbAdapter = this.dbProviderFactory.CreateDataAdapter();        // создаем адаптер
            this.dbDataSet = new DataSet();                                     // создаем виртуальную таблицу
            this.comboBox_TableName.SelectedIndex = 7;                          // первая таблица в списке комбобокса
            
            // **************** запускаем окно регистрации
            this.W1= new Window1();
            if (this.W1.ShowDialog() == true)
            {
                this.Loaded += MainWindow_Loaded;
                this.comboBox_TableName.SelectionChanged += ComboBox_TableName_SelectionChanged; // комбобокс выбора таблиц
                this.Sawe.Click += Sawe_Click;                                      // кнопка изминений в БД
                this.Update.Click += Update_Click;                                  // обновление таблиц GridView
                this.DataGridView.RowEditEnding += DataGridView_RowEditEnding;      // событие срабатывает после редактирования(валидация редактируемой или вставляемой строки)
                this.DataGridView.BeginningEdit += DataGridView_BeginningEdit;      // обработчик начала редактирования
                this.button_Click += MainWindow_button_Click;                       // обработчик кнопки добавления картинки в товар
                this.comboBox_Profit.SelectionChanged += ComboBox_Profit_SelectionChanged; // обработчик выбора подсчета прибыли
               
                // заполняем масив комбобоксов по поиску товаров и массив с именами полей (последовательность елементов в этих массивах не менять!!!!!!!)
                this.ArrComboboxSearchProduct = new ComboBox[] {this.combobox_Itemtype,this.combobox_CategoryName,this.combobox_ManufacturersName,this.combobox_ManufacturersTypeName,this.combobox_DateDelivery };
                this.Arr_nameTable = new string[] { "Itemtype.name='{0}'", "ItemtypeCategory.name='{0}'","Manufacturers.name='{0}'","ManufacturersModels.name='{0}'","Delivery.datedelivery='{0}'" };
               
                // обработчики GridProduct
                this.DataGridProduct.RowEditEnding += DataGridView_RowEditEnding;     
                this.DataGridProduct.BeginningEdit += DataGridView_BeginningEdit;     
                this.DataGridProduct.SelectionChanged += DataGridView_SelectionChanged;

                this.LProduct.CollectionChanged += LProduct_CollectionChanged;
            }
            else
                this.Close();
        }
        private void Conection()       // создаем соединение с БД 
        {
            string provider = ConfigurationManager.AppSettings["provider"];
            string connectionString = ConfigurationManager.AppSettings["connectionStr"];
            // 1
            this.dbProviderFactory = DbProviderFactories.GetFactory(provider);
            // 2
            this.dbConection = this.dbProviderFactory.CreateConnection();
            this.dbConection.ConnectionString = connectionString;
        }
        private void FillingDataSet()  // заполняем DataSet , получаем курс $ из БД, заполняем учетные данные админа, вызываем FillingLists() и AllRelations()
        {
            this.dbCommand = this.dbConection.CreateCommand();                  // создаем объект комманды
            // заполняем
            foreach (KeyValuePair<string, string> KV in this.tablesname)
            {
                this.dbCommand.CommandText = "SELECT* FROM " + KV.Key;
                this.dbAdapter.SelectCommand = this.dbCommand;
                this.dbAdapter.Fill(this.dbDataSet, KV.Key);
            }
            this.dbConection.Close();
         
            // *** получаем курс $
            this.dbCommand.CommandText = "SELECT Admin.course FROM Admin";
            try
            {
                this.dbConection.Open();
                DbDataReader dbReader = this.dbCommand.ExecuteReader();
                if (dbReader.HasRows)// если содержит строки
                    while (dbReader.Read())
                        this.course = dbReader.GetFloat(0); 
            }
            catch (Exception e1) { MessageBox.Show(e1.Message); }
            finally { this.dbConection.Close(); }
            // заполняем учетные данные админа
            this.textbox_Course.Text = this.course.ToString();
            this.nameAdmin = this.W1.textBox_Name.Text;
            this.passwordAdmin = this.W1.passwordBox_Pasword.Password;
            // ***
            this.AllRelations(); // связи и ограничения
            this.FillingLists(); // загрузка коллекция из DataSet
        }
        private void AllRelations()    // устанавливаем связи и ограничения в таблицах 
        {
            //******************************* таблица ItemtypeCategory
            DataRelation DR = new DataRelation("Itemtype_ItemtypeCategory", this.dbDataSet.Tables["Itemtype"].Columns["id"],
            this.dbDataSet.Tables["ItemtypeCategory"].Columns["id_itemtype"]);
            this.dbDataSet.Relations.Add(DR);
            ForeignKeyConstraint FKS = DR.ChildKeyConstraint;  //при попытке удаления категории в которой есть тип  - ошибка 
            FKS.DeleteRule = Rule.None;
            FKS.UpdateRule = Rule.Cascade;
            //******************************* таблица ManufacturersModels
            DataRelation DR1 = new DataRelation("Manufacturers_ManufacturersModels", this.dbDataSet.Tables["Manufacturers"].Columns["id"],
            this.dbDataSet.Tables["ManufacturersModels"].Columns["id_manufacturers"]);
            this.dbDataSet.Relations.Add(DR1);
            ForeignKeyConstraint FKS1 = DR1.ChildKeyConstraint;
            FKS1.DeleteRule = Rule.None;
            FKS1.UpdateRule = Rule.Cascade;
            //******************************* таблица ProductDetail
            DataRelation DR2 = new DataRelation("ProductDetail_ItemtypeCategory", this.dbDataSet.Tables["ItemtypeCategory"].Columns["id"],
            this.dbDataSet.Tables["ProductDetail"].Columns["id_itemtypeCategory"]);
            this.dbDataSet.Relations.Add(DR2);
            ForeignKeyConstraint FKS2 = DR2.ChildKeyConstraint;
            FKS2.DeleteRule = Rule.None;
            FKS2.UpdateRule = Rule.Cascade;

            DataRelation DR3 = new DataRelation("ProductDetail_ManufacturersModels", this.dbDataSet.Tables["ManufacturersModels"].Columns["id"],
            this.dbDataSet.Tables["ProductDetail"].Columns["id_manufacturersModels"]);
            this.dbDataSet.Relations.Add(DR3);
            ForeignKeyConstraint FKS3 = DR3.ChildKeyConstraint;
            FKS3.DeleteRule = Rule.None;
            FKS3.UpdateRule = Rule.Cascade;
            //******************************* таблица Delivery
            DataRelation DR4 = new DataRelation("Delivery_Country", this.dbDataSet.Tables["Country"].Columns["id"],
            this.dbDataSet.Tables["Delivery"].Columns["id_country"]);
            this.dbDataSet.Relations.Add(DR4);
            ForeignKeyConstraint FKS4 = DR4.ChildKeyConstraint;
            FKS4.DeleteRule = Rule.None;
            FKS4.UpdateRule = Rule.Cascade;

            DataRelation DR5 = new DataRelation("Delivery_Provider", this.dbDataSet.Tables["Provider"].Columns["id"],
            this.dbDataSet.Tables["Delivery"].Columns["id_provider"]);
            this.dbDataSet.Relations.Add(DR5);
            ForeignKeyConstraint FKS5 = DR5.ChildKeyConstraint;
            FKS5.DeleteRule = Rule.None;
            FKS5.UpdateRule = Rule.Cascade;
            //******************************* таблица Product
            DataRelation DR6 = new DataRelation("Product_ProductDetail", this.dbDataSet.Tables["ProductDetail"].Columns["id"],
            this.dbDataSet.Tables["Product"].Columns["id_productDetail"]);
            this.dbDataSet.Relations.Add(DR6);
            ForeignKeyConstraint FKS6 = DR6.ChildKeyConstraint;
            FKS6.DeleteRule = Rule.None;
            FKS6.UpdateRule = Rule.Cascade;

            DataRelation DR7 = new DataRelation("Product_Delivery", this.dbDataSet.Tables["Delivery"].Columns["id"],
            this.dbDataSet.Tables["Product"].Columns["id_delivery"]);
            this.dbDataSet.Relations.Add(DR7);
            ForeignKeyConstraint FKS7 = DR7.ChildKeyConstraint;
            FKS7.DeleteRule = Rule.None;
            FKS7.UpdateRule = Rule.Cascade;
            //******************************* таблица Sale
            DataRelation DR8 = new DataRelation("Sale_Product", this.dbDataSet.Tables["Product"].Columns["id"],
           this.dbDataSet.Tables["Sale"].Columns["id_product"]);
            this.dbDataSet.Relations.Add(DR8);
            ForeignKeyConstraint FKS8 = DR8.ChildKeyConstraint;
            FKS8.DeleteRule = Rule.None;
            FKS8.UpdateRule = Rule.Cascade;

            DataRelation DR9 = new DataRelation("Sale_SalerNames", this.dbDataSet.Tables["SalerNames"].Columns["id"],
            this.dbDataSet.Tables["Sale"].Columns["id_salerNames"]);
            this.dbDataSet.Relations.Add(DR9);
            ForeignKeyConstraint FKS9 = DR9.ChildKeyConstraint;
            FKS9.DeleteRule = Rule.None;
            FKS9.UpdateRule = Rule.Cascade;

        }
        private void FillingLists()    // заполняем ObservableCollection 
        {
            foreach (DataRow r in this.dbDataSet.Tables["Itemtype"].Rows)
            {
                this.LItemtype.Add(new Itemtype((int)r["id"], r["name"].ToString()));
                this.combobox_Itemtype.Items.Add(r["name"].ToString()); // заполняем комбобокс для поиска в товарах
            }

            foreach (DataRow r in this.dbDataSet.Tables["Itemtype"].Rows) // бежим по первой таблице
            {
                // получаем дочерние елементы
                DataRow[] childRows = r.GetChildRows(this.dbDataSet.Relations["Itemtype_ItemtypeCategory"]);
                for (int i = 0; i < childRows.Length; i++)
                {
                    this.LItemtypeCategory.Add(new ItemtypeCategory((int)childRows[i]["id"], childRows[i]["name"].ToString(), r["name"].ToString()));
                    this.combobox_CategoryName.Items.Add(childRows[i]["name"].ToString());// заполняем комбобокс для поиска в товарах
                }
            }
            foreach (DataRow r in this.dbDataSet.Tables["Manufacturers"].Rows)
            {
                this.LManufacturers.Add(new Manufacturers((int)r["id"], r["name"].ToString()));
                this.combobox_ManufacturersName.Items.Add(r["name"].ToString());// заполняем комбобокс для поиска в товарах
            }

            foreach (DataRow r in this.dbDataSet.Tables["Manufacturers"].Rows) // бежим по первой таблице
            {
                // получаем дочерние елементы
                DataRow[] childRows = r.GetChildRows(this.dbDataSet.Relations["Manufacturers_ManufacturersModels"]);
                for (int i = 0; i < childRows.Length; i++)
                {
                    this.LManufacturersModels.Add(new ManufacturersModels((int)childRows[i]["id"], childRows[i]["name"].ToString(), r["name"].ToString()));
                    if(childRows[i]["name"].ToString()!="")
                         this.combobox_ManufacturersTypeName.Items.Add(childRows[i]["name"].ToString());// заполняем комбобокс для поиска в товарах
                }
            }

            this.RequestProdDetail();// 13 ms \376ms
                                    
            foreach (DataRow r in this.dbDataSet.Tables["Country"].Rows)
                this.LCountry.Add(new Country((int)r["id"], r["name"].ToString()));

            foreach (DataRow r in this.dbDataSet.Tables["Provider"].Rows)
                this.LProvider.Add(new Provider((int)r["id"], r["name"].ToString()));

            this.RequestDelivery();//13 ms

            this.RequestProd();// 13ms\16 202ms
                              
            foreach (DataRow r in this.dbDataSet.Tables["SalerNames"].Rows)
                this.LSalerNames.Add(new SalerNames((int)r["id"], r["name"].ToString(), (float)r["percented"]));

            this.RequestSales();// 57ms\41 223ms
        }
        private void ClearLists()      // очищаем все ObservableCollection 
        {
            this.LItemtype.Clear();
            this.LItemtypeCategory.Clear();
            this.LManufacturers.Clear();
            this.LManufacturersModels.Clear();
            this.LProductDetail.Clear();
            this.LCountry.Clear();
            this.LProvider.Clear();
            this.LDelivery.Clear();
            this.LProduct.Clear();
            this.LSalerNames.Clear();
            this.LSale.Clear();
            this.LBedBugs.Clear();
            // чистим комбобоксы выбора товара
            for (int i = 0; i < this.ArrComboboxSearchProduct.Length; i++)
                this.ArrComboboxSearchProduct[i].Items.Clear();

            this.LProductFromClient.Clear();

        }
        private void MainWindow_Loaded(object sender, RoutedEventArgs e) // FormLoad 
        {
            this.FillingDataSet();   // заполняем DataSet, проставляем связи и заполняем коллекции 
            this.command = this.dbConection.CreateCommand();// команда для обновления,добавления,удаления из БД
            foreach (KeyValuePair<string, string> KV in this.tablesname) // загружаем комбобокс сназванием таблц
                this.comboBox_TableName.Items.Add(KV.Value);
            this.comboBox_TableName.Items.Remove("Товар");
            // ************************************************** сервер
            this.ip= ConfigurationManager.AppSettings["IP"];
            this. port = int.Parse(ConfigurationManager.AppSettings["PORT"]);

            // поток обновления количества товара после продажи
            Thread T1 = new Thread(this.RunUpdate);
            T1.IsBackground = true;
            Thread.Sleep(100);
            T1.Start();
           
            // запускаем серверный поток ( через 2 класса)
            ServerThread ST = new ServerThread(this.ip,this.port,this.LProductFromClient,this.dbConection,this.LSalerNames);
            Thread T = new Thread(ST.Run);
            T.IsBackground = true;
            Thread.Sleep(100);
            T.Start();
        }

        #region ************************ Потоковые методы обновления таблицы продуктов после продажи ( редактируем количество товара)
        private void RunUpdate()    //потоковый метод 
        {
            De d = new De(this.UP);
            while (true)
            {
                Thread.Sleep(2000);
                Console.WriteLine("Ждем обновления продуктов... наша заветная = {0}", MainWindow.IsPause.ToString());
                if (MainWindow.IsPause)
                {
                    this.DataGridProduct.Dispatcher.Invoke(d);
                    Console.WriteLine("Ок, обновили продукты !!!!! переменная={0}", MainWindow.IsPause.ToString());
                    MainWindow.IsPause = false;
                    Console.WriteLine("Все,переменная снова ={0}",MainWindow.IsPause.ToString());
                }
            }
        }
        private void UP()           // обрабатываемый метод 
        {
            object block = new object();
            lock (block)
            {
                this.DataGridProduct.Columns.Clear();       // чистим таблицу GridProduct
                this.dbDataSet.Clear();                     // чистим таблицу DataSet
                this.dbDataSet.Relations.Clear();           // чистим взаимосвязи
                this.ClearLists();                          // чистим все  ObservableCollection
                this.FillingDataSet();                      // заполняем DataSet, проставляем связи и заполняем коллекции
                this.TableProduct();
            }
        }
        #endregion

        #region*************************************** Mетоды редактирования и валидация строк 
        private void DataGridView_BeginningEdit(object sender, DataGridBeginningEditEventArgs e) // (начало редактирования таблиц GridView) скрываем кнопку 
        {
            // при редактировании записываем продукт для перебора в методе ProductAddDellUp() 
            if (e.Row.Item is Product)
            {
                Product p = e.Row.Item as Product;
                if (p.IDProduct != 0)
                {
                    this.LidProductUpdate.Add(p);
                   // MessageBox.Show(p.IDProduct.ToString());
                }
            }
            this.Sawe.IsEnabled = false;
           // this.Update.IsEnabled = false;
            DataGrid DR = sender as DataGrid;
            // колонку с кнопками(Product) делаем видимой
            if (DR.SelectedItem is SerwerMoto.Product)
            {
                DR.Columns[7].Visibility = Visibility.Visible;      
            }
            //делаем столбец с выбором даты(Delivery) невидемым
            if (DR.SelectedItem is SerwerMoto.Delivery)
            {
                Delivery de = (Delivery)e.Row.Item;
                if(de.DelivDate==null)
                 de.DateFromDp = DateTime.Now;
                DR.Columns[6].Visibility = Visibility.Visible;
                DR.Columns[5].Visibility = Visibility.Hidden;
            }
        }
        private void EdEndRowItemtype(DataGridRowEditEndingEventArgs e) 
        {
            Itemtype It = (Itemtype)e.Row.Item;
            //  проверка на правильность значений
            if (It.NameIt == null || It.NameIt=="")
            {
                if (It.IDItemtupe != 0)              // если редактируем строку
                {
                    It.CancelEdit();
                    string error = string.Format("Строка № {0} имеет неверный формат !!!", It.IDItemtupe);
                    MessageBox.Show(error, "Error!", MessageBoxButton.OK, MessageBoxImage.Error);
                }
                if (It.IDItemtupe == 0)             // если вставляем новую  неправильную строку
                {
                    MessageBox.Show("Строка имеет неверный формат !!!", "Error!", MessageBoxButton.OK, MessageBoxImage.Error);
                    ObservableCollection<Itemtype> LP = (ObservableCollection<Itemtype>)this.DataGridView.ItemsSource;
                    LP.Remove(It);
                }
            }
            else It.NameIt=It.NameIt.Replace(' ', '_'); // заменяем пробелы на _
        }
        private void EdEndRowItemtypeCategory(DataGridRowEditEndingEventArgs e) 
        {
            ItemtypeCategory Itc = (ItemtypeCategory)e.Row.Item;
            //  проверка на правильность значений
            if (Itc.NameItemtype == null || Itc.NameItemtype == "" || Itc.Name==null || Itc.Name == "")
            {
                if (Itc.ID != 0)              // если редактируем строку
                {
                    Itc.CancelEdit();
                    string error = string.Format("Строка № {0} имеет неверный формат !!!", Itc.ID);
                    MessageBox.Show(error, "Error!", MessageBoxButton.OK, MessageBoxImage.Error);
                }
                if (Itc.ID == 0)             // если вставляем новую  неправильную строку
                {
                    MessageBox.Show("Строка имеет неверный формат !!!", "Error!", MessageBoxButton.OK, MessageBoxImage.Error);
                    ObservableCollection<ItemtypeCategory> LP = (ObservableCollection<ItemtypeCategory>)this.DataGridView.ItemsSource;
                    LP.Remove(Itc);
                }
            }
            else
            {   // заменяем пробелы на - 
                Itc.Name = Itc.Name.Replace(' ', '_'); 
                Itc.NameItemtype=Itc.NameItemtype.Replace(' ', '_');
            }
        }
        private void EdEndRowManufacturers(DataGridRowEditEndingEventArgs e) 
        {
            Manufacturers M = (Manufacturers)e.Row.Item;
            //  проверка на правильность значений
            if (M.NameMan == null || M.NameMan == "")
            {
                if (M.IDManufacturers != 0)              // если редактируем строку
                {
                    M.CancelEdit();
                    string error = string.Format("Строка № {0} имеет неверный формат !!!", M.IDManufacturers);
                    MessageBox.Show(error, "Error!", MessageBoxButton.OK, MessageBoxImage.Error);
                }
                if (M.IDManufacturers == 0)             // если вставляем новую  неправильную строку
                {
                    MessageBox.Show("Строка имеет неверный формат !!!", "Error!", MessageBoxButton.OK, MessageBoxImage.Error);
                    ObservableCollection<Manufacturers> LP = (ObservableCollection<Manufacturers>)this.DataGridView.ItemsSource;
                    LP.Remove(M);
                }
            }
            else M.NameMan = M.NameMan.Replace(' ', '_'); // заменяем пробелы на - 
        }
        private void EdEndRowManufacturersModels(DataGridRowEditEndingEventArgs e) 
        {
            ManufacturersModels MM = (ManufacturersModels)e.Row.Item;
            //  проверка на правильность значений
            if (MM.NameManufacturers == null || MM.NameManufacturers == "" /*|| MM.NameManufacturersModels == null || MM.NameManufacturersModels == ""*/) // начение второго столбца может быть null
            {
                if (MM.IDManufacturersModels != 0)              // если редактируем строку
                {
                    MM.CancelEdit();
                    string error = string.Format("Строка № {0} имеет неверный формат !!!", MM.IDManufacturersModels);
                    MessageBox.Show(error, "Error!", MessageBoxButton.OK, MessageBoxImage.Error);
                }
                if (MM.IDManufacturersModels == 0)             // если вставляем новую  неправильную строку
                {
                    MessageBox.Show("Строка имеет неверный формат !!!", "Error!", MessageBoxButton.OK, MessageBoxImage.Error);
                    ObservableCollection<ManufacturersModels> LP = (ObservableCollection<ManufacturersModels>)this.DataGridView.ItemsSource;
                    LP.Remove(MM);
                }
            }
            else 
            {   // заменяем пробелы на - 
                // MM.NameManufacturers =MM.NameManufacturers.Replace(' ', '_');// мы не заполняем комбобокс
                if (MM.NameManufacturersModels!=null)
                  MM.NameManufacturersModels = MM.NameManufacturersModels.Replace(' ', '_');
            }
        }
        private void EdEndRowProductDetail(DataGridRowEditEndingEventArgs e) 
        {
            ProductDetail PD = (ProductDetail)e.Row.Item;
            //  проверка на правильность значений
            if (PD.NameItCat == null || PD.NameItCat == "" || PD.NameManMod == null || PD.NameManMod == ""|| PD.NameProductDetail == "" || PD.NameProductDetail == null) 
            {
                if (PD.IDProductDetail != 0)              // если редактируем строку
                {
                    PD.CancelEdit();
                    string error = string.Format("Строка № {0} имеет неверный формат !!!",PD.IDProductDetail);
                    MessageBox.Show(error, "Error!", MessageBoxButton.OK, MessageBoxImage.Error);
                }
                if (PD.IDProductDetail == 0)             // если вставляем новую  неправильную строку
                {
                    MessageBox.Show("Строка имеет неверный формат !!!", "Error!", MessageBoxButton.OK, MessageBoxImage.Error);
                    ObservableCollection<ProductDetail> LP = (ObservableCollection<ProductDetail>)this.DataGridView.ItemsSource;
                    LP.Remove(PD);
                }
            }
            else
            {   // заменяем пробелы на _ 
               PD.NameProductDetail=PD.NameProductDetail.Replace(' ', '_');
            }
        }
        private void EdEndRowCountry(DataGridRowEditEndingEventArgs e)  
        {
            Country c = (Country)e.Row.Item;
            //  проверка на правильность значений
            if (c.NameCountry == null || c.NameCountry == "")
            {
                if (c.IDCountry != 0)              // если редактируем строку
                {
                    c.CancelEdit();
                    string error = string.Format("Строка № {0} имеет неверный формат !!!",c.IDCountry);
                    MessageBox.Show(error, "Error!", MessageBoxButton.OK, MessageBoxImage.Error);
                }
                if (c.IDCountry == 0)             // если вставляем новую  неправильную строку
                {
                    MessageBox.Show("Строка имеет неверный формат !!!", "Error!", MessageBoxButton.OK, MessageBoxImage.Error);
                    ObservableCollection<Country> LP = (ObservableCollection<Country>)this.DataGridView.ItemsSource;
                    LP.Remove(c);
                }
            }
            else c.NameCountry = c.NameCountry.Replace(' ', '_'); // заменяем пробелы на -
        }
        private void EdEndRowProvider(DataGridRowEditEndingEventArgs e) 
        {
            Provider pr = (Provider)e.Row.Item;
            //  проверка на правильность значений
            if (pr.NameProvider == null || pr.NameProvider == "")
            {
                if (pr.IDProvider != 0)              // если редактируем строку
                {
                    pr.CancelEdit();
                    string error = string.Format("Строка № {0} имеет неверный формат !!!", pr.IDProvider);
                    MessageBox.Show(error, "Error!", MessageBoxButton.OK, MessageBoxImage.Error);
                }
                if (pr.IDProvider == 0)             // если вставляем новую  неправильную строку
                {
                    MessageBox.Show("Строка имеет неверный формат !!!", "Error!", MessageBoxButton.OK, MessageBoxImage.Error);
                    ObservableCollection<Provider> LP = (ObservableCollection<Provider>)this.DataGridView.ItemsSource;
                    LP.Remove(pr);
                }
            }
            else pr.NameProvider = pr.NameProvider.Replace(' ', '_'); // заменяем пробелы на -
        }
        private void EdEndRowDelivery(object sender, DataGridRowEditEndingEventArgs e) 
        {
            Delivery de = (Delivery)e.Row.Item;
            //  проверка на правильность значений
            if (de.NameDelivCount == null || de.NameDelivCount == "" || /*de.NameDelivForm == null || de.NameDelivForm == "" ||*/ de.NameDelivProv == null || de.NameDelivProv == "" || de.DelivPercent < 0 /*|| de.DelivDate == ""*/ || de.DelivDate == null)
            {
                if (de.IDDelivery != 0)              // если редактируем строку
                {
                    de.CancelEdit();
                    string error = string.Format("Строка № {0} имеет неверный формат !!!", de.IDDelivery);
                    MessageBox.Show(error, "Error!", MessageBoxButton.OK, MessageBoxImage.Error);
                }
                if (de.IDDelivery == 0)             // если вставляем новую неправильную строку
                {
                    MessageBox.Show("Строка имеет неверный формат !!!", "Error!", MessageBoxButton.OK, MessageBoxImage.Error);
                    ObservableCollection<Delivery> LP = (ObservableCollection<Delivery>)this.DataGridView.ItemsSource;
                    LP.Remove(de);
                }
            }
            else // заменяем пробелы на _
            {
                de.NameDelivCount= de.NameDelivCount.Replace(' ', '_');
                if(de.NameDelivForm !=null)
                   de.NameDelivForm=de.NameDelivForm.Replace(' ', '_');
                de.NameDelivProv = de.NameDelivProv.Replace(' ', '_');
            }
            // колонку с выбором даты делаем не видимой
            DataGrid DR = sender as DataGrid;
            DR.Columns[6].Visibility = Visibility.Hidden;
            DR.Columns[5].Visibility = Visibility.Visible;
        }
        private void EdEndRowProduct(object sender,DataGridRowEditEndingEventArgs e)  
        {
            Product P = (Product)e.Row.Item;
            //  проверка на правильность значений
            if (P.NameAllProduct == null || P.NameAllDelivery == null || P.Cnt < 0 || P.Price1 < 0 )
            {
                if (P.IDProduct != 0)              // если редактируем строку
                {
                    P.CancelEdit();
                    string error = string.Format("Строка № {0} имеет неверный формат !!!", P.IDProduct);
                    MessageBox.Show(error);
                }
                if (P.IDProduct == 0)              // если вставляем новую  неправильную строку
                {
                    MessageBox.Show("Строка имеет неверный формат !!!");
                    //ObservableCollection<Product> LP = (ObservableCollection<Product>)this.DataGridView.ItemsSource;
                    ObservableCollection<Product> LP = (ObservableCollection<Product>)this.DataGridProduct.ItemsSource;
                    LP.Remove(P);
                }
            }
            else
            {
                // заполняем значением колонку с ценой+ % 
                string[] deliv = P.NameAllDelivery.Split(' ');
                int id_delivery = this.IdDelivery(deliv[0], deliv[1], deliv[2], deliv[3]);
                var orderpercent = from T in this.dbDataSet.Tables["Delivery"].AsEnumerable()
                                   where T.Field<int>("id") == id_delivery
                                   select new { percent = T.Field<float>("percentage") };
                string d = string.Format("{0:F2} ", (P.Price1 + (P.Price1 * ((orderpercent.Single().percent) * (float)0.01))));
                P.Price2 =float.Parse(d);
            }
            
            // колонку с кнопками делаем не видимой
            DataGrid DR = sender as DataGrid;
            DR.Columns[7].Visibility = Visibility.Hidden;
        }
        private void EdEndRowSalerNames(DataGridRowEditEndingEventArgs e) 
        {
            SalerNames sn = (SalerNames)e.Row.Item;
            //  проверка на правильность значений
            if (sn.NameSalernames == null || sn.NameSalernames == "" || sn.SelerPercent<0)
            {
                if (sn.IDSelerName != 0)              // если редактируем строку
                {
                    sn.CancelEdit();
                    string error = string.Format("Строка № {0} имеет неверный формат !!!", sn.IDSelerName);
                    MessageBox.Show(error, "Error!", MessageBoxButton.OK, MessageBoxImage.Error);
                }
                if (sn.IDSelerName == 0)             // если вставляем новую  неправильную строку
                {
                    MessageBox.Show("Строка имеет неверный формат !!!", "Error!", MessageBoxButton.OK, MessageBoxImage.Error);
                    ObservableCollection<SalerNames> LP = (ObservableCollection<SalerNames>)this.DataGridView.ItemsSource;
                    LP.Remove(sn);
                }
            }
            else sn.NameSalernames = sn.NameSalernames.Replace(' ', '_'); // заменяем пробелы на _
        }
        private void EdEndRowBedBugs() 
        { }
        private void DataGridView_RowEditEnding(object sender, DataGridRowEditEndingEventArgs e) // ( конец редактирования таблиц GridView) проверяем правильность заполненых комбобоксов в таблицах 
        {
            // возврашает имя класса 
            string s = e.Row.DataContext.ToString();
            switch (e.Row.DataContext.ToString())
            {
                case "SerwerMoto.Itemtype": this.EdEndRowItemtype(e);break;
                case "SerwerMoto.ItemtypeCategory": this.EdEndRowItemtypeCategory(e); break;
                case "SerwerMoto.Manufacturers": this.EdEndRowManufacturers(e); break;
                case "SerwerMoto.ManufacturersModels": this.EdEndRowManufacturersModels(e); break;
                case "SerwerMoto.ProductDetail": this.EdEndRowProductDetail(e); break;
                case "SerwerMoto.Country": this.EdEndRowCountry(e); break;
                case "SerwerMoto.Provider": this.EdEndRowProvider(e); break;
                case "SerwerMoto.Delivery": this.EdEndRowDelivery(sender,e); break;
                case "SerwerMoto.Product": this.EdEndRowProduct(sender,e); break;
                case "SerwerMoto.SalerNames": this.EdEndRowSalerNames(e); break;
                case "SerwerMoto.Sale": break;
                case "SerwerMoto.BedBugs": break;
            }
            this.Sawe.IsEnabled = true; // показывем кнопку SAWE
            //this.Update.IsEnabled = true;// показывем кнопку Update
        }
        #endregion

        #region *************************************** Mетоды поиска id 
        private int IdProduct (string product,string delivery)  //   поиск проданного продукта КЛИЕНТ 
        {
            string[] prod = product.Split(' ');
            string[] deliv = delivery.Split(' ');
            int id_productDetail = this.IdProductDatale(prod[0], prod[1], prod[2], prod[3], prod[4]);
            int id_delivery = this.IdDelivery(deliv[0], deliv[1], deliv[2], deliv[3]);

            int id = 0;
            var order = from T in this.dbDataSet.Tables["Product"].AsEnumerable()
                        where T.Field<int>("id_productDetai") == id_productDetail &&
                        T.Field<int>("id_productDelivery") == id_delivery 
                        select new { id_nameg = T.Field<int>("id") };
            id = order.Single().id_nameg;
            return id;
        }
        private int IdSalerName(string name) 
        {
            int id = 0;
            var order = from T in this.dbDataSet.Tables["SalerNames"].AsEnumerable()
                        where T.Field<string>("name") == name
                        select new { id_nameg = T.Field<int>("id") };
            id = order.Single().id_nameg;
            return id;
        }
        private int IdProductDatale(string item, string itemtypecategory, string manufactures, string manufacturersmodels, string prod)
        {
            int id = 0;
            if (manufacturersmodels != "")
            {
                var order = from T in this.dbDataSet.Tables["ItemtypeCategory"].AsEnumerable()
                            from T1 in this.dbDataSet.Tables["Itemtype"].AsEnumerable()
                            from T2 in this.dbDataSet.Tables["Manufacturers"].AsEnumerable()
                            from T3 in this.dbDataSet.Tables["ProductDetail"].AsEnumerable()
                            from T4 in this.dbDataSet.Tables["ManufacturersModels"].AsEnumerable()
                            where
                            T3.Field<int>("id_itemtypeCategory") == T.Field<int>("id") &&
                            T.Field<int>("id_itemtype") == T1.Field<int>("id") &&
                            T3.Field<int>("id_manufacturersModels") == T4.Field<int>("id") &&
                            T4.Field<int>("id_manufacturers") == T2.Field<int>("id") &&
                                
                            T1.Field<string>("name") == item &&
                            T.Field<string>("name") == itemtypecategory &&
                            T2.Field<string>("name") == manufactures &&
                            T4.Field<string>("name") == manufacturersmodels &&
                            T3.Field<string>("name") == prod    
                            select new { id_nameg = T3.Field<int>("id") };
                id = order.Single().id_nameg;
            }
            else
            {
                var order = from T in this.dbDataSet.Tables["ItemtypeCategory"].AsEnumerable()
                            from T1 in this.dbDataSet.Tables["Itemtype"].AsEnumerable()
                            from T2 in this.dbDataSet.Tables["Manufacturers"].AsEnumerable()
                            from T3 in this.dbDataSet.Tables["ProductDetail"].AsEnumerable()
                            from T4 in this.dbDataSet.Tables["ManufacturersModels"].AsEnumerable()

                            where
                            T3.Field<int>("id_itemtypeCategory") == T.Field<int>("id") &&
                            T.Field<int>("id_itemtype") == T1.Field<int>("id") &&
                            T3.Field<int>("id_manufacturersModels") == T4.Field<int>("id") &&
                            T4.Field<int>("id_manufacturers") == T2.Field<int>("id") &&

                            T1.Field<string>("name") == item &&
                            T.Field<string>("name") == itemtypecategory &&
                            T2.Field<string>("name") == manufactures &&
                            T3.Field<string>("name") == prod
                            select new { id_nameg = T3.Field<int>("id") };
                id = order.Single().id_nameg;
            }
            
            return id;
        }
        private int IdDelivery(string canname, string delform, string provname, string date) 
        {
            int id = 0;
            if (delform != "")
            {
                var order = from T in this.dbDataSet.Tables["Delivery"].AsEnumerable()
                            from T1 in this.dbDataSet.Tables["Country"].AsEnumerable()
                            from T2 in this.dbDataSet.Tables["Provider"].AsEnumerable()
                            where T.Field<int>("id_country") == T1.Field<int>("id") &&
                            T.Field<int>("id_provider") == T2.Field<int>("id") &&
                            T1.Field<string>("name") == canname &&
                            T.Field<string>("deliveryform_name") == delform &&
                            T2.Field<string>("name") == provname &&
                            T.Field<DateTime>("datedelivery") == DateTime.Parse(date)
                            select new { id_nameg = T.Field<int>("id") };
                id = order.Single().id_nameg;
            }
            else
            {
                var order = from T in this.dbDataSet.Tables["Delivery"].AsEnumerable()
                            from T1 in this.dbDataSet.Tables["Country"].AsEnumerable()
                            from T2 in this.dbDataSet.Tables["Provider"].AsEnumerable()
                            where T.Field<int>("id_country") == T1.Field<int>("id") &&
                            T.Field<int>("id_provider") == T2.Field<int>("id") &&
                            T1.Field<string>("name") == canname &&
                            T2.Field<string>("name") == provname &&
                            T.Field<DateTime>("datedelivery") == DateTime.Parse(date)
                            select new { id_nameg = T.Field<int>("id") };
                id = order.Single().id_nameg;
            }
            return id;
        }
        private int IdItemtypeCategory(string itemtype, string itemtypecategory) 
        {
            int id = 0;
            var order = from T in this.dbDataSet.Tables["ItemtypeCategory"].AsEnumerable()
                        from T1 in this.dbDataSet.Tables["Itemtype"].AsEnumerable()
                        where T.Field<int>("id_itemtype") == T1.Field<int>("id")&&
                        T.Field<string>("name") == itemtypecategory &&
                        T1.Field<string>("name") == itemtype
                        select new { id_nameg = T.Field<int>("id") };
            id = order.Single().id_nameg;
            return id;
        }
        private int IdManufacturersModels(string manufacturers,string manmodname) 
        {
            int id = 0;
            if (manmodname != "")
            {
                var order = from T in this.dbDataSet.Tables["ManufacturersModels"].AsEnumerable()
                            from T1 in this.dbDataSet.Tables["Manufacturers"].AsEnumerable()
                            where T.Field<int>("id_manufacturers") == T1.Field<int>("id") &&
                            T.Field<string>("name") == manmodname &&
                            T1.Field<string>("name") == manufacturers
                            select new { id_nameg = T.Field<int>("id") };
                id = order.Single().id_nameg;
            }
            else
            {
                var order = from T in this.dbDataSet.Tables["ManufacturersModels"].AsEnumerable()
                            from T1 in this.dbDataSet.Tables["Manufacturers"].AsEnumerable()
                            where T.Field<int>("id_manufacturers") == T1.Field<int>("id") &&
                            T1.Field<string>("name") == manufacturers &&
                            T.Field<string>("name") == "" 
                            select new { id_nameg = T.Field<int>("id") };
                id = order.Single().id_nameg;
            }
            return id;

        }
        private int IdItemtype(string itemtype) 
        {
            int id = 0;
            var order = from T in this.dbDataSet.Tables["Itemtype"].AsEnumerable()
                         where T.Field<string>("name") == itemtype
                         select new { id_nameg = T.Field<int>("id") };
            id = order.Single().id_nameg; 
            return id;
        }
        private int IdManufacturers(string manufacturers) 
        {
            int id = 0;
            var order = from T in this.dbDataSet.Tables["Manufacturers"].AsEnumerable()
                        where T.Field<string>("name") == manufacturers
                        select new { id_nameg = T.Field<int>("id") };
            id = order.Single().id_nameg;
            return id;
        }
        private int IdCountry(string country) 
        {
            int id = 0;
            var order = from T in this.dbDataSet.Tables["Country"].AsEnumerable()
                        where T.Field<string>("name") == country
                        select new { id_nameg = T.Field<int>("id") };
            id = order.Single().id_nameg;
            return id;
        }
        private int IdProvider(string provider) 
        {
            int id = 0;
            var order = from T in this.dbDataSet.Tables["Provider"].AsEnumerable()
                        where T.Field<string>("name") == provider
                        select new { id_nameg = T.Field<int>("id") };
            id = order.Single().id_nameg;
            return id;
        }
        #endregion

        #region *************************************** Удаление Добавление Редактирование
        private void ItemtypeAddDellUp() 
        {
            //1- переписываем все id из таблицы датасета
            this.idinDataset.Clear();
            foreach (DataRow R in this.dbDataSet.Tables["Itemtype"].Rows)
                this.idinDataset.Add((int)R["id"]);
            //2- добавление и обновление
            bool iscontains;
            int idUpdate;
            foreach (Itemtype it in this.LItemtype)
            {
                int id =it.IDItemtupe;
                string name = it.NameIt;

                // определяем есть ли такой id в таблице в датасете
                iscontains = this.idinDataset.Contains(id);
                if (iscontains)
                    this.idinDataset.Remove(id);

                // если новая запись
                if (id == 0)
                {
                    this.command.CommandText = string.Format("INSERT INTO Itemtype (name) VALUES('{0}')",name);
                    try { this.command.ExecuteNonQuery(); } //для обновления БД, вызываем после каждого запроса
                    catch (Exception e1) { MessageBox.Show(e1.Message); }
                }
                // строку либо редактировали либо нет
                if (id != 0)
                {
                    // если в загруженном датасете нет записи с такими данными то по catch обновляем( если все совпадает то строку не меняли)
                    idUpdate = 0;
                    var order = from T in this.dbDataSet.Tables["Itemtype"].AsEnumerable()
                                where T.Field<string>("name") == name
                                select new { idUpdate = T.Field<int>("id") };
                    try { idUpdate = order.Single().idUpdate; }
                    catch (Exception e1)
                    {
                        if (e1.Message == "Последовательность не содержит элементов")
                        {
                            this.command.CommandText = string.Format("UPDATE Itemtype SET name='{0}' WHERE id={1}", name, id);
                            try { this.command.ExecuteNonQuery(); }
                            catch (Exception e2) { MessageBox.Show(e2.Message); }
                        }
                    }
                }
            }
            //3- удаление из базы данных 
            foreach (int i in this.idinDataset)
            {
                this.command.CommandText = string.Format("DELETE FROM Itemtype WHERE id ={0}", i);
                try { this.command.ExecuteNonQuery(); }
                catch (Exception e1) { MessageBox.Show(e1.Message); }
            }
        }
        private void ItemtypeCategoryAddDellUp() 
        {
            //1- переписываем все id из таблицы датасета
            this.idinDataset.Clear();
            foreach (DataRow R in this.dbDataSet.Tables["ItemtypeCategory"].Rows)
                this.idinDataset.Add((int)R["id"]);
            //2- добавление и обновление
            bool iscontains;
            int idUpdate;
            foreach (ItemtypeCategory itc in this.LItemtypeCategory)
            {
                int id = itc.ID;
                int id_itemtype = this.IdItemtype(itc.NameItemtype);
                string name = itc.Name;

                // определяем есть ли такой id в таблице в датасете
                iscontains = this.idinDataset.Contains(id);
                if (iscontains)
                    this.idinDataset.Remove(id);

                // если новая запись
                if (id == 0)
                {
                    this.command.CommandText = string.Format("INSERT INTO ItemtypeCategory (id_itemType,name) VALUES({0},'{1}')",id_itemtype, name);
                    try { this.command.ExecuteNonQuery(); } //для обновления БД, вызываем после каждого запроса
                    catch (Exception e1) { MessageBox.Show(e1.Message); }
                }
                // строку либо редактировали либо нет
                if (id != 0)
                {
                    // если в загруженном датасете нет записи с такими данными то по catch обновляем(а если все совпадает то строку не меняли)
                    idUpdate = 0;
                    var order = from T in this.dbDataSet.Tables["ItemtypeCategory"].AsEnumerable()
                                where T.Field<string>("name") == name &&
                                      T.Field<int>("id_itemType") == id_itemtype
                                select new { idUpdate = T.Field<int>("id") };
                    try { idUpdate = order.Single().idUpdate; }
                    catch (Exception e1)
                    {
                        if (e1.Message == "Последовательность не содержит элементов")
                        {
                            this.command.CommandText = string.Format("UPDATE ItemtypeCategory SET id_itemtype={0},name='{1}' WHERE id={2}",id_itemtype, name, id);
                            try { this.command.ExecuteNonQuery(); }
                            catch (Exception e2) { MessageBox.Show(e2.Message); }
                        }
                    }
                }
            }
            //3- удаление из базы данных 
            foreach (int i in this.idinDataset)
            {
                this.command.CommandText = string.Format("DELETE FROM ItemtypeCategory WHERE id ={0}", i);
                try { this.command.ExecuteNonQuery(); }
                catch (Exception e1) { MessageBox.Show(e1.Message); }
            }
        }
        private void ManufacturersAddDellUp() 
        {
            //1- переписываем все id из таблицы датасета
            this.idinDataset.Clear();
            foreach (DataRow R in this.dbDataSet.Tables["Manufacturers"].Rows)
                this.idinDataset.Add((int)R["id"]);
            //2- добавление и обновление
            bool iscontains;
            int idUpdate;
            foreach (Manufacturers m in this.LManufacturers)
            {
                int id = m.IDManufacturers;
                string name = m.NameMan;

                // определяем есть ли такой id в таблице в датасете
                iscontains = this.idinDataset.Contains(id);
                if (iscontains)
                    this.idinDataset.Remove(id);

                // если новая запись
                if (id == 0)
                {
                    this.command.CommandText = string.Format("INSERT INTO Manufacturers (name) VALUES('{0}')", name);
                    try { this.command.ExecuteNonQuery(); } //для обновления БД, вызываем после каждого запроса
                    catch (Exception e1) { MessageBox.Show(e1.Message); }
                }
                // строку либо редактировали либо нет
                if (id != 0)
                {
                    // если в загруженном датасете нет записи с такими данными то по catch обновляем( если все совпадает то строку не меняли)
                    idUpdate = 0;
                    var order = from T in this.dbDataSet.Tables["Manufacturers"].AsEnumerable()
                                where T.Field<string>("name") == name
                                select new { idUpdate = T.Field<int>("id") };
                    try { idUpdate = order.Single().idUpdate; }
                    catch (Exception e1)
                    {
                        if (e1.Message == "Последовательность не содержит элементов")
                        {
                            this.command.CommandText = string.Format("UPDATE Manufacturers SET name='{0}' WHERE id={1}", name, id);
                            try { this.command.ExecuteNonQuery(); }
                            catch (Exception e2) { MessageBox.Show(e2.Message); }
                        }
                    }
                }
            }
            //3- удаление из базы данных 
            foreach (int i in this.idinDataset)
            {
                this.command.CommandText = string.Format("DELETE FROM Manufacturers WHERE id ={0}", i);
                try { this.command.ExecuteNonQuery(); }
                catch (Exception e1) { MessageBox.Show(e1.Message); }
            }
        }
        private void ManufacturersModelsAddDellUp() 
        {
            //1- переписываем все id из таблицы датасета
            this.idinDataset.Clear();
            foreach (DataRow R in this.dbDataSet.Tables["ManufacturersModels"].Rows)
                this.idinDataset.Add((int)R["id"]);
            //2- добавление и обновление
            bool iscontains;
            int idUpdate;
            foreach (ManufacturersModels mm in this.LManufacturersModels)
            {
                int id = mm.IDManufacturersModels;
                int id_manufacturers = this.IdManufacturers(mm.NameManufacturers);
                string name = mm.NameManufacturersModels;

                // определяем есть ли такой id в таблице в датасете
                iscontains = this.idinDataset.Contains(id);
                if (iscontains)
                    this.idinDataset.Remove(id);

                // если новая запись
                if (id == 0)
                {
                    this.command.CommandText = string.Format("INSERT INTO ManufacturersModels (id_manufacturers,name) VALUES({0},'{1}')", id_manufacturers, name);
                    try { this.command.ExecuteNonQuery(); } //для обновления БД, вызываем после каждого запроса
                    catch (Exception e1) { MessageBox.Show(e1.Message); }
                }
                // строку либо редактировали либо нет
                if (id != 0)
                {
                    // если в загруженном датасете нет записи с такими данными то по catch обновляем(а если все совпадает то строку не меняли)
                    idUpdate = 0;
                    var order = from T in this.dbDataSet.Tables["ManufacturersModels"].AsEnumerable()
                                where T.Field<string>("name") == name &&
                                      T.Field<int>("id_manufacturers") == id_manufacturers
                                select new { idUpdate = T.Field<int>("id") };
                    try { idUpdate = order.Single().idUpdate; }
                    catch (Exception e1)
                    {
                        if (e1.Message == "Последовательность не содержит элементов")
                        {
                            this.command.CommandText = string.Format("UPDATE ManufacturersModels SET id_manufacturers={0},name='{1}' WHERE id={2}", id_manufacturers, name, id);
                            try { this.command.ExecuteNonQuery(); }
                            catch (Exception e2) { MessageBox.Show(e2.Message); }
                        }
                    }
                }
            }
            //3- удаление из базы данных 
            foreach (int i in this.idinDataset)
            {
                this.command.CommandText = string.Format("DELETE FROM ManufacturersModels WHERE id ={0}", i);
                try { this.command.ExecuteNonQuery(); }
                catch (Exception e1) { MessageBox.Show(e1.Message); }
            }
        }
        private void ProductDetailAddDellUp() 
        {
            //1- переписываем все id из таблицы датасета
            this.idinDataset.Clear();
            foreach (DataRow R in this.dbDataSet.Tables["ProductDetail"].Rows)
                this.idinDataset.Add((int)R["id"]);
            //2- добавление и обновление
            bool iscontains;
            int idUpdate;
            foreach (ProductDetail pd in this.LProductDetail)
            {
                int id = pd.IDProductDetail;
                string[] prod = pd.NameItCat.Split(' ');
                string[] man = pd.NameManMod.Split(' ');
                int id_itemtypecategory = this.IdItemtypeCategory(prod[0], prod[1]);
                int id_manufacturersmodels = this.IdManufacturersModels(man[0], man[1]);
                string name = pd.NameProductDetail;

                // определяем есть ли такой id в таблице в датасете
                iscontains = this.idinDataset.Contains(id);
                if (iscontains)
                    this.idinDataset.Remove(id); // если есть то удаляем, оставляя только те id которые нужно будет потом удалить

                // если новая запись
                if (id == 0)
                {
                    this.command.CommandText = string.Format("INSERT INTO ProductDetail (id_itemtypeCategory,id_manufacturersModels,name) VALUES({0},{1},'{2}')", id_itemtypecategory, id_manufacturersmodels, name);
                    try { this.command.ExecuteNonQuery(); } //для обновления БД, вызываем после каждого запроса
                    catch (Exception e1) { MessageBox.Show(e1.Message); }
                }
                // строку либо редактировали либо нет
                if (id != 0)
                {
                    idUpdate = 0;
                    var order = from T in this.dbDataSet.Tables["ProductDetail"].AsEnumerable()
                                where T.Field<int>("id_itemtypeCategory") == id_itemtypecategory &&
                                      T.Field<int>("id_manufacturersModels") == id_manufacturersmodels &&
                                      T.Field<string>("name") == name
                                select new { idUpdate = T.Field<int>("id") };
                    try { idUpdate = order.Single().idUpdate; }
                    catch (Exception e1)
                    {
                        if (e1.Message == "Последовательность не содержит элементов")
                        {
                            this.command.CommandText = string.Format("UPDATE ProductDetail SET id_itemtypeCategory={0},id_manufacturersModels={1},name='{2}' WHERE id={3}", id_itemtypecategory, id_manufacturersmodels, name, id);
                            try { this.command.ExecuteNonQuery(); }
                            catch (Exception e2) { MessageBox.Show(e2.Message); }
                        }
                    }
                }
            }
            //3- удаление из базы данных (бежим по коллекции id и удаляем все из БД)
            foreach (int i in this.idinDataset)
            {
                this.command.CommandText = string.Format("DELETE FROM ProductDetail WHERE id ={0}", i);
                try { this.command.ExecuteNonQuery(); }
                catch (Exception e1) { MessageBox.Show(e1.Message); }
            }
        }
        private void ProductAddDellUp() 
        {
            //2- обновление
            int idUpdate;
            foreach (Product p in this.LidProductUpdate)
            {
                if(p.NameAllProduct==""||p.NameAllProduct==null || p.NameAllDelivery==""||p.NameAllDelivery==null)
                    return;
                int id = p.IDProduct;
                string[] prod = p.NameAllProduct.Split(' ');
                string[] deliv = p.NameAllDelivery.Split(' ');
                int id_productDetail = this.IdProductDatale(prod[0], prod[1], prod[2], prod[3], prod[4]);
                int id_delivery = this.IdDelivery(deliv[0], deliv[1], deliv[2], deliv[3]);
                int cnt = p.Cnt;
                // все данные типа float переводим к стрингу с заменой , на .
                string price1 = (p.Price1.ToString()).Replace(',','.');
                string price2 = (p.Price2.ToString()).Replace(',', '.');
                string pictures = p.PichterPath;

                // строку либо редактировали либо нет
                if (id != 0)
                {
                    idUpdate = 0;
                    var order = from T in this.dbDataSet.Tables["Product"].AsEnumerable()
                                where T.Field<int>("id_productDetail") == id_productDetail &&
                                      T.Field<int>("id_delivery") == id_delivery &&
                                      T.Field<int>("cnt") == cnt &&
                                      T.Field<float>("price1") ==p.Price1 &&
                                      T.Field<float>("price2") ==p.Price2 &&
                                      T.Field<string>("pictures") == pictures
                                select new { idUpdate = T.Field<int>("id") };
                    try { idUpdate = order.Single().idUpdate; }
                    catch (Exception e1)
                    {
                        if (e1.Message == "Последовательность не содержит элементов")
                        {
                            this.command.CommandText = string.Format("UPDATE Product SET id_productDetail={0},id_delivery={1},cnt={2},price1={3},price2={4},pictures='{5}' WHERE id={6}", id_productDetail, id_delivery, cnt, price1, price2, pictures, id);
                            try { this.command.ExecuteNonQuery(); }
                            catch (Exception e2) { MessageBox.Show(e2.Message); }
                        }
                    }
                }
            }

            // если новая запись
            foreach (Product p in this.LProduct)
            {
                if (p.NameAllProduct == "" || p.NameAllProduct == null || p.NameAllDelivery == "" || p.NameAllDelivery == null)
                    return;
               // int id = p.IDProduct;
                if (p.IDProduct == 0)
                {
                    string[] prod = p.NameAllProduct.Split(' ');
                    string[] deliv = p.NameAllDelivery.Split(' ');
                    int id_productDetail = this.IdProductDatale(prod[0], prod[1], prod[2], prod[3], prod[4]);
                    int id_delivery = this.IdDelivery(deliv[0], deliv[1], deliv[2], deliv[3]);
                    int cnt = p.Cnt;
                    // все данные типа float переводим к стрингу с заменой , на .
                    string price1 = (p.Price1.ToString()).Replace(',', '.');
                    string price2 = (p.Price2.ToString()).Replace(',', '.');
                    string pictures = p.PichterPath;

                    this.command.CommandText = string.Format("INSERT INTO Product (id_productDetail, id_delivery,cnt,price1,price2,pictures) VALUES({0},{1},{2},{3},{4},'{5}')", id_productDetail, id_delivery, cnt, price1, price2, pictures);
                    try { this.command.ExecuteNonQuery(); } //для обновления БД, вызываем после каждого запроса
                    catch (Exception e1) { MessageBox.Show(e1.Message); }
                }
            }
           
            //3 - удаление из базы данных
            foreach (int i in this.LidProductDel)
            {
                this.command.CommandText = string.Format("DELETE FROM Product WHERE id ={0}", i);
                try { this.command.ExecuteNonQuery(); }
                catch (Exception e1) { MessageBox.Show(e1.Message); }
            }
            this.LidProductDel.Clear();
            this.LidProductUpdate.Clear();
        }
        private void LProduct_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e) // обработчик удаления товара 
        {
            if (e.Action == NotifyCollectionChangedAction.Remove)
            {
                Product deleted = e.OldItems[0] as Product;
                this.LidProductDel.Add(deleted.IDProduct);// id продуктов которые удаляем
            }
        }
        private void CountryAddDellUp() 
        {
            //1- переписываем все id из таблицы датасета
            this.idinDataset.Clear();
            foreach (DataRow R in this.dbDataSet.Tables["Country"].Rows)
                this.idinDataset.Add((int)R["id"]);
            //2- добавление и обновление
            bool iscontains;
            int idUpdate;
            foreach (Country cn in this.LCountry)
            {
                int id =cn.IDCountry;
                string name = cn.NameCountry;

                // определяем есть ли такой id в таблице в датасете
                iscontains = this.idinDataset.Contains(id);
                if (iscontains)
                    this.idinDataset.Remove(id);

                // если новая запись
                if (id == 0)
                {
                    this.command.CommandText = string.Format("INSERT INTO Country (name) VALUES('{0}')", name);
                    try { this.command.ExecuteNonQuery(); } //для обновления БД, вызываем после каждого запроса
                    catch (Exception e1) { MessageBox.Show(e1.Message); }
                }
                // строку либо редактировали либо нет
                if (id != 0)
                {
                    // если в загруженном датасете нет записи с такими данными то по catch обновляем( если все совпадает то строку не меняли)
                    idUpdate = 0;
                    var order = from T in this.dbDataSet.Tables["Country"].AsEnumerable()
                                where T.Field<string>("name") == name
                                select new { idUpdate = T.Field<int>("id") };
                    try { idUpdate = order.Single().idUpdate; }
                    catch (Exception e1)
                    {
                        if (e1.Message == "Последовательность не содержит элементов")
                        {
                            this.command.CommandText = string.Format("UPDATE Country SET name='{0}' WHERE id={1}", name, id);
                            try { this.command.ExecuteNonQuery(); }
                            catch (Exception e2) { MessageBox.Show(e2.Message); }
                        }
                    }
                }
            }
            //3- удаление из базы данных 
            foreach (int i in this.idinDataset)
            {
                this.command.CommandText = string.Format("DELETE FROM Country WHERE id ={0}", i);
                try { this.command.ExecuteNonQuery(); }
                catch (Exception e1) { MessageBox.Show(e1.Message); }
            }
        }
        private void ProviderAddDellUp() 
        {
            //1- переписываем все id из таблицы датасета
            this.idinDataset.Clear();
            foreach (DataRow R in this.dbDataSet.Tables["Provider"].Rows)
                this.idinDataset.Add((int)R["id"]);
            //2- добавление и обновление
            bool iscontains;
            int idUpdate;
            foreach (Provider pr in this.LProvider)
            {
                int id = pr.IDProvider;
                string name = pr.NameProvider;

                // определяем есть ли такой id в таблице в датасете
                iscontains = this.idinDataset.Contains(id);
                if (iscontains)
                    this.idinDataset.Remove(id);

                // если новая запись
                if (id == 0)
                {
                    this.command.CommandText = string.Format("INSERT INTO Provider (name) VALUES('{0}')", name);
                    try { this.command.ExecuteNonQuery(); } //для обновления БД, вызываем после каждого запроса
                    catch (Exception e1) { MessageBox.Show(e1.Message); }
                }
                // строку либо редактировали либо нет
                if (id != 0)
                {
                    // если в загруженном датасете нет записи с такими данными то по catch обновляем( если все совпадает то строку не меняли)
                    idUpdate = 0;
                    var order = from T in this.dbDataSet.Tables["Provider"].AsEnumerable()
                                where T.Field<string>("name") == name
                                select new { idUpdate = T.Field<int>("id") };
                    try { idUpdate = order.Single().idUpdate; }
                    catch (Exception e1)
                    {
                        if (e1.Message == "Последовательность не содержит элементов")
                        {
                            this.command.CommandText = string.Format("UPDATE Provider SET name='{0}' WHERE id={1}", name, id);
                            try { this.command.ExecuteNonQuery(); }
                            catch (Exception e2) { MessageBox.Show(e2.Message); }
                        }
                    }
                }
            }
            //3- удаление из базы данных 
            foreach (int i in this.idinDataset)
            {
                this.command.CommandText = string.Format("DELETE FROM Provider WHERE id ={0}", i);
                try { this.command.ExecuteNonQuery(); }
                catch (Exception e1) { MessageBox.Show(e1.Message); }
            }
        }
        private void DeliveryAddDellUp() 
        {
            //1- переписываем все id из таблицы датасета
            this.idinDataset.Clear();
            foreach (DataRow R in this.dbDataSet.Tables["Delivery"].Rows)
                this.idinDataset.Add((int)R["id"]);
            //2- добавление и обновление
            bool iscontains;
            int idUpdate;
            foreach (Delivery dl in this.LDelivery)
            {
                int id = dl.IDDelivery;
                int id_country=this.IdCountry(dl.NameDelivCount);
                string nameform = dl.NameDelivForm;
                int id_provider=this.IdProvider(dl.NameDelivProv);
                float percentage = dl.DelivPercent;
                string p = (percentage.ToString().Replace(',','.'));
                string date = dl.DelivDate;

                // определяем есть ли такой id в таблице в датасете
                iscontains = this.idinDataset.Contains(id);
                if (iscontains)
                    this.idinDataset.Remove(id);

                // если новая запись
                if (id == 0)
                {
                    this.command.CommandText = string.Format("INSERT INTO Delivery (id_country,deliveryform_name,id_provider,percentage,datedelivery) VALUES({0},'{1}',{2},{3},'{4}')",id_country, nameform,id_provider,p,date);
                    try { this.command.ExecuteNonQuery(); } //для обновления БД, вызываем после каждого запроса
                    catch (Exception e1) { MessageBox.Show(e1.Message); }
                }
                // строку либо редактировали либо нет
                if (id != 0)
                {
                    // если в загруженном датасете нет записи с такими данными то по catch обновляем( если все совпадает то строку не меняли)
                    idUpdate = 0;
                    var order = from T in this.dbDataSet.Tables["Delivery"].AsEnumerable()
                                where T.Field<int>("id_country") == id_country &&
                                T.Field<string>("deliveryform_name") == nameform &&
                                T.Field<int>("id_provider") == id_provider &&
                                T.Field<float>("percentage") == percentage &&
                                T.Field<DateTime>("datedelivery")==DateTime.Parse(date)
                                select new { idUpdate = T.Field<int>("id") };
                    try { idUpdate = order.Single().idUpdate; }
                    catch (Exception e1)
                    {
                        if (e1.Message == "Последовательность не содержит элементов")
                        {
                            this.command.CommandText = string.Format("UPDATE Delivery SET id_country={0}, deliveryform_name='{1}',id_provider={2}, percentage={3}, datedelivery='{4}' WHERE id={5}", id_country, nameform, id_provider,p, date, id);
                            try { this.command.ExecuteNonQuery(); }
                            catch (Exception e2) { MessageBox.Show(e2.Message); }
                        }
                    }
                }
            }
            //3- удаление из базы данных 
            foreach (int i in this.idinDataset)
            {
                this.command.CommandText = string.Format("DELETE FROM Delivery WHERE id ={0}", i);
                try { this.command.ExecuteNonQuery(); }
                catch (Exception e1) { MessageBox.Show(e1.Message); }
            }
        }
        private void SalerNamesAddDellUp() 
        {
            //1- переписываем все id из таблицы датасета
            this.idinDataset.Clear();
            foreach (DataRow R in this.dbDataSet.Tables["SalerNames"].Rows)
                this.idinDataset.Add((int)R["id"]);
            //2- добавление и обновление
            bool iscontains;
            int idUpdate;
            foreach (SalerNames sn in this.LSalerNames)
            {
                int id = sn.IDSelerName;
                string name = sn.NameSalernames;
                float percent = sn.SelerPercent;
                string p = (sn.SelerPercent.ToString()).Replace(',', '.');

                // определяем есть ли такой id в таблице в датасете
                iscontains = this.idinDataset.Contains(id);
                if (iscontains)
                    this.idinDataset.Remove(id);

                // если новая запись
                if (id == 0)
                {
                    this.command.CommandText = string.Format("INSERT INTO SalerNames (name,percented) VALUES('{0}',{1})", name,p);
                    try { this.command.ExecuteNonQuery(); } //для обновления БД, вызываем после каждого запроса
                    catch (Exception e1) { MessageBox.Show(e1.Message); }
                }
                // строку либо редактировали либо нет
                if (id != 0)
                {
                    // если в загруженном датасете нет записи с такими данными то по catch обновляем( если все совпадает то строку не меняли)
                    idUpdate = 0;
                    var order = from T in this.dbDataSet.Tables["SalerNames"].AsEnumerable()
                                where T.Field<string>("name") == name &&
                                      T.Field<float>("percented") == percent
                                select new { idUpdate = T.Field<int>("id") };
                    try { idUpdate = order.Single().idUpdate; }
                    catch (Exception e1)
                    {
                        if (e1.Message == "Последовательность не содержит элементов")
                        {
                            this.command.CommandText = string.Format("UPDATE SalerNames SET name='{0}', percented={1} WHERE id={2}", name, p,id);
                            try { this.command.ExecuteNonQuery(); }
                            catch (Exception e2) { MessageBox.Show(e2.Message); }
                        }
                    }
                }
            }
            //3- удаление из базы данных 
            foreach (int i in this.idinDataset)
            {
                this.command.CommandText = string.Format("DELETE FROM SalerNames WHERE id ={0}", i);
                try { this.command.ExecuteNonQuery(); }
                catch (Exception e1) { MessageBox.Show(e1.Message); }
            }
        }
        private void SaleAddDellUp() // метод  удаления проданного товара 
        {
            //1- переписываем все id из таблицы датасета
            this.idinDataset.Clear();
            foreach (DataRow R in this.dbDataSet.Tables["Sale"].Rows)
                this.idinDataset.Add((int)R["id"]);
         
            bool iscontains;
            foreach (Sale sl in this.LSale)
            {
                int id =sl.IDSale;
                // определяем есть ли такой id в таблице в датасете
                iscontains = this.idinDataset.Contains(id);
                if (iscontains)
                    this.idinDataset.Remove(id);
            }
            //3- удаление из базы данных 
            foreach (int i in this.idinDataset)
            {
                this.command.CommandText = string.Format("DELETE FROM Sale WHERE id ={0}", i);
                try { this.command.ExecuteNonQuery(); }
                catch (Exception e1) { MessageBox.Show(e1.Message); }
            }
        }
        private void BadBugsAddDellUp() 
        {

        }
        
        //************* методы обновления и сохранения
        private void UpdateTablesCode()// метод очишает все таблица и перезаполняет данные ( используем в 2х методах ниже ) 
        {
            this.DataGridView.Columns.Clear();          // чистим таблицу GridView
            this.DataGridProduct.Columns.Clear();       // чистим таблицу GridProduct
            this.dbDataSet.Clear();                     // чистим таблицу DataSet
            this.dbDataSet.Relations.Clear();           // чистим взаимосвязи
            this.ClearLists();                          // чистим все  ObservableCollection
            this.FillingDataSet();                      // заполняем DataSet, проставляем связи и заполняем коллекции 
        }
        private void UpdateGridView() // метод обновляет GridViev ( в обработчике кнопок Sawe + Update ) 
        {
            this.dbConection.Open();
            //****************************************************
            this.SaleAddDellUp();
            this.ProductAddDellUp();
            this.ProductDetailAddDellUp();

            this.ItemtypeAddDellUp();
            this.ItemtypeCategoryAddDellUp();
            this.ManufacturersAddDellUp();
            this.ManufacturersModelsAddDellUp();
           
            this.CountryAddDellUp();
            this.ProviderAddDellUp();
            this.DeliveryAddDellUp();

            this.SalerNamesAddDellUp();
           
            // this.BadBugsAddDellUp();

            //****************************************************
            this.UpdateTablesCode(); // очишаем и перезаполняем таблицы
            // загружаем все таблицы по новому в зависимости от того какая открыта (т.е чему равен индекс комбобокса таблиц)
            this.SelectingTables_toLoad(this.comboBox_TableName.SelectedItem.ToString());
            this.dbConection.Close();
           // this.IsUpdateProduct = false;
        }
        private void Sawe_Click(object sender, RoutedEventArgs e)  // сохраняем изминения в базу данных 
        {
            MessageBoxResult result= MessageBox.Show("Обновить базу данных ?", "Внести изминения в таблицы", MessageBoxButton.YesNo,MessageBoxImage.Question);
            if (result == MessageBoxResult.Yes)
                this.UpdateGridView();
        }
        private void Update_Click(object sender, RoutedEventArgs e) // обновляем текущие таблицы GridView 
        {
            this.UpdateTablesCode();// очишаем и перезаполняем таблицы
            // загружаем все таблицы по новому в зависимости от того какая открыта (т.е чему равен индекс комбобокса таблиц)
            this.SelectingTables_toLoad(this.comboBox_TableName.SelectedItem.ToString());
        }

        #endregion

        #region *************************************** Методы картинок в Product, установка даты в Delivery, комбобокс прибыли, смена учетки
        private void MainWindow_button_Click(object sender, RoutedEventArgs e) // обработчик добавления картинки в товар 
        {
            OpenFileDialog OFD = new OpenFileDialog();
            OFD.Filter = "Фото(*.jpg)|*.jpg|Рисунок(*.bmp)|*.bmp|Рисунок(*.png)|*.png";
            //OFD.Filter = "Фото(*.jpg)|*.jpg       |Рисунок(*.bmp)|*.bmp        |Рисунок(*.png)|*.png";
            OFD.ShowReadOnly = true;
            if (OFD.ShowDialog() == true)
            {
                FileInfo Pichter = new FileInfo(OFD.FileName); // файл который хотим открыть (картинка)
                string pichtersizeKB =string.Format("{0}", (float)Pichter.Length * 0.001);  // размер файла
                string[] size = pichtersizeKB.Split(',');// убираем , получаем целую часть
                if (int.Parse(size[0]) > 100)
                {
                    MessageBox.Show("Максимальный размер загружаемой картинки 100 Кб", "Внимание", MessageBoxButton.OK, MessageBoxImage.Information);
                    return;
                }
                object r = e.Source;
                Button b = (Button)r;
                Product p = (Product)b.DataContext;
                p.PichterPath = OFD.FileName;
            }
        }
        private void DataGridView_SelectionChanged(object sender, SelectionChangedEventArgs e) // выбираем картинку для загрузки 
        {
            DataGrid dg = sender as DataGrid;
            Product p = dg.SelectedItem as Product;
            if (p != null)
            {
                try
                {
                    if (p.PichterPath != "" && p.IDProduct > 0)
                    {
                        BitmapImage bmp = new BitmapImage();
                        bmp.BeginInit();
                        bmp.UriSource = new Uri(p.PichterPath);
                        bmp.EndInit();
                        this.image.Stretch = Stretch.UniformToFill;
                        this.image.Source = bmp;
                    }
                    else
                    {
                        this.image.Stretch = Stretch.Uniform;
                        this.image.Source = new BitmapImage(new Uri("pack://application:,,,/Resources/Noimage.png"));
                    }
                }
                catch (Exception e1) { MessageBox.Show(e1.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Stop); }
            }

        }
        private void days_Selected(object sender, SelectionChangedEventArgs e) // обработчик установки даты при редактировании в Delivery
        {
            DatePicker dp = e.Source as DatePicker;
            Delivery d = (Delivery)dp.DataContext;
            DateTime DT = DateTime.Parse(dp.ToString());
            d.DelivDate = DT.ToShortDateString();
        }
        private void ComboBox_Profit_SelectionChanged(object sender, SelectionChangedEventArgs e)// комбобокс подсчета прибыли 
        {
            string text = this.comboBox_Profit.SelectedItem.ToString();
            switch (text)
            {
                case "за день": break;
                case "за неделю": break;
                case "за месяц": break;
                case "за 6 месяцев": break;
                case "за 12 месяцев": break;
            }
        }
        private void button_SaweCourse_Click(object sender, RoutedEventArgs e) // обработчик изминения курса $ 
        {
            MessageBoxResult result = MessageBox.Show("Изменить курс $ ?", "Внимание", MessageBoxButton.YesNo, MessageBoxImage.Question);
            if (result == MessageBoxResult.Yes)
            {
                this.dbConection.Open();
                // находим id записи
                int id = 0;
                this.dbCommand = this.dbConection.CreateCommand();
                this.dbCommand.CommandText = string.Format("SELECT Admin.id FROM Admin WHERE Admin.name='{0}' AND Admin.password='{1}'", this.nameAdmin, this.passwordAdmin);
                try
                {
                    DbDataReader R = this.dbCommand.ExecuteReader();
                    if (R.HasRows)// если содержит строки
                        while (R.Read())
                            id = R.GetInt32(0);
                    R.Close();
                }
                catch (Exception e1) { MessageBox.Show(e1.Message + "\n Изминения в базу данных Не внесены !!! ","Ошибка",MessageBoxButton.OK,MessageBoxImage.Error); }
                
                // меняем значение курса $
                this.course = float.Parse((this.textbox_Course.Text).Replace('.', ','));
                this.command.CommandText = string.Format("UPDATE Admin SET course={0} WHERE id={1}",( this.textbox_Course.Text).Replace(',', '.'),id);
                try { this.command.ExecuteNonQuery(); }
                catch (Exception e2) { MessageBox.Show(e2.Message); }
                finally { this.dbConection.Close(); }
            }
        }
        private void button_EditAdmin_Click(object sender, RoutedEventArgs e)// обработчик изменения имени и пароля
        {
            Window1 W2 = new Window1();
            W2.textBox_Name.Text = this.nameAdmin;
            W2.passwordBox_Pasword.Password = this.passwordAdmin;
            W2.label_Name.Content = " имя";
            W2.label_Password.Content = " пароль";
            W2.grid.Background = Brushes.Beige;
            W2.WindowStartupLocation = WindowStartupLocation.Manual;
            if (W2.ShowDialog() == true)
                MessageBox.Show("Ваше имя и пароль успешно изменены !", "Сообщение", MessageBoxButton.OK, MessageBoxImage.Exclamation);
        }
        private void buttonSearchProd_Click(object sender, RoutedEventArgs e) // кнопка поиска по комбобоксам 
        {
            this.LProduct.Clear(); // чистим коллекцию Product
            this.LsearchProduct.Clear();// чистим список с перечнем из комбобоксов
            string fullname;
            //заполняем список с выбранными значениями из комбобокса
            for (int i = 0; i < this.ArrComboboxSearchProduct.Length; i++)
            {
                fullname = null;
                if(this.ArrComboboxSearchProduct[i].SelectedItem!=null)
                {
                    fullname = string.Format(this.Arr_nameTable[i], this.ArrComboboxSearchProduct[i].SelectedItem.ToString());
                    this.LsearchProduct.Add(fullname);
                }
            }
            RequestProduct RP = new RequestProduct(this.dbConection,this.dbAdapter,this.dbDataSet,this.LProduct);
            this.DataGridProduct.Columns.Clear(); // чистим DataGrid
            RP.GetProductrequestCollection(this.LsearchProduct); // отправляем запрос и заполняем OC LProduct
            this.TableProduct();// создаем колонки в DataGrid и заполняем
        }
        #endregion

        #region *************************************** Загружаем админские таблицы 
        private void RequestProdDetail() // запрос для заполнения таблицы описания товара 
        {
            DbCommand com= this.dbConection.CreateCommand();
            com.CommandText = "SELECT  ProductDetail.id,Itemtype.name As Itname,ItemtypeCategory.name As ITCname,Manufacturers.name As Mannames,ManufacturersModels.name As ManModname,ProductDetail.name As Pdname FROM   Itemtype,ItemtypeCategory,Manufacturers,ManufacturersModels,ProductDetail WHERE  ProductDetail.id_itemtypeCategory = ItemtypeCategory.id AND ItemtypeCategory.id_itemtype = Itemtype.id AND ProductDetail.id_manufacturersModels = ManufacturersModels.id AND ManufacturersModels.id_manufacturers = Manufacturers.id";
            this.dbAdapter.SelectCommand =com;
            this.dbAdapter.Fill(this.dbDataSet, "ProductDetail2");

            foreach (DataRow r in this.dbDataSet.Tables["ProductDetail2"].Rows)
            {
                ProductDetail PD = new ProductDetail();
                PD.IDProductDetail = (int)r["id"];
                PD.NameItCat = (r["Itname"].ToString() + " " + r["ITCname"].ToString());
                PD.NameManMod=(r["Mannames"].ToString() + " " + r["ManModname"].ToString());
                PD.NameProductDetail = r["Pdname"].ToString();
                this.LProductDetail.Add(PD);
            }
        }
        private void RequestDelivery() // запрос для заполнения таблицы доставки 
        {
            var order = from T in this.dbDataSet.Tables["Country"].AsEnumerable()
                        from T1 in this.dbDataSet.Tables["Delivery"].AsEnumerable()
                        from T2 in this.dbDataSet.Tables["Provider"].AsEnumerable()
                        where
                        T1.Field<int>("id_country") == T.Field<int>("id") &&
                        T1.Field<int>("id_provider") == T2.Field<int>("id")
                        select new
                        {
                            id = T1.Field<int>("id"),
                            nameCountry = T.Field<string>("name"),
                            nameForm = T1.Field<string>("deliveryform_name"),
                            nameProv = T2.Field<string>("name"),
                            percent = T1.Field<float>("percentage"),
                            date = T1.Field<DateTime>("datedelivery")
                        };
            foreach (var q in order)
            {
                this.LDelivery.Add(new Delivery(q.id, q.nameCountry, q.nameForm, q.nameProv, q.percent, q.date.ToShortDateString()));
                this.combobox_DateDelivery.Items.Add(q.date.ToShortDateString()); // заполняем комбобокс для поиска в товарах
            }
                
        }
        private void RequestProd() // запрос для заполнения таблицы товара 
        {
            DbCommand com = this.dbConection.CreateCommand();
            com.CommandText = "SELECT Product.id As id, Itemtype.name As Itname ,ItemtypeCategory.name As ItemCatname,Manufacturers.name As Manname,ManufacturersModels.name AS ManModnames,ProductDetail.name AS Pdname, Country.name AS Cname,Delivery.deliveryform_name As Delname,Provider.name AS Provname,Delivery.datedelivery AS Deldate,Product.cnt AS cnt,Product.price1 AS price1 ,Product.price2 AS price2,Product.pictures As pictures FROM Itemtype, ItemtypeCategory,Manufacturers, ManufacturersModels,ProductDetail,Country, Provider, Delivery,Product WHERE ProductDetail.id_itemtypeCategory = ItemtypeCategory.id AND ItemtypeCategory.id_itemtype = Itemtype.id AND ProductDetail.id_manufacturersModels = ManufacturersModels.id AND ManufacturersModels.id_manufacturers = Manufacturers.id AND Delivery.id_country = Country.id AND Delivery.id_provider = Provider.id AND Product.id_delivery = Delivery.id AND Product.id_productDetail = ProductDetail.id";
            this.dbAdapter.SelectCommand = com;
            this.dbAdapter.Fill(this.dbDataSet, "Product2");

            foreach (DataRow r in this.dbDataSet.Tables["Product2"].Rows)
            {
                Product PD = new Product();
                PD.IDProduct = (int)r["id"];
                PD.NameAllProduct = (r["Itname"].ToString() + " " + r["ItemCatname"].ToString()+" "+r["Manname"].ToString()+" "+r["ManModnames"].ToString()+" "+r["Pdname"].ToString());
                PD.Cnt = (int)r["cnt"];
                DateTime dt = (DateTime)r["Deldate"];
                string date = dt.ToShortDateString();
                PD.NameAllDelivery= (r["Cname"].ToString() + " " + r["Delname"].ToString()+" "+ r["Provname"].ToString() + " " + date);
                PD.Price1 = (float)r["price1"];
                PD.Price2 = (float)r["price2"];
                PD.PichterPath = r["pictures"].ToString();
                this.LProduct.Add(PD);
                this.LProductFromClient.Add(PD);
            }
        }
        private void RequestSales() // запрос для заполнения таблицы продаж 
        {
            DbCommand com = this.dbConection.CreateCommand();
            com.CommandText = "SELECT Sale.id As id, Itemtype.name As Itname, ItemtypeCategory.name As ItCname,Manufacturers.name As Manname,ManufacturersModels.name AS ManModName,ProductDetail.name AS ProdDetname,Country.name AS Cname,Delivery.deliveryform_name As DelFormname,Provider.name AS Provname,Delivery.datedelivery AS Delivname,SalerNames.name AS salername,Sale.cnt AS cnt,Sale.price AS saleprice,Sale.summa AS Summ,Sale.profitAll AS profitall,Sale.profitSaler AS profitsaler,Sale.dateSale AS datasale FROM Itemtype,ItemtypeCategory, Manufacturers,ManufacturersModels, ProductDetail, Country,Provider,Delivery, Product,SalerNames,Sale WHERE ProductDetail.id_itemtypeCategory = ItemtypeCategory.id AND ItemtypeCategory.id_itemtype = Itemtype.id AND ProductDetail.id_manufacturersModels = ManufacturersModels.id AND ManufacturersModels.id_manufacturers = Manufacturers.id AND Delivery.id_country = Country.id AND Delivery.id_provider = Provider.id AND Product.id_delivery = Delivery.id AND Product.id_productDetail = ProductDetail.id AND Sale.id_product = Product.id AND Sale.id_salerNames = SalerNames.id";
            this.dbAdapter.SelectCommand = com;
            this.dbAdapter.Fill(this.dbDataSet, "Sale2");

            foreach (DataRow r in this.dbDataSet.Tables["Sale2"].Rows)
            {
                Sale S = new Sale();
                S.IDSale = (int)r["id"];
                DateTime dt = (DateTime)r["Delivname"];
                string date = dt.ToShortDateString();
                S.ProductName = r["Itname"].ToString() + " " + r["ItCname"].ToString() + " " + r["Manname"].ToString() + " " + r["ManModName"].ToString() + " " + r["ProdDetname"].ToString(); /*+" "+*/
                S.DeliveryName = r["Cname"].ToString() + " " + r["DelFormname"].ToString() + " " + r["Provname"].ToString() + " " + date;
                S.SalerName = r["salername"].ToString();
                S.CntProduct = (int)r["cnt"];
                S.SalePrice = (float)r["saleprice"];
                S.SaleSumm = (float)r["Summ"];
                S.ProfitAll = (float)r["profitall"];
                S.ProfitSaller = (float)r["profitsaler"];
                DateTime dt1 = (DateTime)r["datasale"];
                string date1 = dt1.ToShortDateString();
                S.SaleDate = date1;
                this.LSale.Add(S);
            }
        }
       
        // ***************************************** колонки GridView
        private void TableItemtype() 
        {
            DataGridTextColumn TC = new DataGridTextColumn();
            TC.Header = "ID";
            TC.IsReadOnly = true;
            Binding B = new Binding();
            B.Path = new PropertyPath("IDItemtupe");
            B.UpdateSourceTrigger = UpdateSourceTrigger.PropertyChanged;
            TC.Binding = B;
            this.DataGridView.Columns.Add(TC);

            DataGridTextColumn TC1 = new DataGridTextColumn();
            TC1.Header = "Категория";
            Binding B1 = new Binding();
            B1.Path = new PropertyPath("NameIt");
            B1.UpdateSourceTrigger = UpdateSourceTrigger.PropertyChanged;
            TC1.Binding = B1;
            this.DataGridView.Columns.Add(TC1);

            this.DataGridView.ItemsSource = this.LItemtype;
        }
        private void TableItemtypeCategory() 
        {
            // заполняем комбобокс
            List<string> AllItems = new List<string>();
            foreach (DataRow r in this.dbDataSet.Tables["Itemtype"].Rows)
                AllItems.Add(r["name"].ToString());

            //********************* создаем колонки и устанавливаем привязки к свойствам класса
            DataGridTextColumn TC = new DataGridTextColumn();
            TC.Header = "ID";
            TC.IsReadOnly = true;
            Binding B = new Binding();
            B.UpdateSourceTrigger = UpdateSourceTrigger.PropertyChanged;
            B.Path = new PropertyPath("ID");
            TC.Binding = B;
            this.DataGridView.Columns.Add(TC);

            DataGridTemplateColumn TGC = new DataGridTemplateColumn();
            TGC.Header = "Тип товара";
            TGC.Width = 155;
            // создаем шаблон
            DataTemplate DT = new DataTemplate();
            // создаем объект фабрики
            FrameworkElementFactory fefMain = new FrameworkElementFactory(typeof(ComboBox));
            // устанавливаем привязку 
            Binding B1 = new Binding();
            B1.UpdateSourceTrigger = UpdateSourceTrigger.PropertyChanged;
            B1.Path = new PropertyPath("NameItemtype");                         // к кому привязываемся
            TGC.SortMemberPath = "NameItemtype";                                // задаем привязку по сортировке
            fefMain.SetBinding(ComboBox.SelectedItemProperty, B1);              // устанавливаем привязку к свойству
            fefMain.SetValue(ComboBox.ItemsSourceProperty, AllItems);           // заполняем комбобокс
            fefMain.SetValue(ComboBox.BackgroundProperty,Brushes.AliceBlue);
            DT.VisualTree = fefMain;
            TGC.CellTemplate = DT;
            this.DataGridView.Columns.Add(TGC);

            DataGridTextColumn TC1 = new DataGridTextColumn();
            TC1.Header = "Категория товара";
            Binding B2 = new Binding();
            B2.UpdateSourceTrigger = UpdateSourceTrigger.PropertyChanged;
            B2.Path = new PropertyPath("Name");
            TC1.Binding = B2;
            this.DataGridView.Columns.Add(TC1);

            // заполняем DataGrid
            this.DataGridView.ItemsSource =this.LItemtypeCategory;
        }
        private void TableManufacturers() 
        {
            DataGridTextColumn TC = new DataGridTextColumn();
            TC.Header = "ID";
            Binding B = new Binding();
            B.UpdateSourceTrigger = UpdateSourceTrigger.PropertyChanged;
            B.Path = new PropertyPath("IDManufacturers");
            TC.Binding = B;
            this.DataGridView.Columns.Add(TC);

            DataGridTextColumn TC1 = new DataGridTextColumn();
            TC1.Header = "Производитель";
            Binding B1 = new Binding();
            B1.UpdateSourceTrigger = UpdateSourceTrigger.PropertyChanged;
            B1.Path = new PropertyPath("NameMan");
            TC1.Binding = B1;
            this.DataGridView.Columns.Add(TC1);

            // заполняем DataGrid
            this.DataGridView.ItemsSource =this.LManufacturers;
        }
        private void TableManufacturersModels() 
        {
            // заполняем комбобокс
            List<string> AllManufacturers = new List<string>();
            foreach (DataRow r in this.dbDataSet.Tables["Manufacturers"].Rows)
                AllManufacturers.Add(r["name"].ToString());

            //********************* создаем колонки и устанавливаем привязки к свойствам класса
            DataGridTextColumn TC = new DataGridTextColumn();
            TC.Header = "ID";
            TC.IsReadOnly = true;
            Binding B = new Binding();
            B.UpdateSourceTrigger = UpdateSourceTrigger.PropertyChanged;
            B.Path = new PropertyPath("IDManufacturersModels");
            TC.Binding = B;
            this.DataGridView.Columns.Add(TC);

            DataGridTemplateColumn TGC = new DataGridTemplateColumn();
            TGC.Header = "Производитель";
            TGC.Width = 100;
            // создаем шаблон
            DataTemplate DT = new DataTemplate();
            // создаем объект фабрики
            FrameworkElementFactory fefMain = new FrameworkElementFactory(typeof(ComboBox));
            // устанавливаем привязку 
            Binding B1 = new Binding();
            B1.UpdateSourceTrigger = UpdateSourceTrigger.PropertyChanged;
            B1.Path = new PropertyPath("NameManufacturers");                        // к кому привязываемся
            TGC.SortMemberPath = "NameManufacturers";                               // задаем привязку по сортировке
            fefMain.SetBinding(ComboBox.SelectedItemProperty, B1);                  // устанавливаем привязку к свойству
            fefMain.SetValue(ComboBox.ItemsSourceProperty, AllManufacturers);       // заполняем комбобокс
            fefMain.SetValue(ComboBox.BackgroundProperty, Brushes.AliceBlue);
            DT.VisualTree = fefMain;
            TGC.CellTemplate = DT;
            this.DataGridView.Columns.Add(TGC);

            DataGridTextColumn TC1 = new DataGridTextColumn();
            TC1.Header = "Модель";
            Binding B2 = new Binding();
            B2.UpdateSourceTrigger = UpdateSourceTrigger.PropertyChanged;
            B2.Path = new PropertyPath("NameManufacturersModels");
            TC1.Binding = B2;
            this.DataGridView.Columns.Add(TC1);

            // заполняем DataGrid
            this.DataGridView.ItemsSource = this.LManufacturersModels;
        }
        private void TableProductDetail() 
        {
            // заполняем комбобоксы
            List<string> AllItemsCategory = new List<string>();   
            foreach (DataRow r in this.dbDataSet.Tables["Itemtype"].Rows) // бежим по первой таблице
            {
                // получаем дочерние елементы
                DataRow[] childRows = r.GetChildRows(this.dbDataSet.Relations["Itemtype_ItemtypeCategory"]);
                for (int i = 0; i < childRows.Length; i++)
                    AllItemsCategory.Add((r["name"].ToString()+" "+childRows[i]["name"].ToString()));
            }
            List<string> AllManufacturers = new List<string>();
            foreach (DataRow r in this.dbDataSet.Tables["Manufacturers"].Rows) // бежим по первой таблице
            {
                // получаем дочерние елементы
                DataRow[] childRows = r.GetChildRows(this.dbDataSet.Relations["Manufacturers_ManufacturersModels"]);
                for (int i = 0; i < childRows.Length; i++)
                    AllManufacturers.Add((r["name"].ToString()+" "+childRows[i]["name"].ToString()));
            }

            //********************* создаем колонки и устанавливаем привязки к свойствам класса
            DataGridTextColumn TC = new DataGridTextColumn();
            TC.Header = "ID";
            TC.IsReadOnly = true;
            Binding B = new Binding();
            B.UpdateSourceTrigger = UpdateSourceTrigger.PropertyChanged;
            B.Path = new PropertyPath("IDProductDetail");
            TC.Binding = B;
            this.DataGridView.Columns.Add(TC);

            DataGridTemplateColumn TGC = new DataGridTemplateColumn();
            TGC.Header = "Категория и тип товара";
            TGC.Width = 200;
            // создаем шаблон
            DataTemplate DT = new DataTemplate();
            // создаем объект фабрики
            FrameworkElementFactory fefMain = new FrameworkElementFactory(typeof(ComboBox));
            // устанавливаем привязку 
            Binding B1 = new Binding();
            B1.UpdateSourceTrigger = UpdateSourceTrigger.PropertyChanged;
            B1.Path = new PropertyPath("NameItCat");                                // к кому привязываемся
            TGC.SortMemberPath = "NameItCat";                                       // задаем привязку по сортировке
            fefMain.SetBinding(ComboBox.SelectedItemProperty, B1);                  // устанавливаем привязку к свойству
            fefMain.SetValue(ComboBox.ItemsSourceProperty, AllItemsCategory);       // заполняем комбобокс
            fefMain.SetValue(ComboBox.BackgroundProperty, Brushes.AliceBlue);
            DT.VisualTree = fefMain;
            TGC.CellTemplate = DT;
            this.DataGridView.Columns.Add(TGC);

            DataGridTemplateColumn TGC1 = new DataGridTemplateColumn();
            TGC1.Header = "Производитель и модель";
            TGC1.Width = 200;
            // создаем шаблон
            DataTemplate DT1 = new DataTemplate();
            // создаем объект фабрики
            FrameworkElementFactory fefMain1 = new FrameworkElementFactory(typeof(ComboBox));
            // устанавливаем привязку 
            Binding B2 = new Binding();
            B2.UpdateSourceTrigger = UpdateSourceTrigger.PropertyChanged;
            B2.Path = new PropertyPath("NameManMod");                                // к кому привязываемся
            TGC1.SortMemberPath = "NameManMod";                                      // задаем привязку по сортировке
            fefMain1.SetBinding(ComboBox.SelectedItemProperty, B2);                  // устанавливаем привязку к свойству
            fefMain1.SetValue(ComboBox.ItemsSourceProperty,AllManufacturers);        // заполняем комбобокс
            fefMain1.SetValue(ComboBox.BackgroundProperty, Brushes.AliceBlue);
            DT1.VisualTree = fefMain1;
            TGC1.CellTemplate = DT1;
            this.DataGridView.Columns.Add(TGC1);

            DataGridTextColumn TC1 = new DataGridTextColumn();
            TC1.Header = "Название";
            Binding B3 = new Binding();
            B3.UpdateSourceTrigger = UpdateSourceTrigger.PropertyChanged;
            B3.Path = new PropertyPath("NameProductDetail");
            TC1.Binding = B3;
            this.DataGridView.Columns.Add(TC1);

            // заполняем DataGrid
            this.DataGridView.ItemsSource =this.LProductDetail;
        }
        private void TableCountry() 
        {
            DataGridTextColumn TC = new DataGridTextColumn();
            TC.Header = "ID";
            Binding B = new Binding();
            B.UpdateSourceTrigger = UpdateSourceTrigger.PropertyChanged;
            B.Path = new PropertyPath("IDCountry");
            TC.Binding = B;
            this.DataGridView.Columns.Add(TC);

            DataGridTextColumn TC1 = new DataGridTextColumn();
            TC1.Header = "Страна";
            Binding B1 = new Binding();
            B1.UpdateSourceTrigger = UpdateSourceTrigger.PropertyChanged;
            B1.Path = new PropertyPath("NameCountry");
            TC1.Binding = B1;
            this.DataGridView.Columns.Add(TC1);

            // заполняем DataGrid
            this.DataGridView.ItemsSource =this.LCountry;
        }
        private void TableProvider() 
        {
            DataGridTextColumn TC = new DataGridTextColumn();
            TC.Header = "ID";
            Binding B = new Binding();
            B.UpdateSourceTrigger = UpdateSourceTrigger.PropertyChanged;
            B.Path = new PropertyPath("IDProvider");
            TC.Binding = B;
            this.DataGridView.Columns.Add(TC);

            DataGridTextColumn TC1 = new DataGridTextColumn();
            TC1.Header = "Поставщик";
            Binding B1 = new Binding();
            B1.UpdateSourceTrigger = UpdateSourceTrigger.PropertyChanged;
            B1.Path = new PropertyPath("NameProvider");
            TC1.Binding = B1;
            this.DataGridView.Columns.Add(TC1);

            // заполняем DataGrid
            this.DataGridView.ItemsSource = this.LProvider;
        }
        private void TableDelivery() 
        {
            // заполняем комбобоксы
            List<string> AllCountry = new List<string>();
            foreach (DataRow r in this.dbDataSet.Tables["Country"].Rows) 
                AllCountry.Add((r["name"].ToString()));
            
            List<string> AllProvider = new List<string>();
            foreach (DataRow r in this.dbDataSet.Tables["Provider"].Rows) 
                AllProvider.Add((r["name"].ToString()));

            //********************* создаем колонки и устанавливаем привязки к свойствам класса
            DataGridTextColumn TC = new DataGridTextColumn();
            TC.Header = "ID";
            TC.IsReadOnly = true;
            Binding B = new Binding();
            B.UpdateSourceTrigger = UpdateSourceTrigger.PropertyChanged;
            B.Path = new PropertyPath("IDDelivery");
            TC.Binding = B;
            this.DataGridView.Columns.Add(TC);

            DataGridTemplateColumn TGC = new DataGridTemplateColumn();
            TGC.Header = "Страна";
            TGC.Width = 90;
            // создаем шаблон
            DataTemplate DT = new DataTemplate();
            // создаем объект фабрики
            FrameworkElementFactory fefMain = new FrameworkElementFactory(typeof(ComboBox));
            // устанавливаем привязку 
            Binding B1 = new Binding();
            B1.UpdateSourceTrigger = UpdateSourceTrigger.PropertyChanged;
            B1.Path = new PropertyPath("NameDelivCount");                                // к кому привязываемся
            TGC.SortMemberPath = "NameDelivCount";                                       // задаем привязку по сортировке
            fefMain.SetBinding(ComboBox.SelectedItemProperty, B1);                       // устанавливаем привязку к свойству
            fefMain.SetValue(ComboBox.ItemsSourceProperty, AllCountry);                  // заполняем комбобокс
            fefMain.SetValue(ComboBox.BackgroundProperty, Brushes.AliceBlue);
            DT.VisualTree = fefMain;
            TGC.CellTemplate = DT;
            this.DataGridView.Columns.Add(TGC);

            DataGridTextColumn TC1 = new DataGridTextColumn();
            TC1.Header = "Вид доставки";
            TC1.Width = 90;
            Binding B2 = new Binding();
            B2.UpdateSourceTrigger = UpdateSourceTrigger.PropertyChanged;
            B2.Path = new PropertyPath("NameDelivForm");
            TC1.Binding = B2;
            this.DataGridView.Columns.Add(TC1);

            DataGridTemplateColumn TGC1 = new DataGridTemplateColumn();
            TGC1.Header = "Поставщик";
            TGC1.Width = 80;
            // создаем шаблон
            DataTemplate DT1 = new DataTemplate();
            // создаем объект фабрики
            FrameworkElementFactory fefMain1 = new FrameworkElementFactory(typeof(ComboBox));
            // устанавливаем привязку 
            Binding B3 = new Binding();
            B3.UpdateSourceTrigger = UpdateSourceTrigger.PropertyChanged;
            B3.Path = new PropertyPath("NameDelivProv");                                
            TGC1.SortMemberPath = "NameDelivProv";                                     
            fefMain1.SetBinding(ComboBox.SelectedItemProperty, B3);                  
            fefMain1.SetValue(ComboBox.ItemsSourceProperty, AllProvider);
            fefMain1.SetValue(ComboBox.BackgroundProperty, Brushes.AliceBlue);
            DT1.VisualTree = fefMain1;
            TGC1.CellTemplate = DT1;
            this.DataGridView.Columns.Add(TGC1);

            DataGridTextColumn TC2 = new DataGridTextColumn();
            TC2.Header = "Процент";
            TC2.Width = 60;
            Binding B4 = new Binding();
            B4.UpdateSourceTrigger = UpdateSourceTrigger.LostFocus;
            B4.Path = new PropertyPath("DelivPercent");
            TC2.Binding = B4;
            this.DataGridView.Columns.Add(TC2);

            DataGridTextColumn TC3 = new DataGridTextColumn();
            TC3.Header = "Дата";
            TC3.Width = 80;
            Binding B5 = new Binding();
            B5.UpdateSourceTrigger = UpdateSourceTrigger.PropertyChanged;
            B5.Path = new PropertyPath("DelivDate");
            TC3.Binding = B5;
            this.DataGridView.Columns.Add(TC3);

            DataGridTemplateColumn TGTCDate = new DataGridTemplateColumn();
            TGTCDate.Header = "Дата";
            TGTCDate.Visibility = Visibility.Hidden;
            // создаем шаблон
            DataTemplate DTdate = new DataTemplate();
            // создаем объект фабрики
            FrameworkElementFactory fefMainDate = new FrameworkElementFactory(typeof(DatePicker));
            // событие изминения даты
            fefMainDate.AddHandler(DatePicker.SelectedDateChangedEvent, new EventHandler<SelectionChangedEventArgs>(days_Selected));
            // в классе сделали свойство по конвертации даты из строки в объект DateTime  и привязку устанавливаем на него
            Binding Bdp = new Binding();
            Bdp.UpdateSourceTrigger = UpdateSourceTrigger.PropertyChanged;
            Bdp.Path = new PropertyPath("DateFromDp");
            fefMainDate.SetBinding(DatePicker.SelectedDateProperty, Bdp);
            fefMainDate.SetValue(DatePicker.SelectedDateFormatProperty,DatePickerFormat.Short ); // устанавливаем дату
            fefMainDate.SetValue(ComboBox.BackgroundProperty, Brushes.Beige);
            DTdate.VisualTree = fefMainDate;
            TGTCDate.CellTemplate = DTdate;
            this.DataGridView.Columns.Add(TGTCDate);

            // заполняем DataGrid
            this.DataGridView.ItemsSource = this.LDelivery;
        }
        private void TableProduct() 
        {
            // заполняем комбобокс из запроса заполнения таблицы ProductDetail
            List<string> AllProductDetail = new List<string>();
            foreach (ProductDetail Pd in this.LProductDetail)
                AllProductDetail.Add(Pd.NameItCat + " " + Pd.NameManMod + " " + Pd.NameProductDetail);

            // заполняем комбобокс из запроса заполнения таблицы Delivery
            List<string> AllDelivery = new List<string>();
            foreach (Delivery D in this.LDelivery)
                AllDelivery.Add(D.NameDelivCount + " " + D.NameDelivForm + " " + D.NameDelivProv + " " + D.DelivDate);

            //********************* создаем колонки и устанавливаем привязки к свойствам класса

            DataGridTextColumn TC = new DataGridTextColumn();
            TC.Header = "ID";
            TC.IsReadOnly = true;
            Binding B = new Binding();
            B.Path = new PropertyPath("IDProduct");
            B.UpdateSourceTrigger = UpdateSourceTrigger.PropertyChanged; // ****
            TC.Binding = B;
            //this.DataGridView.Columns.Add(TC);
            this.DataGridProduct.Columns.Add(TC);

            DataGridTemplateColumn TGC = new DataGridTemplateColumn();
            TGC.Header = "Описание товара";
            TGC.Width = 300;
            // создаем шаблон
            DataTemplate DT = new DataTemplate();
            // создаем объект фабрики
            FrameworkElementFactory fefMain = new FrameworkElementFactory(typeof(ComboBox));
            // устанавливаем привязку 
            Binding B1 = new Binding();
            B1.UpdateSourceTrigger = UpdateSourceTrigger.PropertyChanged;                // привязка для события после редактирования
            B1.Path = new PropertyPath("NameAllProduct");                                // к кому привязываемся
            TGC.SortMemberPath = "NameAllProduct";                                       // задаем привязку по сортировке
            fefMain.SetBinding(ComboBox.SelectedItemProperty, B1);                       // устанавливаем привязку к свойству
            fefMain.SetValue(ComboBox.ItemsSourceProperty, AllProductDetail);            // заполняем комбобокс
            fefMain.SetValue(ComboBox.IsEditableProperty, true);                         // автозаполнение комбобокса при вводе текста
            fefMain.SetValue(ComboBox.StaysOpenOnEditProperty, true);                    // открывает поле при заполнении
            fefMain.SetValue(ComboBox.BackgroundProperty, Brushes.AliceBlue);
            DT.VisualTree = fefMain;
            TGC.CellTemplate = DT;
           // this.DataGridView.Columns.Add(TGC);
            this.DataGridProduct.Columns.Add(TGC);

            DataGridTextColumn TC1 = new DataGridTextColumn(); 
            TC1.Header = "Кол-во";
            TC1.Width = 50;
            Binding B2 = new Binding();
            B2.UpdateSourceTrigger = UpdateSourceTrigger.PropertyChanged;
            B2.Path = new PropertyPath("Cnt");
            B2.ValidatesOnDataErrors = true;
            B2.ValidatesOnExceptions = true;
            TC1.Binding = B2;
            // this.DataGridView.Columns.Add(TC1);
            this.DataGridProduct.Columns.Add(TC1);

            DataGridTemplateColumn TGC1 = new DataGridTemplateColumn();
            TGC1.Header = "Доставка";
            TGC1.Width = 205;
            // создаем шаблон
            DataTemplate DT1 = new DataTemplate();
            // создаем объект фабрики
            FrameworkElementFactory fefMain1 = new FrameworkElementFactory(typeof(ComboBox));
            // устанавливаем привязку 
            Binding B3 = new Binding();
            B3.UpdateSourceTrigger = UpdateSourceTrigger.PropertyChanged;
            B3.Path = new PropertyPath("NameAllDelivery");
            TGC1.SortMemberPath = "NameAllDelivery";
            fefMain1.SetBinding(ComboBox.SelectedItemProperty, B3);
            fefMain1.SetValue(ComboBox.ItemsSourceProperty, AllDelivery);
            fefMain1.SetValue(ComboBox.IsEditableProperty, true);                         // автозаполнение комбобокса при вводе текста
            fefMain1.SetValue(ComboBox.StaysOpenOnEditProperty, true);                    // открывает поле при заполнении
            fefMain1.SetValue(ComboBox.BackgroundProperty,Brushes.AliceBlue);
            DT1.VisualTree = fefMain1;
            TGC1.CellTemplate = DT1;
            //this.DataGridView.Columns.Add(TGC1);
            this.DataGridProduct.Columns.Add(TGC1);

            DataGridTextColumn TC2 = new DataGridTextColumn();
            TC2.Header = "Цена $";
            Binding B4 = new Binding();
            B4.UpdateSourceTrigger = UpdateSourceTrigger.LostFocus;
            B4.Path = new PropertyPath("Price1");
            TC2.Binding = B4;
            // this.DataGridView.Columns.Add(TC2);
            this.DataGridProduct.Columns.Add(TC2);

            DataGridTextColumn TC3 = new DataGridTextColumn();
            TC3.Header = "Цена $+%";
            TC3.IsReadOnly = true;
            Binding B5 = new Binding();
            B5.UpdateSourceTrigger = UpdateSourceTrigger.LostFocus;
            B5.Path = new PropertyPath("Price2");
            TC3.Binding = B5;
            //this.DataGridView.Columns.Add(TC3);
            this.DataGridProduct.Columns.Add(TC3);

            DataGridTextColumn TC4 = new DataGridTextColumn();
            TC4.Visibility = Visibility.Hidden;
            TC4.Header = "Картинка";
            TC4.Width = 100;
            Binding B6 = new Binding();
            B6.UpdateSourceTrigger = UpdateSourceTrigger.PropertyChanged;
            B6.Path = new PropertyPath("PichterPath");
            TC4.Binding = B6;
            //this.DataGridView.Columns.Add(TC4);
            this.DataGridProduct.Columns.Add(TC4);

            DataGridTemplateColumn TGCB = new DataGridTemplateColumn();
            TGCB.Visibility = Visibility.Hidden;
            TGCB.Header = "Кнопка картинки";
            DataTemplate DTB = new DataTemplate();
            FrameworkElementFactory fefMainB = new FrameworkElementFactory(typeof(Button));
            fefMainB.SetValue(Button.ContentProperty, "Добавить картинку");
            fefMainB.AddHandler(Button.ClickEvent, this.button_Click); // обработчик клика кнопки
            fefMainB.SetValue(ComboBox.BackgroundProperty, Brushes.Beige);
            DTB.VisualTree = fefMainB;
            TGCB.CellTemplate = DTB;
            //this.DataGridView.Columns.Add(TGCB);
            this.DataGridProduct.Columns.Add(TGCB);

            // заполняем DataGrid
            //this.DataGridView.ItemsSource =this.LProduct;
            this.DataGridProduct.ItemsSource = this.LProduct;
        }
        private void TableSalerNames() 
        {
            DataGridTextColumn TC = new DataGridTextColumn();
            TC.Header = "ID";
            Binding B = new Binding();
            B.UpdateSourceTrigger = UpdateSourceTrigger.PropertyChanged;
            B.Path = new PropertyPath("IDSelerName ");
            TC.Binding = B;
            this.DataGridView.Columns.Add(TC);

            DataGridTextColumn TC1 = new DataGridTextColumn();
            TC1.Header = "Имя";
            TC1.Width = 80;
            Binding B1 = new Binding();
            B1.UpdateSourceTrigger = UpdateSourceTrigger.PropertyChanged;
            B1.Path = new PropertyPath("NameSalernames");
            TC1.Binding = B1;
            this.DataGridView.Columns.Add(TC1);

            DataGridTextColumn TC2 = new DataGridTextColumn();
            TC2.Header = " % ";
            Binding B2 = new Binding();
            B2.UpdateSourceTrigger = UpdateSourceTrigger.LostFocus;
            B2.Path = new PropertyPath("SelerPercent ");
            TC2.Binding = B2;
            this.DataGridView.Columns.Add(TC2);

            // заполняем DataGrid
            this.DataGridView.ItemsSource = this.LSalerNames;
        }
        private void TableSale() 
        {
            DataGridTextColumn TC = new DataGridTextColumn();
            TC.Header = "ID";
            TC.IsReadOnly = true;
            Binding B = new Binding();
            B.UpdateSourceTrigger = UpdateSourceTrigger.PropertyChanged;
            B.Path = new PropertyPath("IDSale");
            TC.Binding = B;
            this.DataGridView.Columns.Add(TC);

            DataGridTextColumn TC1 = new DataGridTextColumn();
            TC1.Header = "Проданный товар";
            TC1.IsReadOnly = true;
            Binding B1 = new Binding();
            B1.UpdateSourceTrigger = UpdateSourceTrigger.PropertyChanged;
            B1.Path = new PropertyPath("ProductName");
            TC1.Binding = B1;
            this.DataGridView.Columns.Add(TC1);

            DataGridTextColumn TC9 = new DataGridTextColumn();
            TC9.Header = "Вид доставки";
            TC9.IsReadOnly = true;
            Binding B9 = new Binding();
            B9.UpdateSourceTrigger = UpdateSourceTrigger.PropertyChanged;
            B9.Path = new PropertyPath("DeliveryName");
            TC9.Binding = B9;
            this.DataGridView.Columns.Add(TC9);

            DataGridTextColumn TC2 = new DataGridTextColumn();
            TC2.Header = "Продавец";
            TC2.IsReadOnly = true;
            Binding B2 = new Binding();
            B2.UpdateSourceTrigger = UpdateSourceTrigger.PropertyChanged;
            B2.Path = new PropertyPath("SalerName");
            TC2.Binding = B2;
            this.DataGridView.Columns.Add(TC2);

            DataGridTextColumn TC3 = new DataGridTextColumn();
            TC3.Header = "Кол-во";
            TC3.IsReadOnly = true;
            Binding B3 = new Binding();
            B3.UpdateSourceTrigger = UpdateSourceTrigger.PropertyChanged;
            B3.Path = new PropertyPath("CntProduct");
            TC3.Binding = B3;
            this.DataGridView.Columns.Add(TC3);

            DataGridTextColumn TC4 = new DataGridTextColumn();
            TC4.Header = "Цена продажи";
            TC4.IsReadOnly = true;
            Binding B4 = new Binding();
            B4.UpdateSourceTrigger = UpdateSourceTrigger.LostFocus;
            B4.Path = new PropertyPath("SalePrice");
            TC4.Binding = B4;
            this.DataGridView.Columns.Add(TC4);

            DataGridTextColumn TC5 = new DataGridTextColumn();
            TC5.Header = "Сумма";
            TC5.IsReadOnly = true;
            Binding B5 = new Binding();
            B5.UpdateSourceTrigger = UpdateSourceTrigger.LostFocus;
            B5.Path = new PropertyPath("SaleSumm");
            TC5.Binding = B5;
            this.DataGridView.Columns.Add(TC5);

            DataGridTextColumn TC6 = new DataGridTextColumn();
            TC6.Header = "Прибыль";
            TC6.IsReadOnly = true;
            Binding B6 = new Binding();
            B6.UpdateSourceTrigger = UpdateSourceTrigger.LostFocus;
            B6.Path = new PropertyPath("ProfitAll");
            TC6.Binding = B6;
            this.DataGridView.Columns.Add(TC6);

            DataGridTextColumn TC7 = new DataGridTextColumn();
            TC7.Header = "Прибыль продавца";
            TC7.IsReadOnly = true;
            Binding B7 = new Binding();
            B7.UpdateSourceTrigger = UpdateSourceTrigger.LostFocus;
            B7.Path = new PropertyPath("ProfitSaller");
            TC7.Binding = B7;
            this.DataGridView.Columns.Add(TC7);

            DataGridTextColumn TC8 = new DataGridTextColumn();
            TC8.Header = "Дата продажи";
            TC8.IsReadOnly = true;
            Binding B8 = new Binding();
            B8.UpdateSourceTrigger = UpdateSourceTrigger.PropertyChanged;
            B8.Path = new PropertyPath("SaleDate");
            TC8.Binding = B8;
            this.DataGridView.Columns.Add(TC8);

            // заполняем DataGrid
            this.DataGridView.ItemsSource = this.LSale;
        }
        private void TableBedBugs() 
        {

        }
        private void SelectingTables_toLoad(string tname) // выбираем какую таблицу загружаем в GridView 
        {
            switch (tname)
            {
                case "Тип товара": this.TableItemtype(); break;
                case "Категория товара": this.TableItemtypeCategory(); break;
                case "Производитель": this.TableManufacturers(); break;
                case "Модель": this.TableManufacturersModels(); break;
                case "Описание товара": this.TableProductDetail(); break;
                case "Страна": this.TableCountry(); break;
                case "Поставщик": this.TableProvider(); break;
                case "Доставка": this.TableDelivery(); break;
             //   case "Товар": this.TableProduct(); break;
                case "Продавцы": this.TableSalerNames(); break;
                case "Продажи": this.TableSale(); break;
                case "BedBugs": this.TableBedBugs(); break;
            }
            this.TableProduct();
        }
        private void ComboBox_TableName_SelectionChanged(object sender, SelectionChangedEventArgs e) // обработчик комбобокса выбора таблиц 
        {
            this.DataGridView.Columns.Clear();
            this.DataGridProduct.Columns.Clear();
            string tableName = e.AddedItems[0].ToString();
            this.SelectingTables_toLoad(tableName);
          // this.DataGridView.Columns[0].Visibility = Visibility.Collapsed; // колонку с Id делаем невидимой
         }
        #endregion
    }

    #region *************************************** Классы представления таблиц 

    class Itemtype : IEditableObject
    {
        public int IDItemtupe { get; set; }
        public string NameIt { get; set; }
        public Itemtype() { }
        public Itemtype(int id, string name)
        {
            this.IDItemtupe = id;
            this.NameIt = name;
        }
        // ********************************** реализация : IEditableObject
        private Itemtype backupCopy;
        private bool inEdit;
        public void BeginEdit()
        {
            Console.WriteLine("Start BeginEdit_Itemtype");
            if (inEdit) return;
            inEdit = true;
            backupCopy = this.MemberwiseClone() as Itemtype;
        }
        public void CancelEdit()
        {
            Console.WriteLine("Start CancelEdit_Itemtype");
            if (!inEdit) return;
            inEdit = false;
            // все свойства которым возврашаем значения
            this.IDItemtupe = backupCopy.IDItemtupe;
            this.NameIt = backupCopy.NameIt;
        }
        public void EndEdit()
        {
            Console.WriteLine("Start EndEdit_Itemtype");
            if (!inEdit) return;
            inEdit = false;
            backupCopy = null;
        }
    }
    class ItemtypeCategory : IEditableObject
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public string NameItemtype { get; set; }

        public ItemtypeCategory() { }
        public ItemtypeCategory(int id, string name, string nameitemtype)
        {
            this.ID = id;
            this.Name = name;
            this.NameItemtype = nameitemtype;
        }
        // ********************************** реализация : IEditableObject
        private ItemtypeCategory backupCopy;
        private bool inEdit;
        public void BeginEdit()
        {
            Console.WriteLine("Start BeginEdit_ItemtypeCategory");
            if (inEdit) return;
            inEdit = true;
            backupCopy = this.MemberwiseClone() as ItemtypeCategory;
        }
        public void CancelEdit()
        {
            Console.WriteLine("Start CancelEdit_ItemtypeCategory");
            if (!inEdit) return;
            inEdit = false;
            // все свойства которым возврашаем значения
            this.ID = backupCopy.ID;
            this.Name = backupCopy.Name;
            this.NameItemtype = backupCopy.NameItemtype;
        }
        public void EndEdit()
        {
            Console.WriteLine("Start EndEdit_ItemtypeCategory");
            if (!inEdit) return;
            inEdit = false;
            backupCopy = null;
        }
    }
    class Manufacturers : IEditableObject
    {
        public int IDManufacturers { get; set; }
        public string NameMan { get; set; }
        public Manufacturers() { }
        public Manufacturers(int id, string name)
        {
            this.IDManufacturers = id;
            this.NameMan = name;
        }
        // ********************************** реализация : IEditableObject
        private Manufacturers backupCopy;
        private bool inEdit;
        public void BeginEdit()
        {
            Console.WriteLine("Start BeginEdit_Manufacturers");
            if (inEdit) return;
            inEdit = true;
            backupCopy = this.MemberwiseClone() as Manufacturers;
        }
        public void CancelEdit()
        {
            Console.WriteLine("Start CancelEdit_Manufacturers");
            if (!inEdit) return;
            inEdit = false;
            // все свойства которым возврашаем значения
            this.IDManufacturers = backupCopy.IDManufacturers;
            this.NameMan = backupCopy.NameMan;
        }
        public void EndEdit()
        {
            Console.WriteLine("Start EndEdit_Manufacturers");
            if (!inEdit) return;
            inEdit = false;
            backupCopy = null;
        }
    }
    class ManufacturersModels : IEditableObject
    {
        public int IDManufacturersModels { get; set; }
        public string NameManufacturers { get; set; }
        public string NameManufacturersModels { get; set; }

        public ManufacturersModels() { }
        public ManufacturersModels(int id, string name, string nameman)
        {
            this.IDManufacturersModels = id;
            this.NameManufacturersModels = name;
            this.NameManufacturers = nameman;
        }
        // ********************************** реализация : IEditableObject
        private ManufacturersModels backupCopy;
        private bool inEdit;
        public void BeginEdit()
        {
            Console.WriteLine("Start BeginEdit_ManufacturersModels");
            if (inEdit) return;
            inEdit = true;
            backupCopy = this.MemberwiseClone() as ManufacturersModels;
        }
        public void CancelEdit()
        {
            Console.WriteLine("Start CancelEdit_ManufacturersModels");
            if (!inEdit) return;
            inEdit = false;
            // все свойства которым возврашаем значения
            this.IDManufacturersModels = backupCopy.IDManufacturersModels;
            this.NameManufacturers = backupCopy.NameManufacturers;
            this.NameManufacturersModels = backupCopy.NameManufacturersModels;
        }
        public void EndEdit()
        {
            Console.WriteLine("Start EndEdit_ManufacturersModels");
            if (!inEdit) return;
            inEdit = false;
            backupCopy = null;
        }
    }
    class ProductDetail : IEditableObject
    {
        public int IDProductDetail { get; set; }
        public string NameItCat { get; set; }
        public string NameManMod { get; set; }
        public string NameProductDetail { get; set; }

        public ProductDetail() { }
        public ProductDetail(int id, string name, string nameman, string nameProdDetail)
        {
            this.IDProductDetail = id;
            this.NameItCat = name;
            this.NameManMod = nameman;
            this.NameProductDetail = nameProdDetail;
        }
        // ********************************** реализация : IEditableObject
        private ProductDetail backupCopy;
        private bool inEdit;
        public void BeginEdit()
        {
            Console.WriteLine("Start BeginEdit_ProductDetail");
            if (inEdit) return;
            inEdit = true;
            backupCopy = this.MemberwiseClone() as ProductDetail;
        }
        public void CancelEdit()
        {
            Console.WriteLine("Start CancelEdit_ProductDetail");
            if (!inEdit) return;
            inEdit = false;
            // все свойства которым возврашаем значения
            this.IDProductDetail = backupCopy.IDProductDetail;
            this.NameItCat = backupCopy.NameItCat;
            this.NameManMod = backupCopy.NameManMod;
            this.NameProductDetail = backupCopy.NameProductDetail;
        }
        public void EndEdit()
        {
            Console.WriteLine("Start EndEdit_ProductDetail");
            if (!inEdit) return;
            inEdit = false;
            backupCopy = null;
        }
    }
    class Country : IEditableObject
    {
        public int IDCountry { get; set; }
        public string NameCountry { get; set; }
        public Country() { }
        public Country(int id, string name)
        {
            this.IDCountry = id;
            this.NameCountry = name;
        }
        // ********************************** реализация : IEditableObject
        private Country backupCopy;
        private bool inEdit;
        public void BeginEdit()
        {
            Console.WriteLine("Start BeginEdit_Country");
            if (inEdit) return;
            inEdit = true;
            backupCopy = this.MemberwiseClone() as Country;
        }
        public void CancelEdit()
        {
            Console.WriteLine("Start CancelEdit_Country");
            if (!inEdit) return;
            inEdit = false;
            // все свойства которым возврашаем значения
            this.IDCountry = backupCopy.IDCountry;
            this.NameCountry = backupCopy.NameCountry;
        }
        public void EndEdit()
        {
            Console.WriteLine("Start EndEdit_Country");
            if (!inEdit) return;
            inEdit = false;
            backupCopy = null;
        }
    }
    class Provider : IEditableObject
    {
        public int IDProvider { get; set; }
        public string NameProvider { get; set; }
        public Provider() { }
        public Provider(int id, string name)
        {
            this.IDProvider = id;
            this.NameProvider = name;
        }
        // ********************************** реализация : IEditableObject
        private Provider backupCopy;
        private bool inEdit;
        public void BeginEdit()
        {
            Console.WriteLine("Start BeginEdit_Provider");
            if (inEdit) return;
            inEdit = true;
            backupCopy = this.MemberwiseClone() as Provider;
        }
        public void CancelEdit()
        {
            Console.WriteLine("Start CancelEdit_Provider");
            if (!inEdit) return;
            inEdit = false;
            // все свойства которым возврашаем значения
            this.IDProvider = backupCopy.IDProvider;
            this.NameProvider = backupCopy.NameProvider;
        }
        public void EndEdit()
        {
            Console.WriteLine("Start EndEdit_Provider");
            if (!inEdit) return;
            inEdit = false;
            backupCopy = null;
        }
    }
    class Delivery : IEditableObject
    {
        public int IDDelivery { get; set; }
        public string NameDelivCount { get; set; }
        public string NameDelivForm { get; set; }
        public string NameDelivProv { get; set; }
        public float DelivPercent { get; set; }
        public string DelivDate { get; set; }
        public DateTime DateFromDp { get; set; }
       
        public Delivery() { }
        public Delivery(int id, string country, string form, string provider, float percent, string date)
        {
            this.IDDelivery = id;
            this.NameDelivCount = country; 
            this.NameDelivForm = form;
            this.NameDelivProv = provider;
            this.DelivPercent = percent;
            this.DelivDate = date;
            this.DateFromDp = DateTime.Parse(date);
        }
        // ********************************** реализация : IEditableObject
        private Delivery backupCopy;
        private bool inEdit;
        public void BeginEdit()
        {
            Console.WriteLine("Start BeginEdit_Delivery");
            if (inEdit) return;
            inEdit = true;
            backupCopy = this.MemberwiseClone() as Delivery;
        }
        public void CancelEdit()
        {
            Console.WriteLine("Start CancelEdit_Delivery");
            if (!inEdit) return;
            inEdit = false;
            // все свойства которым возврашаем значения
            this.IDDelivery = backupCopy.IDDelivery;
            this.NameDelivCount = backupCopy.NameDelivCount;
            this.NameDelivForm = backupCopy.NameDelivForm;
            this.NameDelivProv = backupCopy.NameDelivProv;
            this.DelivPercent = backupCopy.DelivPercent;
            this.DelivDate = backupCopy.DelivDate;
        }
        public void EndEdit()
        {
            Console.WriteLine("Start EndEdit_Delivery");
            if (!inEdit) return;
            inEdit = false;
            backupCopy = null;
        }
    }
    class Product : IEditableObject
    {
        public int IDProduct { get; set; }
        public string NameAllProduct { get; set; }
        public string NameAllDelivery { get; set; }
        public int Cnt { get; set; }
        public float Price1 { get; set; }
        public float Price2 { get; set; }
        public string PichterPath { get; set; }

        public Product() { }
        public Product(int id, string nameProd, string nameDel, int cnt, float price1, float price2, string path)
        {
            this.IDProduct = id;
            this.NameAllProduct = nameProd;
            this.NameAllDelivery = nameDel;
            this.Cnt = cnt;
            this.Price1 = price1;
            this.Price2 = price2;
            this.PichterPath = path;
        }
        // ********************************** реализация : IEditableObject
        private Product backupCopy;
        private bool inEdit;
        public void BeginEdit()
        {
            Console.WriteLine("Start BeginEdit_Product");
            if (inEdit) return;
            inEdit = true;
            backupCopy = this.MemberwiseClone() as Product;
        }

        public void CancelEdit()
        {
            Console.WriteLine("Start CancelEdit_Product");
            if (!inEdit) return;
            inEdit = false;
            // все свойства которым возврашаем значения
            this.NameAllDelivery = backupCopy.NameAllDelivery;
            this.Cnt = backupCopy.Cnt;
            this.NameAllProduct = backupCopy.NameAllProduct;
            this.Price1 = backupCopy.Price1;
            this.Price2 = backupCopy.Price2;
            this.PichterPath = backupCopy.PichterPath;
        }

        public void EndEdit()
        {
            Console.WriteLine("Start EndEdit_Product");
            if (!inEdit) return;
            inEdit = false;
            backupCopy = null;
        }
    }
    class SalerNames : IEditableObject
    {
        public int IDSelerName { get; set; }
        public string NameSalernames { get; set; }
        public float SelerPercent { get; set; }
        public SalerNames() { }
        public SalerNames(int id, string name, float percent)
        {
            this.IDSelerName = id;
            this.NameSalernames = name;
            this.SelerPercent = percent;
        }
        // ********************************** реализация : IEditableObject
        private SalerNames backupCopy;
        private bool inEdit;
        public void BeginEdit()
        {
            Console.WriteLine("Start BeginEdit_SalerNames");
            if (inEdit) return;
            inEdit = true;
            backupCopy = this.MemberwiseClone() as SalerNames;
        }
        public void CancelEdit()
        {
            Console.WriteLine("Start CancelEdit_SalerNames");
            if (!inEdit) return;
            inEdit = false;
            // все свойства которым возврашаем значения
            this.IDSelerName = backupCopy.IDSelerName;
            this.NameSalernames = backupCopy.NameSalernames;
            this.SelerPercent = backupCopy.SelerPercent;
        }
        public void EndEdit()
        {
            Console.WriteLine("Start EndEdit_SalerNames");
            if (!inEdit) return;
            inEdit = false;
            backupCopy = null;
        }
    }
    class Sale 
    {
        public int IDSale { get; set; }
        public string ProductName { get; set; }
        public string DeliveryName { get; set; }
        public string SalerName { get; set; }
        public int CntProduct { get; set; }
        public float SalePrice { get; set; }
        public float SaleSumm { get; set; }
        public float ProfitAll { get; set; }
        public float ProfitSaller { get; set; }
        public string SaleDate { get; set; }

        public Sale() { }
        public Sale(int id, string prodname, string salername, int cnt, float saleprice, float salesumm, float profitall, float profitsaler, string date)
        {
            this.IDSale = id;
            this.ProductName = prodname;
            this.SalerName = salername;
            this.CntProduct = cnt;
            this.SalePrice = saleprice;
            this.SaleSumm = salesumm;
            this.ProfitAll = profitall;
            this.ProfitSaller = profitsaler;
            this.SaleDate = date;
        }

    }
    class BedBugs 
    {
        public int IDBugs { get; set; }
        public string NameBugs { get; set; }
        public string SityBugs { get; set; }
        public string Telefon { get; set; }
        public string ProduktName { get; set; }
        public int Lesion { get; set; }

        public BedBugs() { }
        public BedBugs(int id, string name, string sityname, string telephone, string produktname, int lesion)
        {
            this.IDBugs = id;
            this.NameBugs = name;
            this.SityBugs = sityname;
            this.Telefon = telephone;
            this.ProduktName = produktname;
            this.Lesion = lesion;
        }

    }

    class RequestProduct 
    {
        public string Request { get; set; }
        DbConnection dbConection;
        DbDataAdapter dbAdapter;
        DataSet dbDataSet;
        ObservableCollection<Product> ObsColProd;
        public RequestProduct(DbConnection con,DbDataAdapter ad,DataSet ds,ObservableCollection<Product> oc)
        {
            this.dbConection = con;
            this.dbAdapter = ad;
            this.dbDataSet = ds;
            this.ObsColProd = oc;
            this.Request= "SELECT Product.id As id, Itemtype.name As Itname ,ItemtypeCategory.name As ItemCatname,Manufacturers.name As Manname,ManufacturersModels.name AS ManModnames,ProductDetail.name AS Pdname, Country.name AS Cname,Delivery.deliveryform_name As Delname,Provider.name AS Provname,Delivery.datedelivery AS Deldate,Product.cnt AS cnt,Product.price1 AS price1 ,Product.price2 AS price2,Product.pictures As pictures FROM Itemtype, ItemtypeCategory,Manufacturers, ManufacturersModels,ProductDetail,Country, Provider, Delivery,Product WHERE ProductDetail.id_itemtypeCategory = ItemtypeCategory.id AND ItemtypeCategory.id_itemtype = Itemtype.id AND ProductDetail.id_manufacturersModels = ManufacturersModels.id AND ManufacturersModels.id_manufacturers = Manufacturers.id AND Delivery.id_country = Country.id AND Delivery.id_provider = Provider.id AND Product.id_delivery = Delivery.id AND Product.id_productDetail = ProductDetail.id";
        }
        public RequestProduct() { }
        public  void GetProductrequestCollection( List<string> names)
        {
            DbCommand com = this.dbConection.CreateCommand();
            string request = this.Request;
            foreach (string s in names)
                request += " AND " + s;
            com.CommandText =request;
            this.dbAdapter.SelectCommand = com;
            this.dbAdapter.Fill(this.dbDataSet, "RequestProduct");

            foreach (DataRow r in this.dbDataSet.Tables["RequestProduct"].Rows)
            {
                Product PD = new Product();
                PD.IDProduct = (int)r["id"];
                PD.NameAllProduct = (r["Itname"].ToString() + " " + r["ItemCatname"].ToString() + " " + r["Manname"].ToString() + " " + r["ManModnames"].ToString() + " " + r["Pdname"].ToString());
                PD.Cnt = (int)r["cnt"];
                DateTime dt = (DateTime)r["Deldate"];
                string date = dt.ToShortDateString();
                PD.NameAllDelivery = (r["Cname"].ToString() + " " + r["Delname"].ToString() + " " + r["Provname"].ToString() + " " + date);
                PD.Price1 = (float)r["price1"];
                PD.Price2 = (float)r["price2"];
                PD.PichterPath = r["pictures"].ToString();
                this.ObsColProd.Add(PD);
            }
            this.dbDataSet.Tables["RequestProduct"].Clear();
        }
    }
    #endregion
   
   //************ классы сервера
    class ServerThread 
    {
        private string ip;
        private int port;
        private List<Product> LP;
        private ObservableCollection<SalerNames> LSalNames;
        private DbConnection dbConection;
       
        public ServerThread(string ip, int port,List<Product> lp,DbConnection con,ObservableCollection<SalerNames> sn)
        {
            this.ip = ip;
            this.port = port;
            this.LP = lp;
            this.dbConection = con;
            this.LSalNames = sn;
         }
        public ServerThread() { }
        public void Run()
        {
            TcpListener server=null;
            try
            {
                server = new TcpListener(IPAddress.Parse(this.ip), this.port);
                server.Start();
                while (true)
                {
                    TcpClient S_client = server.AcceptTcpClient();
                    Console.WriteLine("Подключение:{0}", S_client.Client.RemoteEndPoint);

                    // создаем поток  для обмена данными с клиентами через класс ReadWrite
                    ReadWrite RW = new ReadWrite(S_client,this.LP,this.dbConection,this.LSalNames);  // создаем объект для получения и обработки запроса
                    Thread T = new Thread(RW.Run);
                    T.IsBackground = true;
                    T.Start();
                }
            }
            catch (Exception e) {
                server.Stop();
                Console.WriteLine(e.Message);
            }
        }
    }
    class ReadWrite 
    {
        private TcpClient client;
        private List<Product> LP;
        private ObservableCollection<SalerNames> LSalerNames;
      
        private DbCommand dbCommand;
        private DbConnection dbConection;

        public ReadWrite(TcpClient client, List<Product> lp,DbConnection con,ObservableCollection<SalerNames> ls)
        {
            this.client = client;
            this.LP = lp;
            this.dbConection = con;
            this.LSalerNames = ls;
        }
        public ReadWrite() { }
        private int GetProductID( string prodname,string delivname) // получаем id товара из списка который отправляем клиенту 
        {
            foreach (Product p in this.LP)
                if(p.NameAllProduct==prodname && p.NameAllDelivery==delivname)
                    return p.IDProduct;
            return -1;
        }
        private int GetSalerNameID(string salername)// получаем id  продавца 
        {
            foreach (SalerNames sn in this.LSalerNames)
                if (sn.NameSalernames==salername)
                    return sn.IDSelerName;
            return -1;
        }
        private void Protocol(string str, NetworkStream NS) // протокол сервера, принимант запрос от клиента и сссылку на объект связи
        {
            // парсим ключ\значение
            string[] arr = str.Split(new string[] {"|"}, StringSplitOptions.RemoveEmptyEntries);
            string key = arr[0];
            
            // протокол данных возврашаемые сервером
            string answer = null;
            bool IsPichter = true;
            switch(key)
            {
                case "GETPRODUCT":
                    answer=key+"!OK!";
                    string id, prod, delivery ,cnt, price2, pichter,sum;
                    foreach (Product P in this.LP)
                    {
                        id = P.IDProduct.ToString()+"|";
                        prod = P.NameAllProduct + "|";
                        cnt = P.Cnt.ToString() + "|";
                        delivery = P.NameAllDelivery + "|";
                        price2 = P.Price2.ToString() + "|";
                        pichter = P.PichterPath + "|";
                        sum = id + prod + cnt+delivery+ price2 + pichter+"&";
                        answer += sum;
                    }
                    break;
                case "LOGIN":
                    answer = key + "!OK!";
                    this.dbConection.Open();
                    // находим id записи
                    int idSaler = 0;
                    float percenteds = 0;
                    this.dbCommand = this.dbConection.CreateCommand();
                    this.dbCommand.CommandText = string.Format("SELECT SalerNames.id,SalerNames.percented FROM SalerNames WHERE SalerNames.name='{0}' AND SalerNames.passwords='{1}'", arr[1], arr[2]);
                    try
                    {
                        DbDataReader R = this.dbCommand.ExecuteReader();
                        if (R.HasRows)// если содержит строки
                            while (R.Read())
                            {
                                idSaler = R.GetInt32(0);
                                percenteds = R.GetFloat(1);
                            }
                        R.Close();
                    }
                    catch (Exception e1) { MessageBox.Show(e1.Message, "Ошибка", MessageBoxButton.OK, MessageBoxImage.Error); }
                    finally { this.dbConection.Close(); }
                    answer += idSaler.ToString()+"!"+percenteds.ToString();
                    break;
                case "GETRATE":
                    answer = key + "!OK!";
                    this.dbConection.Open();
                    // находим id записи
                    float course = 0;
                    this.dbCommand = this.dbConection.CreateCommand();
                    this.dbCommand.CommandText ="SELECT Admin.course FROM Admin ";
                    try
                    {
                        DbDataReader R = this.dbCommand.ExecuteReader();
                        if (R.HasRows)// если содержит строки
                            while (R.Read())
                                course = R.GetFloat(0);
                        R.Close();
                    }
                    catch (Exception e1) { MessageBox.Show(e1.Message, "Ошибка", MessageBoxButton.OK, MessageBoxImage.Error); }
                    finally { this.dbConection.Close(); }
                    answer += course.ToString();
                    break;
                case "SAWEORDER":
                    string  product, deliverys, saler, cnts, price, summ, profitall, profitsaler, date;
                    int idProduct = 0;
                    int idSalerName = 0;
                    string[] arrSaleProduct;
                    this.dbConection.Open();
                    //1 - получаем проданные продукты со всем описанием 
                    string[] arrAllSaleProduct = arr[1].Split('&');
                    for (int i=0;i<arrAllSaleProduct.Length-1;i++)
                    {
                        arrSaleProduct = arrAllSaleProduct[i].Split('#');
                        product = arrSaleProduct[0];
                        deliverys = arrSaleProduct[1];
                        saler = arrSaleProduct[2];
                        // получаем id товара и продавца
                        idProduct = this.GetProductID(product, deliverys);
                        idSalerName = this.GetSalerNameID(saler);

                        cnts = arrSaleProduct[3];
                        price = arrSaleProduct[4].Replace(',','.');
                        summ = arrSaleProduct[5].Replace(',', '.');
                        profitall = arrSaleProduct[6].Replace(',', '.');
                        profitsaler = arrSaleProduct[7].Replace(',', '.');
                        date = arrSaleProduct[8];
                       
                        //2- записываем в бд 
                        this.dbCommand.CommandText = string.Format("INSERT INTO Sale (id_product,id_salerNames,cnt,price,summa,profitAll,profitSaler,dateSale) VALUES({0},{1},{2},{3},{4},{5},{6},'{7}')", idProduct, idSalerName, cnts, price, summ, profitall, profitsaler, date);
                        try { this.dbCommand.ExecuteNonQuery(); } //для обновления БД, вызываем после каждого запроса
                        catch (Exception e1) { MessageBox.Show(e1.Message); }

                        //3- меняем количество товара в таблице Product
                        //-находим предыдущее количество
                        int oldcnt = 0;
                        this.dbCommand.CommandText =string.Format("SELECT Product.cnt FROM Product WHERE Product.id={0}",idProduct);
                        try
                        {
                            DbDataReader R = this.dbCommand.ExecuteReader();
                            if (R.HasRows)// если содержит строки
                                while (R.Read())
                                    oldcnt = R.GetInt32(0);
                            R.Close();
                        }
                        catch (Exception e2) { MessageBox.Show(e2.Message); }
                        //- устанавливаем новое количество
                        int newcnt = oldcnt - int.Parse(cnts);
                        this.dbCommand.CommandText = string.Format("UPDATE Product SET cnt={0} WHERE id={1}",newcnt, idProduct);
                        try { this.dbCommand.ExecuteNonQuery(); }
                        catch (Exception e3) { MessageBox.Show(e3.Message); }
                        //- обновляем GridViewProduct
                        MainWindow.IsPause = true;
                        Console.WriteLine("------------------------- "+MainWindow.IsPause.ToString());
                    }
                    this.dbConection.Close();
                    answer = key + "!OK!";
                    break;
                case "GETPICHTER":
                    answer = key + "!OK!";
                    string path = arr[1];
                    // тело(сама картинка)
                    byte[] byf = File.ReadAllBytes(path); // байтовый масив с картинкой
                    BinaryWriter BW = new BinaryWriter(NS);
                    BW.Write(byf.Length);// дописываем длину к картинке
                    NS.Write(byf, 0, byf.Length); // отправляем картинку + размер картинки как число
                    // byte[] sumarr = byteanswer.Concat(byf).ToArray();
                    // NS.Write(sumarr, 0, sumarr.Length);
                    IsPichter = false;
                    break;
            }
            if(IsPichter)
            {
                byte[] byteanswer = Encoding.UTF8.GetBytes(answer);
                NS.Write(byteanswer, 0, byteanswer.Length); // отправляем  ответ 
            }
        }
        public void Run()
        {
            string clientinfo = this.client.Client.RemoteEndPoint.ToString();// информация о подключенном клиенте
            MemoryStream MS = new MemoryStream();
            NetworkStream NS = this.client.GetStream();
            byte[] buf = new byte[1024];
            try
            {
                while (true)
                {
                    //*********** читаем полученные от клиента байты (ждет команду мастера)
                    do
                    {
                        int cnt = NS.Read(buf, 0, buf.Length);
                        if (cnt == 0)
                            throw new Exception("0 bytes received");
                        MS.Write(buf, 0, cnt);
                    } while (NS.DataAvailable);

                    //***********  обрабатываем полученный от клиента пакет данных
                    string str = Encoding.UTF8.GetString(MS.GetBuffer(), 0, (int)MS.Length);
                    //***********  ответ сервера на запрос
                    this.Protocol(str, NS);  // в методе обрабатываем запрос и даем на него ответ
                    MS.SetLength(0);         // освобождаем поток памяти
                }
            }
            catch
            {
                Console.WriteLine("Ошибка 3037: разрыв соединения: {0}", clientinfo);
                NS.Close();
            }
        }
    }
 }

