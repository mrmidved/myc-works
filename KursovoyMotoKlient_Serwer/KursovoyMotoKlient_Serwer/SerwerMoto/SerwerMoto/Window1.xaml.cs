﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SerwerMoto
{
    public partial class Window1 : Window
    {
        private DbProviderFactory dbProviderFactory;
        private DbConnection dbConection;
        private DbCommand dbCommand;
        private DbDataReader dbReader;
        private bool IsAdmin;
        private bool IsLogin;
        private int id;
        public Window1()
        {
            InitializeComponent();
            // создаем соединение
            string provider = ConfigurationManager.AppSettings["provider"];
            string connectionString = ConfigurationManager.AppSettings["connectionStr"];
            // 1
            this.dbProviderFactory = DbProviderFactories.GetFactory(provider);
            // 2
            this.dbConection = this.dbProviderFactory.CreateConnection();
            this.dbConection.ConnectionString = connectionString;

            this.Loaded += Window1_Loaded;

            // обработчики кнопок
            this.button_Oк.Click += ButtonClick;
            this.button_Cancel.Click+=ButtonClick;
            // обработчик закрытия окна
            this.Closing += Window1_Closing;
        }
        private void Window1_Loaded(object sender, RoutedEventArgs e) 
        {
            // проверяем заходим первый раз при регистрации или для изменения учетных данных
            if (this.textBox_Name.Text == "" && this.passwordBox_Pasword.Password == "")
                this.IsLogin = true;
            else
            {
                // получаем id записи
                this.dbCommand = this.dbConection.CreateCommand();
                this.dbCommand.CommandText = string.Format("SELECT Admin.id FROM Admin WHERE Admin.name='{0}' AND Admin.password='{1}'", this.textBox_Name.Text, this.passwordBox_Pasword.Password);
                try
                {
                    this.dbConection.Open();
                    this.dbReader = this.dbCommand.ExecuteReader();
                    if (this.dbReader.HasRows)// если содержит строки
                        while (this.dbReader.Read())
                           this.id = this.dbReader.GetInt32(0);
                }
                catch (Exception e1) { MessageBox.Show(e1.Message); }
                finally { this.dbConection.Close(); }
                this.IsLogin = false;
            }
        }

        // методы загрузки логина и  изминения учетных данных
        private void Login( object sender) 
        {
            Button B = sender as Button;
            int id = 0;
            this.IsAdmin = true;
            // если нажали OK
            if (B.Name == "button_Oк")
            {
                //3
                this.dbCommand = this.dbConection.CreateCommand();
                this.dbCommand.CommandText = string.Format("SELECT Admin.id FROM Admin WHERE Admin.name='{0}' AND Admin.password='{1}'", this.textBox_Name.Text, this.passwordBox_Pasword.Password);
                try
                {
                    this.dbConection.Open();
                    this.dbReader = this.dbCommand.ExecuteReader();
                    if (this.dbReader.HasRows)// если содержит строки
                        while (this.dbReader.Read())
                            id = this.dbReader.GetInt32(0);
                }
                catch (Exception e1) { MessageBox.Show(e1.Message); }
                finally { this.dbConection.Close(); }

                // проверяем результат
                if (id > 0)
                    this.IsAdmin = true;
                else
                {
                    this.IsAdmin = false;
                    this.textBox_Name.Text = "Отказано в доступе !";
                    this.passwordBox_Pasword.Clear();
                }
                this.DialogResult = true;
            }

            // если нажали Cancel
            if (B.Name == "button_Cancel")
                this.DialogResult = false;
            // в конце в любом случае закрываем окно
            this.Close();
        }
        private void EditLogin(object sender) 
        {
            Button B = sender as Button;
            // если нажали OK
            if (B.Name == "button_Oк")
            {
                //3
                this.dbConection.Open();
                this.dbCommand = this.dbConection.CreateCommand();
                this.dbCommand.CommandText = string.Format("UPDATE Admin SET name='{0}',password='{1}' WHERE id={2}", this.textBox_Name.Text, this.passwordBox_Pasword.Password,this.id);
                try { this.dbCommand.ExecuteNonQuery(); }
                catch (Exception e1) { MessageBox.Show(e1.Message); }
                finally { this.dbConection.Close(); }
                this.IsAdmin = true;
                this.DialogResult = true;
            }

            // если нажали Cancel
            if (B.Name == "button_Cancel")
            {
                this.DialogResult = false;
                this.IsAdmin = true;
            }
           
        }

        private void ButtonClick(object sender, RoutedEventArgs e)
        {
            if (this.IsLogin)
                this.Login(sender);
            else
            {
                this.EditLogin(sender);
                this.Close();
            } 
        }
        // в обработчике закрытия окна проверяем результат
        private void Window1_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (!this.IsAdmin)
                e.Cancel = true;
        }

    }
}
